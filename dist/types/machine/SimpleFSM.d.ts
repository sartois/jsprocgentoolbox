export interface Transition {
    name: string;
    from?: string;
    to: string;
    before?: () => void;
}
export interface MachineParams {
    initial: string;
    transitions: Transition[];
}
export declare class SimpleFSM extends EventTarget {
    #private;
    transitions: Transition[] | undefined;
    initial: string | undefined;
    hasStarted: boolean;
    setTransitions({ transitions, initial }: MachineParams): void;
    start(): void;
    dispatch(transitionName: string): void;
    getCurrentState(): string | undefined;
}
//# sourceMappingURL=SimpleFSM.d.ts.map