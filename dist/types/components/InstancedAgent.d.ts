/// <reference types="react" />
import { InstancedMesh } from "three";
import { type Behavior } from "../behaviors";
export type InstancedAgentProps = {
    behaviors: Behavior[];
    disableGameLoop?: boolean;
};
export declare const InstancedAgent: import("react").ForwardRefExoticComponent<InstancedAgentProps & import("react").RefAttributes<InstancedMesh<import("three").BufferGeometry<import("three").NormalBufferAttributes>, import("three").Material | import("three").Material[]>>>;
//# sourceMappingURL=InstancedAgent.d.ts.map