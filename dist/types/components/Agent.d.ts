import { PropsWithChildren } from "react";
import { IAgent } from "../agents";
import { type Behavior } from "../behaviors";
export type AgentProps = {
    agent: IAgent;
    behaviors: Behavior[];
    disableGameLoop?: boolean;
};
export declare function Agent(props: PropsWithChildren<AgentProps>): import("react/jsx-runtime").JSX.Element;
//# sourceMappingURL=Agent.d.ts.map