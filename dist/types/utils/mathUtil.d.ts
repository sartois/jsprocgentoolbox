/**
 * Returns a random number between min (inclusive) and max (exclusive)
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * @returns {Number}
 */
export declare function random(min: number, max: number, rng?: () => number): number;
//# sourceMappingURL=mathUtil.d.ts.map