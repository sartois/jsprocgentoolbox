import { Distributor, NormalDistributionConfig } from ".";
export declare class CircleDistributor extends Distributor {
    radius: number;
    constructor(radius: number);
    getUniform(agentNb: number): import(".").Position[];
    getEdgy(agentNb: number, config?: NormalDistributionConfig): import(".").Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): import(".").Position[];
}
//# sourceMappingURL=CircleDistributor.d.ts.map