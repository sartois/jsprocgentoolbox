import { Distributor, NormalDistributionConfig, Position } from ".";
export declare class BoxDistributor extends Distributor {
    size: number;
    halfSize: number;
    constructor(halfSize: number);
    getUniform(agentNb: number): Position[];
    getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
}
//# sourceMappingURL=BoxDistributor.d.ts.map