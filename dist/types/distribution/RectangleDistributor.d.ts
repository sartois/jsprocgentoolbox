import { NormalDistributionConfig, Position, Distributor } from ".";
export type Rectangle = {
    width: number;
    height: number;
};
export declare class RectangleDistributor extends Distributor {
    width: number;
    height: number;
    halfWidth: number;
    halfHeight: number;
    constructor({ width, height }: Rectangle, originCentered?: boolean);
    getUniform(agentNb: number): Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
    getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
}
//# sourceMappingURL=RectangleDistributor.d.ts.map