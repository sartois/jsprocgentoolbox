import { Distributor, NormalDistributionConfig } from ".";
export declare class SphereDistributor extends Distributor {
    radius: number;
    constructor(radius: number);
    getEdgy(agentNb: number, config?: NormalDistributionConfig): import(".").Position[];
    getUniform(agentNb: number): import(".").Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): import(".").Position[];
}
//# sourceMappingURL=SphereDistributor.d.ts.map