import { Color, LineSegments, Object3D } from "three";
import { Object3DNode } from "@react-three/fiber";
import { PreComputedFlowField } from "./PreComputedFlowField";
export declare class FlowFieldHelper extends Object3D {
    readonly flowField: PreComputedFlowField;
    constructor(flowField: PreComputedFlowField, gridColor?: Color, arrowColor?: Color, showGrid?: boolean);
    addArrows(arrowColor?: Color): Object3D<import("three").Object3DEventMap>;
}
export declare class FlowFieldGrid extends LineSegments {
    /**
     * @param {FlowField} flowField
     * @param {Color} color
     */
    constructor(flowField: {
        height: number;
        cellSize: number;
        width: number;
    }, color: Color);
}
declare module "@react-three/fiber" {
    interface ThreeElements {
        flowFieldHelper: Object3DNode<FlowFieldHelper, typeof FlowFieldHelper>;
    }
}
//# sourceMappingURL=FlowFieldHelper.d.ts.map