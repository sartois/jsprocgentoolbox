import { Vector3 } from "three";
import { FlowField } from "../FlowField";
export declare class PreComputedFlowField implements FlowField {
    #private;
    readonly cellSize: number;
    readonly flowField: FlowField;
    width: number;
    height: number;
    cols: number;
    rows: number;
    fields: Vector3[];
    constructor(_width: number, _height: number, cellSize: number, flowField: FlowField);
    lookup(position: {
        x: number;
        y: number;
    }): Vector3;
}
//# sourceMappingURL=PreComputedFlowField.d.ts.map