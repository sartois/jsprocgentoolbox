import { Vector3 } from "three";
import { NoiseFunction4D, NoiseFunction3D } from "simplex-noise";
import { FlowField } from "./FlowField";
export declare class SimplexFlowField3D implements FlowField {
    noise3D: NoiseFunction3D;
    noise4D: NoiseFunction4D;
    private static readonly upperLimit;
    private static readonly scaleDownFactor;
    private static shift;
    constructor();
    lookup(position: {
        x: number;
        y: number;
        z: number;
    }, time?: number | undefined): Vector3;
}
//# sourceMappingURL=SimplexFlowField3D.d.ts.map