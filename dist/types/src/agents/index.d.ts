import { Vector3 } from "three";
export interface IAgent {
    id: number;
    acceleration: Vector3;
    velocity: Vector3;
    position: Vector3;
    maxSpeed: number;
    maxForce: number;
    update(delta?: number): void;
    applyForce(force: Vector3): void;
}
export interface AgentArgs {
    acceleration?: Vector3;
    velocity?: Vector3;
    position: Vector3;
}
export interface AgentParam {
    maxForce: number;
    maxSpeed: number;
}
export declare class MovingAgent implements IAgent {
    readonly maxSpeed: number;
    readonly maxForce: number;
    acceleration: Vector3;
    velocity: Vector3;
    position: Vector3;
    id: number;
    static counter: number;
    constructor(agent: AgentArgs, maxSpeed?: number, maxForce?: number, id?: number | undefined);
    update(delta?: number): void;
    applyForce(force: Vector3): void;
}
//# sourceMappingURL=index.d.ts.map