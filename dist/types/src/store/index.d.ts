import { type WanderSettings, type WalkSettings, type PursueSettings, type EvadeSettings, type SeparateSettings, type AlignSettings, type CohesionSettings, BehaviorKind } from "../behaviors";
import { FlowField } from "../flowfield/FlowField";
import { IAgent } from "../agents";
import { type BoundariesSettings } from "../boundary";
export interface FlockSettings {
    world: {
        is2D: boolean;
    };
    agent: {
        maxSpeed: number;
        maxForce: number;
        height: number;
        count: number;
    };
    wander: WanderSettings;
    boundaries: BoundariesSettings;
    separate: SeparateSettings;
    align: AlignSettings;
    cohesion: CohesionSettings;
    neighbors: {
        useGrid: boolean;
        gridCellSize: number;
    };
    walk: WalkSettings;
    pursue: PursueSettings;
    evade: EvadeSettings;
    balance: {
        priorityBehavior?: string | undefined;
        priorities?: [BehaviorKind | string, number][];
    };
}
interface FlockState {
    pursueTarget: IAgent | undefined;
    evadeFrom: IAgent | undefined;
    flock: IAgent[];
    viewport: {
        width: number;
        height: number;
    } | undefined;
    debug: boolean;
    settings: FlockSettings | undefined;
    is2D: boolean;
    flowfield: FlowField | undefined;
}
export declare const state: FlockState;
export declare function mergeSettingsWithDefault(settings: Partial<FlockSettings>): FlockSettings;
export {};
//# sourceMappingURL=index.d.ts.map