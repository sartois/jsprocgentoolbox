import { Vector3, Vector3Tuple } from "three";
import { IAgent } from "../../agents";
export declare function flee(agent: IAgent, target: Vector3): Vector3Tuple;
//# sourceMappingURL=flee.d.ts.map