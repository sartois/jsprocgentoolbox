import { Vector3, Vector3Tuple } from "three";
import { IAgent } from "../../agents";
export declare function seek(agent: IAgent, target: Vector3, clampLength?: boolean): Vector3Tuple;
//# sourceMappingURL=seek.d.ts.map