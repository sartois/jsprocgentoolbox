import { FlockSettings } from "../store";
import { IAgent } from "../agents";
import { TypedVector3 } from ".";
export type WalkSettings = {
    turnChance: number;
    useSteppedAngle: boolean;
};
export declare function walk(agent: IAgent, settings: FlockSettings, _: IAgent[] | undefined, rng?: () => number): TypedVector3 | void;
//# sourceMappingURL=walk.d.ts.map