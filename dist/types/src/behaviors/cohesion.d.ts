import { IAgent } from "../agents";
import { FlockSettings } from "../store";
import { TypedVector3 } from ".";
export interface CohesionSettings {
    threshold?: number;
    depth?: 0 | 1 | 2;
}
export declare function getCohesionDefaults(baseHeight: number): CohesionSettings;
export declare function cohesion(agent: IAgent, settings: FlockSettings, flock?: IAgent[]): TypedVector3 | void;
//# sourceMappingURL=cohesion.d.ts.map