import { IAgent } from "../agents";
import { type FlockSettings } from "../store";
import { TypedVector3 } from ".";
export type AlignSettings = {
    threshold?: number;
    depth?: 0 | 1 | 2;
};
export declare function getAlignDefaults(baseHeight: number): AlignSettings;
export declare function align(agent: IAgent, settings: FlockSettings, flock?: IAgent[]): TypedVector3 | void;
//# sourceMappingURL=align.d.ts.map