import { Object3D } from "three";
import { IAgent } from "../../agents";
export declare function alignVelocity(agent: IAgent, mesh: Object3D): void;
//# sourceMappingURL=alignVelocity.d.ts.map