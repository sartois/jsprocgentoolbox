import { Object3D } from "three";
import { IAgent } from "../../agents";
export declare function followPosition(agent: IAgent, mesh: Object3D, is2D?: boolean): void;
//# sourceMappingURL=followPosition.d.ts.map