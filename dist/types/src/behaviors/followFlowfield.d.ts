import { IAgent } from "../agents";
import { TypedVector3 } from ".";
export declare function followFlowfield(agent: IAgent): TypedVector3 | void;
//# sourceMappingURL=followFlowfield.d.ts.map