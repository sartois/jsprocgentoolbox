import { Vector3 } from "three";
export declare function getRandomVelocity(useSteppedAngle: boolean, is2D: boolean): Vector3;
//# sourceMappingURL=getRandomVelocity.d.ts.map