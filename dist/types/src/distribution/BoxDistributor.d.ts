import { NormalDistributionConfig, Position } from ".";
export declare class BoxDistributor {
    size: number;
    halfSize: number;
    constructor(halfSize: number);
    getUniform(agentNb: number): Position[];
    getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
    protected loop(agentNb: number, callback: (args: any) => Position): Position[];
}
//# sourceMappingURL=BoxDistributor.d.ts.map