import { Position, NormalDistributionConfig } from ".";
export declare class SphereDistributor {
    radius: number;
    constructor(radius: number);
    getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
    getUniform(agentNb: number): Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
    protected loop(agentNb: number, callback: (args: any) => Position): Position[];
}
//# sourceMappingURL=SphereDistributor.d.ts.map