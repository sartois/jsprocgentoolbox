import { NormalDistributionConfig, Position } from ".";
export type Rectangle = {
    width: number;
    height: number;
};
export declare class RectangleDistributor {
    width: number;
    height: number;
    halfWidth: number;
    halfHeight: number;
    constructor({ width, height }: Rectangle, originCentered?: boolean);
    getUniform(agentNb: number): Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
    getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
    protected loop(agentNb: number, callback: (args: any) => Position): Position[];
}
//# sourceMappingURL=RectangleDistributor.d.ts.map