import { NormalDistributionConfig, Position } from ".";
export declare class CircleDistributor {
    radius: number;
    constructor(radius: number);
    getUniform(agentNb: number): Position[];
    getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
    getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
    protected loop(agentNb: number, callback: (args: any) => Position): Position[];
}
//# sourceMappingURL=CircleDistributor.d.ts.map