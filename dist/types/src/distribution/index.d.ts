export type Position = {
    x: number;
    y: number;
    z: number;
};
export type NormalDistributionConfig = {
    mu?: number;
    sigma?: number;
};
export declare abstract class Distributor {
    abstract getUniform(agentNb: number): Position[];
    abstract getNormal(agentNb: number, config?: NormalDistributionConfig): Position[];
    abstract getEdgy(agentNb: number, config?: NormalDistributionConfig): Position[];
    protected loop(agentNb: number, callback: (args: any) => Position): Position[];
}
export * from "./BoxDistributor";
export * from "./CircleDistributor";
export * from "./RectangleDistributor";
export * from "./SphereDistributor";
//# sourceMappingURL=index.d.ts.map