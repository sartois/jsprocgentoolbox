import { IAgent } from "../agents";
import { BoxBoundaries } from ".";
import { TypedVector3 } from "../behaviors";
export declare function boxBoundaries(agent: IAgent, shape: BoxBoundaries, baseHeight: number): void | TypedVector3;
//# sourceMappingURL=boxBoundaries.d.ts.map