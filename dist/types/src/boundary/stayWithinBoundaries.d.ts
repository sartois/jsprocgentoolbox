import { IAgent } from "../agents";
import { TypedVector3 } from "../behaviors";
import { FlockSettings } from "../store";
import { BoundariesShape } from ".";
export interface BoundariesSettings {
    shape: BoundariesShape;
    threshold?: number;
}
export declare function getBoundariesDefaults(baseHeight: number): BoundariesSettings;
export declare function stayWithinBoundaries(agent: IAgent, settings: FlockSettings): TypedVector3 | void;
//# sourceMappingURL=stayWithinBoundaries.d.ts.map