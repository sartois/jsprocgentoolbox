import { PropsWithChildren } from "react";
import { Vector3 } from "three";
import { IAgent } from "../agents";
import { FlockSettings } from "../store";
import type { Position } from "../distribution";
export type FlockProps = {
    distribution?: Position[];
    settings?: Partial<FlockSettings>;
    render?: (agent: IAgent) => JSX.Element;
    agents?: IAgent[];
    randomVelocity?: boolean;
    sameVelocity?: Vector3;
    setFlock?: (flock: IAgent[]) => void;
};
export declare function Flock(props: PropsWithChildren<FlockProps>): import("react/jsx-runtime").JSX.Element;
//# sourceMappingURL=Flock.d.ts.map