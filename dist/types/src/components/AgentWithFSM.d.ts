import { PropsWithChildren } from "react";
import { IAgent } from "../agents";
import { type Behavior } from "../behaviors";
import { SimpleFSM } from "../machine/SimpleFSM";
interface AgentWithFSMProps {
    agent?: IAgent;
    machine: SimpleFSM;
    behaviors: [string, Behavior[] | []][];
}
export declare function AgentWithFSM(props: PropsWithChildren<AgentWithFSMProps>): import("react/jsx-runtime").JSX.Element;
export {};
//# sourceMappingURL=AgentWithFSM.d.ts.map