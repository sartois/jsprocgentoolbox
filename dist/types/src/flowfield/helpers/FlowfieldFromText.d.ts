import { Vector3 } from "three";
type Viewport = {
    width: number;
    height: number;
};
type FillCanvasArgs = {
    text: string;
    font: string;
    color: string;
};
export declare class FlowfieldFromText {
    #private;
    width: number;
    height: number;
    ctx: CanvasRenderingContext2D;
    imgData: ImageData;
    canvas: HTMLCanvasElement;
    fields: Vector3[];
    cellSize: number;
    cols: number;
    constructor({ width, height }: Viewport);
    fillCanvas({ text, font, color }: FillCanvasArgs): void;
    createFlowField(cellSize?: number): void;
    lookup(position: {
        x: number;
        y: number;
    }): Vector3;
}
export {};
//# sourceMappingURL=FlowfieldFromText.d.ts.map