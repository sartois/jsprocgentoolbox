import { Vector3 } from "three";
import { NoiseFunction2D, NoiseFunction3D } from "simplex-noise";
import { FlowField } from "./FlowField";
export declare class SimplexFlowField2D implements FlowField {
    noise3D: NoiseFunction3D;
    noise2D: NoiseFunction2D;
    private static readonly upperLimit;
    private static readonly scaleDownFactor;
    constructor();
    lookup(position: {
        x: number;
        y: number;
        z?: number;
    }, time?: number | undefined): Vector3;
}
//# sourceMappingURL=SimplexFlowField2D.d.ts.map