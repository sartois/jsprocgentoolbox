import { IAgent } from "../agents";
export interface getNeighborsSettings {
    agent: IAgent;
    flock: IAgent[] | undefined;
    threshold?: number;
    depth?: number;
    useGrid: boolean;
}
export declare function getNeighbors({ agent, flock, threshold, depth, useGrid, }: getNeighborsSettings): IAgent[];
//# sourceMappingURL=getNeighbors.d.ts.map