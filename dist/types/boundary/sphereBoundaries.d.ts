import { IAgent } from "../agents";
import { TypedVector3 } from "../behaviors";
import { SphereBoundaries } from ".";
export declare function sphereBoundaries(agent: IAgent, shape: SphereBoundaries, threshold: number | undefined): void | TypedVector3;
//# sourceMappingURL=sphereBoundaries.d.ts.map