import { IAgent } from "../agents";
import { BoundariesShape, RectangleBoundaries } from "../boundary";
import { Behavior } from "../behaviors";
export type GridSize = {
    gridCellSize: number;
};
export { createGrid, registerInGrid, resetGrid, getNeighborsFromGrid, getGrid };
declare function createGrid(shape: BoundariesShape, size: GridSize): void;
declare const registerInGrid: Behavior;
interface RectangleGridData {
    width: number;
    height: number;
    colsCount: number;
    rowsCount: number;
    cellSize: number;
    behavior: RectangleBoundaries["behavior"];
    content: IAgent[][];
}
declare function getGrid(shape: BoundariesShape): RectangleGridData;
declare function resetGrid(): void;
declare function getNeighborsFromGrid(agent: IAgent, depth?: number): IAgent[];
//# sourceMappingURL=gridPartitionner.d.ts.map