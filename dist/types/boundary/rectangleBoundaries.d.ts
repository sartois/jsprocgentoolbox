import { IAgent } from "../agents";
import { RectangleBoundaries } from ".";
import { TypedVector3 } from "../behaviors";
export declare function rectangleBoundaries(agent: IAgent, shape: RectangleBoundaries): void | TypedVector3;
//# sourceMappingURL=rectangleBoundaries.d.ts.map