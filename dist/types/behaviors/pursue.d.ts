import { IAgent } from "../agents";
import { FlockSettings } from "../store";
import { TypedVector3 } from ".";
export interface PursueSettings {
    /**
     * A simple estimator of moderate quality is T=Dc where D is the distance between pursuer and quarry, and c is a turning parameter.
     */
    c?: number;
    /**
     * The threshold from which agent will descelerate to arrive on target
     */
    threshold: number;
}
export declare function getPursueDefault(baseHeight: number): PursueSettings;
export declare function pursue(agent: IAgent, settings: FlockSettings): TypedVector3 | void;
//# sourceMappingURL=pursue.d.ts.map