import { IAgent } from "../../agents";
import { Behavior } from "..";
import { type FlockSettings } from "../../store";
interface BehaviorLoopParam {
    agent: IAgent;
    behaviors: Behavior[];
    flock: IAgent[];
    delta: number;
    settings: FlockSettings;
}
export declare function behaviorLoop({ agent, behaviors, flock, delta, settings, }: BehaviorLoopParam): void;
export {};
//# sourceMappingURL=behaviorLoop.d.ts.map