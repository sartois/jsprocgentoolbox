import { IAgent } from "../agents";
import { FlockSettings } from "../store";
import { TypedVector3 } from ".";
export interface EvadeSettings {
    /**
     * Distance from which agents start to evade
     */
    thresold: number;
    /**
     * A simple estimator of moderate quality is T=Dc where D is the distance between pursuer and quarry, and c is a turning parameter.
     */
    c: number;
}
export declare function getEvadeDefault(baseHeight: number): EvadeSettings;
export declare function evade(agent: IAgent, settings: FlockSettings): TypedVector3 | void;
//# sourceMappingURL=evade.d.ts.map