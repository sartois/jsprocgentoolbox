import { Behavior } from ".";
export type WanderSettings = {
    /**
     * The "wander sphere" radius
     */
    radius: number;
    /**
     * The "wander sphere" center position, following agent velocity
     */
    distance: number;
    /**
     * Maximun variation in radian
     */
    variation: number;
};
export declare function getWanderDefaults(baseHeight: number): WanderSettings;
export declare const wander: Behavior;
//# sourceMappingURL=wander.d.ts.map