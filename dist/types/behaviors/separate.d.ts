import { IAgent } from "../agents";
import { FlockSettings } from "../store";
import { TypedVector3 } from ".";
export type SeparateSettings = {
    threshold?: number;
    depth?: 0 | 1 | 2;
};
export declare function getSeparateDefaults(baseHeight: number): SeparateSettings;
export declare function separate(agent: IAgent, settings: FlockSettings, flock?: IAgent[]): TypedVector3 | void;
//# sourceMappingURL=separate.d.ts.map