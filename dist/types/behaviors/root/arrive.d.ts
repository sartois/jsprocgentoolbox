import { Vector3, Vector3Tuple } from "three";
import { IAgent } from "../../agents";
export declare function arrive(agent: IAgent, target: Vector3, threshold: number): Vector3Tuple;
//# sourceMappingURL=arrive.d.ts.map