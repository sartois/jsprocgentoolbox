import { Vector3, MathUtils, Object3D, Color, ArrowHelper, LineSegments, BufferGeometry, Float32BufferAttribute, LineBasicMaterial } from 'three';
import { proxy, ref } from 'valtio';
import { devtools, subscribeKey } from 'valtio/utils';
import merge from 'lodash.merge';
import { jsx, jsxs, Fragment } from 'react/jsx-runtime';
import { useRef, useMemo, Fragment as Fragment$1, forwardRef } from 'react';
import { useFrame, useThree, extend } from '@react-three/fiber';
import { mergeRefs } from 'react-merge-refs';
import { randomUniform, randomNormal } from 'd3-random';
import { createNoise3D, createNoise2D, createNoise4D } from 'simplex-noise';

class MovingAgent {
    constructor(agent, maxSpeed = 5, maxForce = 0.05, id = undefined) {
        Object.defineProperty(this, "maxSpeed", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: maxSpeed
        });
        Object.defineProperty(this, "maxForce", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: maxForce
        });
        Object.defineProperty(this, "acceleration", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "velocity", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "position", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "id", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.acceleration = agent.acceleration || new Vector3();
        this.velocity = agent.velocity || new Vector3();
        this.position = agent.position;
        this.id = id ?? ++MovingAgent.counter;
    }
    update(delta = 1) {
        this.acceleration.multiplyScalar(delta);
        this.velocity.add(this.acceleration);
        this.velocity.clampLength(0, this.maxSpeed);
        this.position.add(this.velocity);
        this.acceleration.multiplyScalar(0);
    }
    applyForce(force) {
        this.acceleration.add(force);
    }
}
Object.defineProperty(MovingAgent, "counter", {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 0
});

const up$1 = new Vector3(0, 1, 0);
const vel = new Vector3();
function alignVelocity(agent, mesh) {
    mesh.quaternion.setFromUnitVectors(up$1, vel.copy(agent.velocity).normalize());
}

function followPosition(agent, mesh, is2D = false) {
    mesh.position.set(agent.position.x, agent.position.y, is2D ? 0 : agent.position.z);
}

function behaviorLoop({ agent, behaviors, flock, delta, settings, }) {
    if (settings.neighbors.useGrid &&
        !behaviors.some((f) => f.name === "registerInGrid")) {
        logOnce("settings.neighbors.useGrid is true, but registerInGrid is not used in behaviors");
    }
    let forces = behaviors
        .map((behavior) => behavior(agent, settings, flock))
        .filter((v) => v instanceof Vector3);
    //avoid blinking
    if (behaviors.some((f) => f.name === "stay2D")) {
        forces.map((f) => f.setZ(0));
    }
    const { priorityBehavior, priorities } = settings.balance;
    if (priorityBehavior) {
        const primaryForce = forces.find((f) => f.kind === priorityBehavior);
        if (primaryForce) {
            forces = [primaryForce];
        }
    }
    if (priorities && priorities.length > 0) {
        const _prio = Object.fromEntries(priorities);
        forces.map((f) => _prio[f.kind]
            ? f.multiplyScalar(_prio[f.kind])
            : f);
    }
    forces.map((v) => agent.applyForce(v));
    agent.update(delta);
}
const logOnce = (function () {
    let done = false;
    return function (message) {
        if (!done) {
            done = true;
            console.warn(message);
        }
    };
})();

const desired$1 = new Vector3(), steer$1 = new Vector3();
//Compute a steering force towards a target
//Reynolds: STEER = TARGET - VELOCITY
//See http://www.red3d.com/cwr/papers/1999/gdc99steer.pdf
function seek(agent, target, clampLength) {
    if (clampLength === undefined)
        clampLength = true;
    desired$1
        .subVectors(target, agent.position)
        .normalize()
        .multiplyScalar(agent.maxSpeed);
    steer$1.subVectors(desired$1, agent.velocity);
    if (clampLength) {
        steer$1.clampLength(0, agent.maxForce);
    }
    return steer$1.toArray();
}

const bounceWorldCenter = new Vector3();
const bounceFutureFactor = 3;
function boxBoundaries(agent, shape, baseHeight) {
    if (shape.behavior === "torus") {
        return boxTorus(agent, shape.halfSize, baseHeight);
    }
    else {
        return boxBounce(agent, shape.halfSize);
    }
}
function boxTorus(agent, halfSize, baseHeight) {
    const position = agent.position.toArray();
    const min = -halfSize - baseHeight;
    const max = halfSize + baseHeight;
    let methodToCall = null;
    let arg = null;
    position.forEach((p, i) => {
        if (p > max || p < min) {
            methodToCall = i === 0 ? "setX" : i === 1 ? "setY" : "setZ";
            arg = p > max ? min : max;
        }
    });
    if (typeof methodToCall === "string") {
        //@ts-ignore
        agent.position[methodToCall](arg);
    }
}
function boxBounce(agent, halfSize) {
    const bouncer = new Vector3();
    bouncer.kind = BehaviorKind.Boundaries;
    const position = bouncer
        .copy(agent.velocity)
        .multiplyScalar(bounceFutureFactor)
        .add(agent.position)
        .toArray();
    let needToSeek = false;
    position.forEach((p) => {
        if (p > halfSize || p < -halfSize) {
            needToSeek = true;
        }
    });
    if (needToSeek) {
        return bouncer.fromArray(seek(agent, bounceWorldCenter, false));
    }
}

const curFlockState = {
    pursueTarget: undefined,
    evadeFrom: undefined,
    flock: [],
    viewport: undefined,
    debug: true,
    settings: undefined,
    is2D: false,
    flowfield: undefined,
};
const state = proxy(curFlockState);
const nodeEnv = process.env.NODE_ENV;
if (nodeEnv !== "test") {
    /*const unsub = */ devtools(state, { name: "state name", enabled: true });
}
function mergeSettingsWithDefault(settings) {
    if (!settings.agent?.height) {
        console.warn("settings.agent.height is not set, arbitrarily set base height to 25");
    }
    const baseHeight = settings.agent?.height ?? 25;
    const flockSettingsDefault = {
        world: {
            is2D: false,
        },
        agent: {
            maxSpeed: 5,
            maxForce: 0.05,
            height: baseHeight,
            count: 5,
        },
        boundaries: {
            ...getBoundariesDefaults(baseHeight),
        },
        wander: {
            ...getWanderDefaults(baseHeight),
        },
        separate: {
            ...getSeparateDefaults(baseHeight),
        },
        align: {
            ...getAlignDefaults(baseHeight),
        },
        cohesion: {
            ...getCohesionDefaults(baseHeight),
        },
        neighbors: {
            useGrid: false,
            gridCellSize: 0,
        },
        walk: {
            turnChance: 0,
            useSteppedAngle: false,
        },
        pursue: {
            ...getPursueDefault(baseHeight),
        },
        evade: {
            ...getEvadeDefault(baseHeight),
        },
        balance: {
            priorityBehavior: undefined,
            priorities: [],
        },
    };
    return merge(flockSettingsDefault, settings);
}

function createGrid(shape, size) {
    if (shape.kind === "rectangle") {
        createRectangleGrid(shape, size);
    }
    else {
        throw Error("Not yet implemented");
    }
}
const registerInGrid = (agent, settings) => {
    if (settings.boundaries?.shape.kind === "rectangle") {
        registerInRectangleGrid(agent);
    }
    else {
        throw Error("Not yet implemented");
    }
};
const rectangleGrid = {
    width: 0,
    height: 0,
    colsCount: 0,
    rowsCount: 0,
    cellSize: 0,
    content: [],
    behavior: "torus",
};
function getGrid(shape) {
    if (shape.kind === "rectangle") {
        return rectangleGrid;
    }
    else {
        throw Error("Not yet implemented");
    }
}
function createRectangleGrid(shape, size) {
    let remainder;
    const { width: _width, height: _height } = state.viewport;
    if (!_width || !_height) {
        return console.warn("Please provide correct viewport in state");
    }
    const gridCellSize = size.gridCellSize;
    const w = Math.ceil(_width), h = Math.ceil(_height);
    rectangleGrid.width =
        (remainder = w % gridCellSize) === 0 ? w : w + (gridCellSize - remainder);
    rectangleGrid.height =
        (remainder = h % gridCellSize) === 0 ? h : h + (gridCellSize - remainder);
    rectangleGrid.colsCount =
        gridCellSize > 0 ? rectangleGrid.width / gridCellSize : 0;
    rectangleGrid.rowsCount =
        gridCellSize > 0 ? rectangleGrid.height / gridCellSize : 0;
    if (rectangleGrid.colsCount < 3 || rectangleGrid.rowsCount < 3) {
        console.warn("Not enough rows or columns. Modify gridCellSize to have at least 3 columns and 3 rows.");
    }
    rectangleGrid.cellSize = gridCellSize;
    rectangleGrid.behavior = shape.behavior;
}
function getIndex([row, column]) {
    const index = row * rectangleGrid.colsCount + column;
    return index;
}
function getColAndRow(agent) {
    const cellSize = rectangleGrid.cellSize;
    const { width, height } = rectangleGrid;
    if (!cellSize || cellSize <= 0)
        throw Error("Please provide correct cellSize");
    let { x, y } = agent.position;
    if (x < 0)
        x = 0;
    if (x >= width)
        x = width - 1;
    if (y < 0)
        y = 0;
    if (y >= height)
        y = height - 1;
    const row = Math.floor(y / cellSize);
    const column = Math.floor(x / cellSize);
    return [row, column];
}
const registerInRectangleGrid = (agent) => {
    const tuple = getColAndRow(agent);
    const index = getIndex(tuple);
    if (!Array.isArray(rectangleGrid.content[index])) {
        rectangleGrid.content[index] = [];
    }
    rectangleGrid.content[index].push(agent);
};
function resetGrid() {
    for (let index = 0; index < rectangleGrid.content.length; index++) {
        rectangleGrid.content[index] = [];
    }
}
function getNeighboringCells([row, column]) {
    const tuples = [
        [row - 1, column - 1],
        [row - 1, column],
        [row - 1, column + 1],
        [row, column - 1],
        [row, column],
        [row, column + 1],
        [row + 1, column - 1],
        [row + 1, column],
        [row + 1, column + 1],
    ];
    if (rectangleGrid.behavior === "torus") {
        if (row === 0) {
            tuples[0][0] = rectangleGrid.rowsCount - 1;
            tuples[1][0] = rectangleGrid.rowsCount - 1;
            tuples[2][0] = rectangleGrid.rowsCount - 1;
        }
        if (row === rectangleGrid.rowsCount - 1) {
            tuples[6][0] = 0;
            tuples[7][0] = 0;
            tuples[8][0] = 0;
        }
        if (column === 0) {
            tuples[0][1] = rectangleGrid.colsCount - 1;
            tuples[3][1] = rectangleGrid.colsCount - 1;
            tuples[6][1] = rectangleGrid.colsCount - 1;
        }
        if (column === rectangleGrid.colsCount - 1) {
            tuples[2][1] = 0;
            tuples[5][1] = 0;
            tuples[8][1] = 0;
        }
    }
    else if (rectangleGrid.behavior === "bounce") {
        //delete create sparse array, which is exactly what we want
        if (row === 0) {
            delete tuples[0];
            delete tuples[1];
            delete tuples[2];
        }
        if (row === rectangleGrid.rowsCount) {
            delete tuples[6];
            delete tuples[7];
            delete tuples[8];
        }
        if (column === 0) {
            delete tuples[0];
            delete tuples[3];
            delete tuples[6];
        }
        if (column === rectangleGrid.colsCount) {
            delete tuples[2];
            delete tuples[5];
            delete tuples[8];
        }
    }
    return tuples;
}
function getNeighborsFromGrid(agent, depth = 0) {
    const [row, column] = getColAndRow(agent);
    let neighboringCells = [];
    if (depth > 1) {
        throw Error("Not yet implemented. getNeighborsFromGrid only works for depth 0 or 1");
    }
    else if (depth === 1) {
        neighboringCells = getNeighboringCells([row, column]);
    }
    else {
        neighboringCells = [[row, column]];
    }
    return neighboringCells
        .reduce((acc, tuple) => {
        if (tuple) {
            acc.push(getIndex(tuple));
        }
        return acc;
    }, [])
        .reduce((acc, index) => {
        const list = rectangleGrid.content[index];
        if (Array.isArray(list)) {
            acc = acc.concat(list);
        }
        return acc;
    }, []);
}

function getNeighbors({ agent, flock, threshold, depth, useGrid, }) {
    let neighbors = [];
    if (useGrid) {
        neighbors = getNeighborsFromGrid(agent, depth).filter((a) => a !== agent);
    }
    else {
        const squaredThreshold = threshold * threshold;
        //@todo return a tuple [agent, distanceToSquared] to avoid computing twice in separate ?
        neighbors = flock.filter((a) => a !== agent &&
            agent.position.distanceToSquared(a.position) <= squaredThreshold);
    }
    return neighbors;
}

const bounce2DWorldCenter = new Vector3();
function rectangleBoundaries(agent, shape) {
    if (state.viewport === undefined) {
        return console.warn("Please share viewport in state to use RectangleBoundaries");
    }
    if (shape.behavior === "centerAttraction") {
        return centerAttraction(agent, state.viewport, shape.centerAttractionFutureFactor ?? 1);
    }
    else if (shape.behavior === "torus") {
        return torus(agent, state.viewport);
    }
    else {
        return bounce(agent, state.viewport);
    }
}
function bounce(agent, { width, height }) {
    if (agent.position.x <= 0 || agent.position.x >= width) {
        agent.velocity.setX(agent.velocity.x * -1);
    }
    if (agent.position.y <= 0 || agent.position.y >= height) {
        agent.velocity.setY(agent.velocity.y * -1);
    }
}
function centerAttraction(agent, { width, height }, centerAttractionFutureFactor) {
    bounce2DWorldCenter.set(width / 2, height / 2, 0);
    const bouncer = new Vector3();
    bouncer.kind = BehaviorKind.Boundaries;
    bouncer
        .copy(agent.velocity)
        .multiplyScalar(centerAttractionFutureFactor)
        .add(agent.position);
    if (bouncer.x < 0 ||
        bouncer.x > width ||
        bouncer.y < 0 ||
        bouncer.y > height) {
        return bouncer.fromArray(seek(agent, bounce2DWorldCenter, false));
    }
}
function torus(agent, { width, height }) {
    if (agent.position.x < 0) {
        agent.position.setX(width);
    }
    if (agent.position.x > width) {
        agent.position.setX(0);
    }
    if (agent.position.y < 0) {
        agent.position.setY(height);
    }
    if (agent.position.y > height) {
        agent.position.setY(0);
    }
}

function sphereBoundaries(agent, shape, threshold) {
    if (threshold === undefined) {
        return console.warn("Boundaries threshold should be defined, no force will be applied");
    }
    const desired = new Vector3();
    desired.kind = BehaviorKind.Boundaries;
    const { radius } = shape;
    const center = { x: 0, y: 0, z: 0 };
    const distToCenter = agent.position.distanceTo(center);
    let multiplier = 0;
    desired.subVectors(center, agent.position);
    const overtaking = distToCenter - (radius - threshold);
    if (overtaking > 0) {
        multiplier = MathUtils.mapLinear(overtaking, 0, threshold, 0, agent.maxSpeed);
    }
    return desired
        .normalize()
        .multiplyScalar(multiplier); /*.clampLength(0, agent.maxForce)*/
}

function getBoundariesDefaults(baseHeight) {
    return {
        shape: {
            radius: baseHeight * 10,
            kind: "sphere",
        },
        threshold: baseHeight * 2,
    };
}
function stayWithinBoundaries(agent, settings) {
    if (settings.boundaries === undefined) {
        return console.warn("Please configure flock boundaries or remove stayWithinBoundaries behavior");
    }
    const { shape, threshold } = settings.boundaries;
    if (shape.kind === "sphere") {
        return sphereBoundaries(agent, shape, threshold);
    }
    else if (shape.kind === "box") {
        return boxBoundaries(agent, shape, settings.agent.height);
    }
    else if (shape.kind === "rectangle") {
        return rectangleBoundaries(agent, shape);
    }
}

function getAlignDefaults(baseHeight) {
    return {
        threshold: baseHeight * 2,
        depth: 1,
    };
}
//Compute neighbor average velocity
function align(agent, settings, flock) {
    const { useGrid } = settings.neighbors;
    const { threshold, depth } = settings.align;
    const neighbors = getNeighbors({ agent, flock, threshold, depth, useGrid });
    if (neighbors.length === 0)
        return;
    const sum = new Vector3();
    neighbors.forEach((a) => sum.add(a.velocity));
    sum
        .divideScalar(neighbors.length)
        .normalize()
        .multiplyScalar(agent.maxSpeed)
        .clampLength(0, agent.maxForce);
    sum.kind = BehaviorKind.Align;
    return sum;
}

function getCohesionDefaults(baseHeight) {
    return {
        threshold: baseHeight * 2,
        depth: 1,
    };
}
//steering vector towards average position of all nearby agent
function cohesion(agent, settings, flock) {
    const { useGrid } = settings.neighbors;
    const { threshold, depth } = settings.cohesion;
    const neighbors = getNeighbors({ agent, flock, threshold, depth, useGrid });
    if (neighbors.length === 0)
        return;
    const sum = new Vector3();
    sum.kind = BehaviorKind.Cohesion;
    neighbors.forEach((a) => sum.add(a.position));
    sum.divideScalar(neighbors.length);
    return sum.fromArray(seek(agent, sum));
}

const force = new Vector3();
function flee(agent, target) {
    return force.fromArray(seek(agent, target)).multiplyScalar(-1).toArray();
}

function getEvadeDefault(baseHeight) {
    return {
        thresold: baseHeight * 2,
        c: 0.05,
    };
}
let evadeFrom;
function evade(agent, settings) {
    subscribeOnce$1(() => {
        subscribeKey(state, "evadeFrom", (a) => {
            evadeFrom = a;
        });
    });
    const { thresold, c } = settings.evade;
    if (evadeFrom) {
        const d = agent.position.distanceToSquared(evadeFrom.position);
        if (d < thresold * thresold) {
            const v = new Vector3();
            v.kind = BehaviorKind.Evade;
            //see https://www.red3d.com/cwr/steer/gdc99/
            const T = Math.sqrt(d) * c;
            v.copy(evadeFrom.velocity).multiplyScalar(T).add(evadeFrom.position);
            v.fromArray(flee(agent, v));
            return v;
        }
    }
}
const subscribeOnce$1 = (function () {
    let done = false;
    return function (subscribe) {
        if (!done) {
            done = true;
            subscribe();
        }
    };
})();

//From Reynolds https://www.red3d.com/cwr/steer/gdc99/ :
//> The implementation of flow field following is very simple. The future position of a character is estimated and the flow field is sampled at that location. This flow direction (vector F in Figure 11) is the “desired velocity” and the steering direction (vector S) is simply the difference between the current velocity (vector V) and the desired velocity.
//To do that we should have a flowfield that extends behind boundaries, which is not the case
//Sample the flow field at current location
function followFlowfield(agent) {
    if (state.flowfield === undefined) {
        console.warn("Flowfield is undefined");
        return;
    }
    const { x, y, z } = agent.position;
    const noise = state.flowfield.lookup({ x, y, z });
    noise.kind = BehaviorKind.Flowfield;
    noise.setLength(agent.maxSpeed);
    noise.sub(agent.velocity).clampLength(0, agent.maxForce);
    return noise;
}

const desired = new Vector3(), steer = new Vector3();
function arrive(agent, target, threshold) {
    desired.subVectors(target, agent.position);
    const distance = desired.length();
    desired.normalize();
    desired.multiplyScalar(MathUtils.mapLinear(distance, 0, threshold, 0, agent.maxSpeed));
    steer.subVectors(desired, agent.velocity);
    steer.clampLength(0, agent.maxForce);
    return steer.toArray();
}

function getPursueDefault(baseHeight) {
    return {
        c: 0.005,
        threshold: baseHeight * 3,
    };
}
let target;
function pursue(agent, settings) {
    subscribeOnce(() => {
        subscribeKey(state, "pursueTarget", (t) => {
            target = t;
        });
    });
    const { c, threshold } = settings.pursue;
    if (!target) {
        return;
    }
    const T = agent.position.distanceTo(target.position) * (c ?? 1);
    const future = target.velocity
        .clone()
        .multiplyScalar(T)
        .add(target.position);
    future.kind = BehaviorKind.Pursue;
    return future.fromArray(arrive(agent, future, threshold));
}
const subscribeOnce = (function () {
    let done = false;
    return function (subscribe) {
        if (!done) {
            done = true;
            subscribe();
        }
    };
})();

function getSeparateDefaults(baseHeight) {
    return {
        threshold: baseHeight,
        depth: 0,
    };
}
const diff = new Vector3();
//Checks for nearby agents and steers away
//source https://github.com/nature-of-code/noc-examples-processing/blob/master/chp06_agents/NOC_6_09_Flocking/Boid.pde
function separate(agent, settings, flock) {
    const { threshold, depth } = settings.separate;
    const { useGrid } = settings.neighbors;
    const neighbors = getNeighbors({ agent, flock, threshold, depth, useGrid });
    if (neighbors.length === 0)
        return;
    const steer = new Vector3();
    let distinctCounter = 0;
    neighbors.forEach((a) => {
        const d = agent.position.distanceToSquared(a.position);
        if (d !== 0) {
            distinctCounter++;
            diff.subVectors(agent.position, a.position);
            diff.normalize().divideScalar(d);
            steer.add(diff);
        }
    });
    if (distinctCounter > 0) {
        steer.divideScalar(distinctCounter);
    }
    steer
        .normalize()
        .multiplyScalar(agent.maxSpeed)
        .sub(agent.velocity)
        .clampLength(0, agent.maxForce);
    steer.kind = BehaviorKind.Separate;
    return steer;
}

function stay2D(agent) {
    agent.position.setZ(0);
    agent.velocity.setZ(0);
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * @returns {Number}
 */
function random(min, max, rng = Math.random) {
    return rng() * (max - min) + min;
}

const cos45 = 0.707106781186548;
function walk(agent, settings, _, rng = Math.random) {
    const { turnChance, useSteppedAngle } = settings.walk;
    //Dir change chance
    if (turnChance > rng()) {
        if (state.is2D) {
            const v = {
                x: 0,
                y: 0,
                z: 0,
            };
            if (useSteppedAngle) {
                if (rng() > 0.5) {
                    v.x = cos45;
                    v.y = cos45;
                }
                else {
                    v.x = -cos45;
                    v.y = -cos45;
                }
            }
            else {
                const a = random(-Math.PI, Math.PI);
                v.x = Math.cos(a);
                v.y = Math.sin(a);
            }
            agent.velocity.add(v).normalize();
        }
        else {
            agent.velocity.randomDirection();
        }
    }
    const f = new Vector3();
    f.kind = BehaviorKind.Walk;
    return f
        .copy(agent.velocity)
        .normalize()
        .multiplyScalar(agent.maxSpeed)
        .clampLength(0, agent.maxForce);
}

function getWanderDefaults(baseHeight) {
    return {
        radius: baseHeight * 2,
        distance: baseHeight * 0.5,
        variation: 0.52, //Math.PI/6
    };
}
const memory = [];
const wander = (agent, settings) => {
    if (!memory[agent.id]) {
        memory[agent.id] = {
            lastTheta: 0,
            lastPhi: 0,
        };
    }
    let { lastTheta, lastPhi } = memory[agent.id];
    const { radius, distance, variation } = settings.wander;
    const halfVariation = variation / 2;
    const spherePos = new Vector3();
    spherePos.kind = BehaviorKind.Wander;
    //get pos for wander sphere
    spherePos
        .copy(agent.velocity)
        .normalize()
        .multiplyScalar(distance)
        .add(agent.position);
    //displacement
    lastTheta = lastTheta + random(-variation, variation);
    lastPhi = lastPhi + random(-halfVariation, halfVariation);
    memory[agent.id] = {
        lastTheta,
        lastPhi,
    };
    const offset = {
        x: radius * Math.cos(lastTheta) * Math.sin(lastPhi),
        y: radius * Math.sin(lastTheta) * Math.sin(lastPhi),
        z: radius * Math.cos(lastPhi),
    };
    spherePos.add(offset);
    return spherePos.fromArray(seek(agent, spherePos));
};

var BehaviorKind;
(function (BehaviorKind) {
    BehaviorKind["Align"] = "align";
    BehaviorKind["Boundaries"] = "boundaries";
    BehaviorKind["Cohesion"] = "cohesion";
    BehaviorKind["Evade"] = "evade";
    BehaviorKind["Flowfield"] = "flowfield";
    BehaviorKind["Pursue"] = "pursue";
    BehaviorKind["Separate"] = "separate";
    BehaviorKind["Wander"] = "wander";
    BehaviorKind["Walk"] = "walk";
})(BehaviorKind || (BehaviorKind = {}));

function Agent(props) {
    const { children, behaviors, disableGameLoop } = props;
    const agent = props.agent;
    const ref = useRef(null);
    const { flock, is2D, settings } = state;
    useFrame((_, delta) => {
        if (ref?.current && !disableGameLoop && settings) {
            behaviorLoop({ agent, behaviors, delta, flock, settings });
            alignVelocity(agent, ref.current);
            followPosition(agent, ref.current, is2D);
        }
    });
    return (jsx("group", { position: agent.position, ref: ref, children: children }));
}

function AgentWithFSM(props) {
    const ref = useRef(null);
    const agent = props.agent ?? new MovingAgent({ position: new Vector3() });
    const machine = props.machine;
    const behaviors = props.behaviors;
    const { flock, settings, is2D } = state;
    const _behaviors = Object.fromEntries(behaviors);
    useMemo(() => machine.start(), [machine]);
    useFrame((_, delta) => {
        if (ref?.current && settings) {
            behaviorLoop({
                agent,
                behaviors: _behaviors[machine.getCurrentState()],
                delta,
                settings,
                flock,
            });
            alignVelocity(agent, ref.current);
            followPosition(agent, ref.current, is2D);
        }
    });
    return (jsx("group", { position: agent.position, ref: ref, children: props.children }));
}

function Flock(props) {
    const { children, settings, render, distribution, agents, randomVelocity, sameVelocity, setFlock, } = props;
    //settings handling
    const mergedSettings = mergeSettingsWithDefault(settings ?? {});
    //flock creation
    const { maxSpeed, maxForce } = mergedSettings.agent;
    let flock = [];
    if (agents === undefined && distribution !== undefined) {
        flock = distribution.map((pos, i) => new MovingAgent({ position: new Vector3(pos.x, pos.y, pos.z) }, maxSpeed, maxForce, i));
        if (randomVelocity) {
            flock.map((agent) => agent.velocity.randomDirection());
        }
        if (sameVelocity) {
            flock.map((agent) => agent.velocity.copy(sameVelocity));
        }
    }
    else if (agents) {
        flock = agents;
    }
    //populate state
    const { viewport } = useThree();
    state.viewport = viewport;
    state.flock = ref(flock);
    state.settings = mergedSettings;
    state.is2D = mergedSettings.world.is2D;
    if (setFlock) {
        setFlock(flock);
    }
    //grid creation
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    let realResetGrid = () => { };
    if (mergedSettings.neighbors.useGrid &&
        mergedSettings.neighbors.gridCellSize) {
        createGrid(mergedSettings.boundaries.shape, mergedSettings.neighbors);
        realResetGrid = resetGrid;
    }
    useFrame(realResetGrid);
    return (jsxs(Fragment, { children: [render &&
                flock.map((agent) => (jsx(Fragment$1, { children: render(agent) }, agent.id))), children] }));
}

const up = new Vector3(0, 1, 0);
const temp = new Object3D();
const InstancedAgent = forwardRef((props, ref) => {
    const { children, behaviors, disableGameLoop } = props;
    const { flock, settings } = state;
    if (!flock || flock.length === 0) {
        throw Error("Please populate flock");
    }
    const instanceCount = flock.length;
    const localRef = useRef();
    useFrame((_, delta) => {
        if (localRef?.current) {
            if (!disableGameLoop && settings) {
                flock.map((agent) => {
                    behaviorLoop({ agent, behaviors, flock, delta, settings });
                });
            }
            flock.map((agent, i) => {
                temp.quaternion.setFromUnitVectors(up, agent.velocity.clone().normalize());
                temp.position.set(agent.position.x, agent.position.y, state.is2D ? 0 : agent.position.z);
                temp.updateMatrix();
                localRef.current?.setMatrixAt(i, temp.matrix);
            });
            localRef.current.instanceMatrix.needsUpdate = true;
        }
    });
    return (jsx("instancedMesh", { ref: mergeRefs([localRef, ref]), args: [undefined, undefined, instanceCount], children: children }));
});

class BoxDistributor /*extends Distributor*/ {
    constructor(halfSize) {
        Object.defineProperty(this, "size", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "halfSize", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        //super()
        this.size = halfSize * 2;
        this.halfSize = halfSize;
    }
    getUniform(agentNb) {
        const rng = randomUniform(this.size);
        const keys = ["x", "y", "z"];
        //@ts-ignore
        return this.loop(agentNb, () => Object.fromEntries(keys.map((n) => [n, rng() - this.halfSize])));
    }
    getEdgy(agentNb, config) {
        if (config === undefined) {
            config = { sigma: 0.13 };
        }
        const { sigma } = config;
        const rngGaussNorth = randomNormal(0, sigma), rngGaussSouth = randomNormal(1, sigma);
        return this.loop(agentNb, () => {
            const rngX = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth;
            const rngY = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth;
            const rngZ = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth;
            let x = 0, y = 0, z = 0;
            while (!(x > 0 && x < 1 && y > 0 && y < 1 && z > 0 && z < 1)) {
                if (!(x > 0 && x < 1))
                    x = rngX();
                if (!(y > 0 && y < 1))
                    y = rngY();
                if (!(z > 0 && z < 1))
                    z = rngZ();
            }
            return {
                x: MathUtils.mapLinear(x, 0, 1, 0, this.size) - this.halfSize,
                y: MathUtils.mapLinear(y, 0, 1, 0, this.size) - this.halfSize,
                z: MathUtils.mapLinear(z, 0, 1, 0, this.size) - this.halfSize,
            };
        });
    }
    getNormal(agentNb, config) {
        if (config === undefined) {
            config = { mu: 0.5, sigma: 0.1 };
        }
        const { mu, sigma } = config;
        const rngGauss = randomNormal(mu, sigma);
        return this.loop(agentNb, () => ({
            x: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.size) - this.halfSize,
            y: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.size) - this.halfSize,
            z: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.size) - this.halfSize,
        }));
    }
    loop(agentNb, callback) {
        return new Array(agentNb).fill(null).map(callback);
    }
}

const TwoPI$1 = Math.PI * 2;
class CircleDistributor /*extends Distributor*/ {
    constructor(radius) {
        Object.defineProperty(this, "radius", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        //super()
        this.radius = radius;
    }
    getUniform(agentNb) {
        return this.loop(agentNb, () => {
            const r = this.radius * Math.sqrt(Math.random());
            const theta = Math.random() * TwoPI$1;
            return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
        });
    }
    getEdgy(agentNb, config) {
        if (config === undefined) {
            config = { mu: 1, sigma: 0.2 };
        }
        const { mu, sigma } = config;
        const rngGauss = randomNormal(mu, sigma);
        return this.loop(agentNb, () => {
            let h = 0;
            while (h <= 0 || h >= 1) {
                h = rngGauss();
            }
            const r = this.radius * Math.sqrt(h);
            const theta = Math.random() * TwoPI$1;
            return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
        });
    }
    getNormal(agentNb, config) {
        if (config === undefined) {
            config = { mu: 0.1, sigma: 0.3 };
        }
        const { mu, sigma } = config;
        const rngGauss = randomNormal(mu, sigma);
        return this.loop(agentNb, () => {
            let h = 0;
            while (h <= 0 || h >= 1) {
                h = rngGauss();
            }
            const r = this.radius * Math.pow(h, 2);
            const theta = Math.random() * TwoPI$1;
            return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 };
        });
    }
    loop(agentNb, callback) {
        return new Array(agentNb).fill(null).map(callback);
    }
}

class RectangleDistributor /*extends Distributor*/ {
    constructor({ width, height }, originCentered = true) {
        Object.defineProperty(this, "width", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "height", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "halfWidth", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "halfHeight", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        //super()
        this.width = width;
        this.height = height;
        this.halfWidth = originCentered ? this.width / 2 : 0;
        this.halfHeight = originCentered ? this.height / 2 : 0;
    }
    getUniform(agentNb) {
        const rngX = randomUniform(this.width), rngY = randomUniform(this.height);
        return this.loop(agentNb, () => ({
            x: rngX() - this.halfWidth,
            y: rngY() - this.halfHeight,
            z: 0,
        }));
    }
    getNormal(agentNb, config) {
        if (config === undefined) {
            config = { mu: 0.5, sigma: 0.1 };
        }
        const { mu, sigma } = config;
        const rngGauss = randomNormal(mu, sigma);
        return this.loop(agentNb, () => ({
            x: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.width) - this.halfWidth,
            y: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.height) - this.halfHeight,
            z: 0,
        }));
    }
    getEdgy(agentNb, config) {
        if (config === undefined) {
            config = { sigma: 0.13 };
        }
        const { sigma } = config;
        const rngGaussNorth = randomNormal(0, sigma), rngGaussSouth = randomNormal(1, sigma);
        return this.loop(agentNb, () => {
            const rngX = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth;
            const rngY = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth;
            let x = 0, y = 0;
            while (!(x > 0 && x < 1 && y > 0 && y < 1)) {
                if (!(x > 0 && x < 1))
                    x = rngX();
                if (!(y > 0 && y < 1))
                    y = rngY();
            }
            return {
                x: MathUtils.mapLinear(x, 0, 1, 0, this.width) - this.halfWidth,
                y: MathUtils.mapLinear(y, 0, 1, 0, this.height) - this.halfHeight,
                z: 0,
            };
        });
    }
    loop(agentNb, callback) {
        return new Array(agentNb).fill(null).map(callback);
    }
}

const TwoPI = Math.PI * 2;
const dummy = new Vector3();
class SphereDistributor /* extends Distributor*/ {
    constructor(radius) {
        Object.defineProperty(this, "radius", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        //super()
        this.radius = radius;
    }
    getEdgy(agentNb, config) {
        if (config === undefined) {
            config = { mu: 1, sigma: 0.1 };
        }
        const { mu, sigma } = config;
        return this.getNormal(agentNb, { mu, sigma });
    }
    getUniform(agentNb) {
        return this.loop(agentNb, () => {
            const u = Math.random(), v = Math.random();
            const theta = u * TwoPI;
            const phi = Math.acos(2.0 * v - 1.0);
            const r = Math.cbrt(Math.random());
            const sinTheta = Math.sin(theta);
            const cosTheta = Math.cos(theta);
            const sinPhi = Math.sin(phi);
            const cosPhi = Math.cos(phi);
            const x = r * sinPhi * cosTheta;
            const y = r * sinPhi * sinTheta;
            const z = r * cosPhi;
            return {
                x: MathUtils.mapLinear(x, -1, 1, -this.radius, this.radius),
                y: MathUtils.mapLinear(y, -1, 1, -this.radius, this.radius),
                z: MathUtils.mapLinear(z, -1, 1, -this.radius, this.radius),
            };
        });
    }
    getNormal(agentNb, config) {
        if (config === undefined) {
            config = { mu: 0, sigma: 0.2 };
        }
        const { mu, sigma } = config;
        const rngGauss = randomNormal(mu, sigma);
        return this.loop(agentNb, () => {
            let r = 0;
            while (r <= 0 || r >= 1) {
                r = rngGauss();
            }
            r = MathUtils.mapLinear(r, 0, 1, 0, this.radius);
            dummy.randomDirection().multiplyScalar(r);
            return {
                x: dummy.x,
                y: dummy.y,
                z: dummy.z,
            };
        });
    }
    loop(agentNb, callback) {
        return new Array(agentNb).fill(null).map(callback);
    }
}

class Distributor {
    loop(agentNb, callback) {
        return new Array(agentNb).fill(null).map(callback);
    }
}

class SimplexFlowField2D {
    constructor() {
        Object.defineProperty(this, "noise3D", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "noise2D", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.noise3D = createNoise3D();
        this.noise2D = createNoise2D();
    }
    lookup(position, time) {
        let noise;
        if (time === undefined) {
            noise =
                this.noise2D(position.x * SimplexFlowField2D.scaleDownFactor, position.y * SimplexFlowField2D.scaleDownFactor) + 1;
        }
        else {
            noise =
                this.noise3D(position.x * SimplexFlowField2D.scaleDownFactor, position.y * SimplexFlowField2D.scaleDownFactor, time * SimplexFlowField2D.scaleDownFactor) + 1;
        }
        //Map from 0 to 720°, see https://www.bit-101.com/blog/2021/07/mapping-perlin-noise-to-angles/
        const mapped = MathUtils.mapLinear(noise, 0, 2, 0, SimplexFlowField2D.upperLimit);
        return new Vector3(Math.cos(mapped), Math.sin(mapped), 0);
    }
}
//720°
Object.defineProperty(SimplexFlowField2D, "upperLimit", {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 2 * Math.PI * 2
});
//trial and error
Object.defineProperty(SimplexFlowField2D, "scaleDownFactor", {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 0.0005
});

class SimplexFlowField3D {
    constructor() {
        Object.defineProperty(this, "noise3D", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "noise4D", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.noise3D = createNoise3D();
        this.noise4D = createNoise4D();
    }
    lookup(position, time) {
        let theta;
        let phi;
        if (time === undefined) {
            theta =
                this.noise3D(position.x * SimplexFlowField3D.scaleDownFactor, position.y * SimplexFlowField3D.scaleDownFactor, position.z * SimplexFlowField3D.scaleDownFactor) + 1;
            phi =
                this.noise3D((position.x + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor, (position.y + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor, (position.z + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor) + 1;
        }
        else {
            theta =
                this.noise4D(position.x * SimplexFlowField3D.scaleDownFactor, position.y * SimplexFlowField3D.scaleDownFactor, position.z * SimplexFlowField3D.scaleDownFactor, time * SimplexFlowField3D.scaleDownFactor) + 1;
            phi =
                this.noise4D((position.x + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor, (position.y + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor, (position.z + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor, (time + SimplexFlowField3D.shift) *
                    SimplexFlowField3D.scaleDownFactor) + 1;
        }
        //Map from 0 to 720°, see https://www.bit-101.com/blog/2021/07/mapping-perlin-noise-to-angles/
        const mappedTheta = MathUtils.mapLinear(theta, 0, 2, 0, SimplexFlowField3D.upperLimit);
        const mappedPhi = MathUtils.mapLinear(phi, 0, 2, 0, SimplexFlowField3D.upperLimit);
        return new Vector3(Math.cos(mappedTheta) * Math.sin(mappedPhi), Math.sin(mappedTheta) * Math.sin(mappedPhi), Math.cos(mappedPhi));
    }
}
//720°
Object.defineProperty(SimplexFlowField3D, "upperLimit", {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 2 * Math.PI * 2
});
//trial and error
Object.defineProperty(SimplexFlowField3D, "scaleDownFactor", {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 0.0005
});
Object.defineProperty(SimplexFlowField3D, "shift", {
    enumerable: true,
    configurable: true,
    writable: true,
    value: 40000
});

class FlowFieldHelper extends Object3D {
    constructor(flowField, gridColor = new Color(0xff00ff), arrowColor = new Color(0xff00ff), showGrid = false) {
        super();
        Object.defineProperty(this, "flowField", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: flowField
        });
        //@ts-ignore
        this.type = "FlowFieldHelper";
        //eslint-disable-next-line no-constant-condition
        if (showGrid && false) {
            this.add(new FlowFieldGrid(flowField, gridColor));
        }
        this.add(this.addArrows(arrowColor));
    }
    addArrows(arrowColor = new Color(0xff0000)) {
        const halfCell = this.flowField.cellSize / 2;
        const widthEnd = this.flowField.width;
        const heightEnd = this.flowField.height;
        const arrows = new Object3D();
        for (let i = halfCell; i < heightEnd; i += this.flowField.cellSize) {
            for (let j = halfCell; j < widthEnd; j += this.flowField.cellSize) {
                const dir = this.flowField.lookup({ x: j, y: i });
                if (dir != null && dir.length() > 0) {
                    arrows.add(new ArrowHelper(new Vector3(dir.x, dir.y, 0), new Vector3(j, i, 0), halfCell, arrowColor, 0.5 * halfCell, 0.4 * halfCell));
                }
            }
        }
        return arrows;
    }
}
//DONT USE NOW
//@todo fix it, cols are not displayed
//FPS very low
class FlowFieldGrid extends LineSegments {
    /**
     * @param {FlowField} flowField
     * @param {Color} color
     */
    constructor(flowField, color) {
        const vertices = [];
        const colors = [];
        let k = 0;
        for (let i = 0; i <= flowField.height; i += flowField.cellSize) {
            vertices.push(0, i, 0, flowField.width, i, 0);
            color.toArray(colors, k);
            k += 3;
            color.toArray(colors, k);
            k += 3;
        }
        for (let j = 0; j <= flowField.width; j += flowField.cellSize) {
            vertices.push(j, 0, 0, j, 0, flowField.height);
            color.toArray(colors, k);
            k += 3;
            color.toArray(colors, k);
            k += 3;
        }
        const geometry = new BufferGeometry();
        geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3));
        geometry.setAttribute("color", new Float32BufferAttribute(colors, 3));
        const material = new LineBasicMaterial({
            vertexColors: true,
            toneMapped: false,
        });
        super(geometry, material);
        //@ts-ignore
        this.type = "FlowFieldGrid";
    }
}
extend({ FlowFieldHelper, FlowFieldGrid });

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */


function __classPrivateFieldGet(receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
}

function __classPrivateFieldSet(receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

var _PreComputedFlowField_instances, _PreComputedFlowField_fill;
class PreComputedFlowField {
    constructor(_width, _height, cellSize, flowField) {
        _PreComputedFlowField_instances.add(this);
        Object.defineProperty(this, "cellSize", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: cellSize
        });
        Object.defineProperty(this, "flowField", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: flowField
        });
        Object.defineProperty(this, "width", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "height", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "cols", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "rows", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "fields", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: []
        });
        let remainder;
        const w = Math.ceil(_width), h = Math.ceil(_height);
        this.width =
            (remainder = w % cellSize) === 0 ? w : w + (cellSize - remainder);
        this.height =
            (remainder = h % cellSize) === 0 ? h : h + (cellSize - remainder);
        this.cols = cellSize > 0 ? this.width / cellSize : 0;
        this.rows = cellSize > 0 ? this.height / cellSize : 0;
        __classPrivateFieldGet(this, _PreComputedFlowField_instances, "m", _PreComputedFlowField_fill).call(this);
    }
    lookup(position) {
        if (this.cellSize <= 0)
            return new Vector3();
        if (position.x < 0 || position.x >= this.width)
            return new Vector3();
        if (position.y < 0 || position.y >= this.height)
            return new Vector3();
        const row = Math.floor(position.y / this.cellSize);
        const column = Math.floor(position.x / this.cellSize);
        const index = row * this.cols + column;
        return this.fields[index] instanceof Vector3
            ? this.fields[index].clone()
            : new Vector3();
    }
}
_PreComputedFlowField_instances = new WeakSet(), _PreComputedFlowField_fill = function _PreComputedFlowField_fill() {
    const { rows, cols, cellSize } = this;
    const halfCell = Math.round(cellSize / 2);
    const here = new Vector3();
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            here.set(j * cellSize + halfCell, i * cellSize + halfCell, 0);
            this.fields[i * cols + j] = this.flowField.lookup(here);
        }
    }
};

var _SimpleFSM_instances, _SimpleFSM_currentState, _SimpleFSM_findTransition;
class SimpleFSM extends EventTarget {
    constructor() {
        super(...arguments);
        _SimpleFSM_instances.add(this);
        _SimpleFSM_currentState.set(this, void 0);
        Object.defineProperty(this, "transitions", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "initial", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "hasStarted", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
    }
    setTransitions({ transitions, initial }) {
        this.transitions = transitions;
        this.initial = initial;
    }
    start() {
        if (!this.transitions)
            throw Error("Please call setTransitions before starting");
        if (!this.hasStarted) {
            const firstTransition = __classPrivateFieldGet(this, _SimpleFSM_instances, "m", _SimpleFSM_findTransition).call(this, this.initial);
            this.hasStarted = true;
            this.dispatch(firstTransition.name);
        }
    }
    dispatch(transitionName) {
        const transition = __classPrivateFieldGet(this, _SimpleFSM_instances, "m", _SimpleFSM_findTransition).call(this, transitionName);
        if (transition.before) {
            transition.before();
        }
        __classPrivateFieldSet(this, _SimpleFSM_currentState, transition.to, "f");
        this.dispatchEvent(new CustomEvent("MachineStateChanged", { detail: transition.to }));
    }
    getCurrentState() {
        return __classPrivateFieldGet(this, _SimpleFSM_currentState, "f");
    }
}
_SimpleFSM_currentState = new WeakMap(), _SimpleFSM_instances = new WeakSet(), _SimpleFSM_findTransition = function _SimpleFSM_findTransition(name) {
    const transition = this.transitions.find((t) => t.name === name);
    if (!transition) {
        throw Error(`Can't find transition ${name}`);
    }
    return transition;
};

export { Agent, AgentWithFSM, BehaviorKind, BoxDistributor, CircleDistributor, Distributor, Flock, FlowFieldGrid, FlowFieldHelper, InstancedAgent, MovingAgent, PreComputedFlowField, RectangleDistributor, SimpleFSM, SimplexFlowField2D, SimplexFlowField3D, SphereDistributor, align, alignVelocity, arrive, behaviorLoop, boxBoundaries, cohesion, createGrid, evade, flee, followFlowfield, followPosition, getAlignDefaults, getBoundariesDefaults, getCohesionDefaults, getEvadeDefault, getGrid, getNeighbors, getNeighborsFromGrid, getPursueDefault, getSeparateDefaults, getWanderDefaults, mergeSettingsWithDefault, pursue, rectangleBoundaries, registerInGrid, resetGrid, seek, separate, sphereBoundaries, state, stay2D, stayWithinBoundaries, walk, wander };
