import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { BehaviorKind, cohesion } from "../../src/behaviors"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { getNeighbors } from "../../src/boundary/getNeighbors"

jest.mock("../../src/boundary/getNeighbors", () => {
  return {
    getNeighbors: jest
      .fn(() => [])
      //@ts-ignore
      .mockImplementationOnce(() => [
        new MovingAgent({ position: new Vector3(1, 0, 0) }),
      ]) //test 1
      //@ts-ignore
      .mockImplementationOnce(() => [
        new MovingAgent({ position: new Vector3(1, 0, 0) }),
      ]) //test 2
      //@ts-ignore
      .mockImplementationOnce(() => [
        new MovingAgent({ position: new Vector3(1, 0, 0) }),
      ]), //test 2
  }
})

describe("cohesion", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent({ position: new Vector3(0, 1, 0) }, 1, 1)
    //@ts-ignore
    settings = mergeSettingsWithDefault({ agent: { height: 1 } })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should returns coherent computation", () => {
    const result = cohesion(agent, settings, [])
    expect(result).toBeInstanceOf(Vector3)
    expect(getNeighbors).toHaveBeenCalledTimes(1)

    const x = result!.x.toFixed(4)
    const y = result!.y.toFixed(4)

    expect(x).toBe((Math.sqrt(2) / 2).toFixed(4))
    expect(y).toBe((-Math.sqrt(2) / 2).toFixed(4))

    expect(result).toMatchObject({
      z: 0,
      kind: BehaviorKind.Cohesion,
    })
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = cohesion(agent, settings, [])
    const result2 = cohesion(agent, settings, [])

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(getNeighbors).toHaveBeenCalledTimes(2)
    expect(result1 !== result2).toBe(true)
  })

  it("should returns undefined if there's no neighbors", () => {
    const result = cohesion(agent, settings, [])

    expect(getNeighbors).toHaveBeenCalledTimes(1)
    expect(result).toBe(undefined)
  })
})
