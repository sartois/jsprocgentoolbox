import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { BehaviorKind, walk } from "../../src/behaviors"
import { FlockSettings, mergeSettingsWithDefault, state } from "../../src/store"

jest.mock("../../src/store", () => {
  const originalModule = jest.requireActual("../../src/store")
  return {
    ...originalModule,
    state: {
      is2D: true,
    },
  }
})

describe("walk", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent(
      { position: new Vector3(0, 1, 0), velocity: new Vector3(1, 0, 0) },
      1,
      1,
    )
    //@ts-ignore
    settings = mergeSettingsWithDefault({
      agent: { height: 1 },
      walk: { turnChance: 0, useSteppedAngle: false },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = walk(agent, settings, undefined, () => 1)
    const result2 = walk(agent, settings, undefined, () => 1)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })

  it("should copy velocity if turn chance is zero", () => {
    const result1 = walk(agent, settings, undefined, () => 1)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result1).toMatchObject({
      x: agent.velocity.x,
      y: agent.velocity.y,
      z: agent.velocity.z,
      kind: BehaviorKind.Walk,
    })
  })

  it("should update velocity if turn chance is 1", () => {
    //const velX = agent.velocity.x, velY = agent.velocity.y
    settings.walk.useSteppedAngle = true
    settings.walk.turnChance = 1
    const result1 = walk(agent, settings, undefined, () => 0.9)

    const x = Number(result1!.x.toFixed(4))
    const y = Number(result1!.y.toFixed(4))

    expect(x).toBe(0.9239)
    expect(y).toBe(0.3827)
    expect(result1!.kind).toBe(BehaviorKind.Walk)
  })

  it("should randomly update velocity if is2D is false, and turn chance is OK", () => {
    state.is2D = false
    const oldVelocityRD = agent.velocity.randomDirection
    agent.velocity.randomDirection = jest.fn()

    settings.walk.turnChance = 1
    walk(agent, settings, undefined, () => 0.9)

    expect(agent.velocity.randomDirection).toHaveBeenCalledTimes(1)

    agent.velocity.randomDirection = oldVelocityRD
  })
})
