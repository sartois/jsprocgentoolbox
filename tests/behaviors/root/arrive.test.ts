import { Vector3 } from "three"
import { MovingAgent } from "../../../src/agents"
import { arrive } from "../../../src/behaviors/root/arrive"
import { seek } from "../../../src/behaviors/root/seek"

describe("arrive", () => {
  let agent: MovingAgent, target: Vector3

  it("should behave like seek if target is further away than the threshold", () => {
    agent = new MovingAgent({ position: new Vector3() })
    target = new Vector3(1, 0, 0)

    const [x, y, z] = arrive(agent, target, 0.5)
    const [sx, sy, sz] = seek(agent, target)

    expect(x).toBe(sx)
    expect(y).toBe(sy)
    expect(z).toBe(sz)
  })

  it("should reduce force if target is close", () => {
    agent = new MovingAgent({ position: new Vector3() }, 5, 5)
    target = new Vector3(1, 0, 0)

    const [x, y, z] = arrive(agent, target, 5)
    const [sx] = seek(agent, target)

    expect(x).toBeLessThan(sx)
    expect(y).toBe(0)
    expect(z).toBe(0)
  })
})
