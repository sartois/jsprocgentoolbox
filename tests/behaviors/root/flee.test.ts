import { Vector3 } from "three"
import { MovingAgent } from "../../../src/agents"
import { flee } from "../../../src/behaviors/root/flee"

describe("flee", () => {
  let agent: MovingAgent, target: Vector3

  it("should computes correct force", () => {
    agent = new MovingAgent({ position: new Vector3() }, 1, 1)
    target = new Vector3(1, 0, 0)

    const [x, y, z] = flee(agent, target)

    expect(x).toBe(-1)
    expect(Math.abs(y)).toBe(0)
    expect(Math.abs(z)).toBe(0)
  })

  it("should clamp resulting force ", () => {
    agent = new MovingAgent({ position: new Vector3() }, 1)
    target = new Vector3(1, 0, 0)

    const [x, y, z] = flee(agent, target)

    expect(x).toBe(-agent.maxForce)
    expect(Math.abs(y)).toBe(0)
    expect(Math.abs(z)).toBe(0)
  })
})
