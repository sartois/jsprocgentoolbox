import { Vector3 } from "three"
import { MovingAgent } from "../../../src/agents"
import { seek } from "../../../src/behaviors/root/seek"

describe("seek", () => {
  let agent: MovingAgent, target: Vector3

  it("should computes correct force", () => {
    agent = new MovingAgent({ position: new Vector3() }, 1, 1)
    target = new Vector3(1, 0, 0)

    const [x, y, z] = seek(agent, target)

    expect(x).toBe(1)
    expect(y).toBe(0)
    expect(z).toBe(0)
  })

  it("should clamp resulting force ", () => {
    agent = new MovingAgent({ position: new Vector3() }, 1)
    target = new Vector3(1, 0, 0)

    const [x, y, z] = seek(agent, target)

    expect(x).toBe(agent.maxForce)
    expect(y).toBe(0)
    expect(z).toBe(0)
  })

  it("should allow to not clamped length", () => {
    agent = new MovingAgent({ position: new Vector3() }, 1)
    target = new Vector3(1, 0, 0)

    const [x, y, z] = seek(agent, target, false)

    expect(x).toBe(agent.maxSpeed)
    expect(y).toBe(0)
    expect(z).toBe(0)
  })
})
