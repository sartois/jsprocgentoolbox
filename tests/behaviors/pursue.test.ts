import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { BehaviorKind, pursue } from "../../src/behaviors"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { subscribeKey } from "valtio/utils"

const target = new MovingAgent({ position: new Vector3(1, 1, 0) })
let funcRef: any

jest.mock("valtio/utils", () => {
  return {
    subscribeKey: jest.fn((_, __, cb) => {
      funcRef = cb
      cb(target)
    }),
  }
})

describe("pursue", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent({ position: new Vector3(0, 1, 0) }, 1, 1)
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      evade: {
        thresold: 10,
        c: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should call subscribeKey once", () => {
    pursue(agent, settings)
    pursue(agent, settings)

    expect(subscribeKey).toHaveBeenCalledTimes(1)
    //@ts-ignore
    expect(subscribeKey.mock.calls[0][2]).toBe(funcRef)
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = pursue(agent, settings)
    const result2 = pursue(agent, settings)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })

  it("should returns coherent force", () => {
    const result = pursue(agent, settings)
    expect(result).toBeInstanceOf(Vector3)
    expect(result).toMatchObject({
      x: 1 / 3,
      y: 0,
      z: 0,
      kind: BehaviorKind.Pursue,
    })
  })

  it("should return undefined and warn if target is falsy", () => {
    funcRef(undefined)
    const result = pursue(agent, settings)
    expect(result).toBe(undefined)
  })
})
