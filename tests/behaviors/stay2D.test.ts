import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { stay2D } from "../../src/behaviors"

describe("stay2D", () => {
  let agent: MovingAgent

  beforeEach(() => {
    agent = new MovingAgent(
      { position: new Vector3(0, 1, 1), velocity: new Vector3(0, 1, 1) },
      1,
      1,
    )
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should set Z of position and velocity", () => {
    stay2D(agent)

    expect(agent.position.z).toBe(0)
    expect(agent.velocity.z).toBe(0)
  })
})
