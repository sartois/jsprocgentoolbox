import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { BehaviorKind, wander } from "../../src/behaviors"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"

describe("wander", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent(
      { position: new Vector3(0, 1, 0), velocity: new Vector3(1, 0, 0) },
      1,
      1,
    )
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      wander: {
        radius: 1,
        distance: 1,
        variation: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = wander(agent, settings)
    const result2 = wander(agent, settings)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })

  it("should returns coherent computation", () => {
    const result = wander(agent, settings)
    expect(result).toBeInstanceOf(Vector3)
    expect(result!.kind).toBe(BehaviorKind.Wander)
  })
})
