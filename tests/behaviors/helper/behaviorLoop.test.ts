import { Vector3 } from "three"
import { MovingAgent } from "../../../src/agents"
import { TypedVector3, behaviorLoop } from "../../../src/behaviors"
import { IAgent } from "../../../src/agents"
import { mergeSettingsWithDefault } from "../../../src/store"

describe("behaviorLoop", () => {
  const delta = 1
  const agent: IAgent = new MovingAgent({ position: new Vector3() })

  it("should warn if settings set useGrid but registerInGrid behavior is not used", () => {
    const oldWarn = console.warn
    console.warn = jest.fn()

    //@ts-ignore
    const settings = mergeSettingsWithDefault({
      agent: { height: 1 },
      neighbors: { useGrid: true },
    })

    behaviorLoop({ agent, flock: [], delta, settings, behaviors: [] })

    expect(console.warn).toHaveBeenCalledWith(
      "settings.neighbors.useGrid is true, but registerInGrid is not used in behaviors",
    )
    console.warn = oldWarn
  })

  it("should call behaviors", () => {
    const mockBehavior = jest.fn()
    //@ts-ignore
    const settings = mergeSettingsWithDefault({ agent: { height: 1 } })
    behaviorLoop({
      agent,
      behaviors: [mockBehavior],
      delta,
      settings,
      flock: [],
    })
    expect(mockBehavior.mock.calls).toHaveLength(1)
    expect(mockBehavior.mock.calls[0][0]).toBe(agent)
    expect(mockBehavior.mock.calls[0][1]).toBe(settings)
  })

  it("should avoid blinking in 2D", () => {
    const v = new Vector3(0, 0, 1)
    const oldSetZ = v.setZ
    v.setZ = jest.fn()

    function stay2D() {
      return v as TypedVector3
    }

    //@ts-ignore
    const settings = mergeSettingsWithDefault({ agent: { height: 1 } })
    behaviorLoop({ agent, behaviors: [stay2D], delta, settings, flock: [] })

    expect(v.setZ).toHaveBeenCalledTimes(1)
    expect(v.setZ).toHaveBeenCalledWith(0)

    v.setZ = oldSetZ
  })

  it("should respect priorityBehavior", () => {
    const oldApplyForce = agent.applyForce
    agent.applyForce = jest.fn()

    const expected = new Vector3(1, 1, 1) as TypedVector3
    expected.kind = "test"

    //@ts-ignore
    const settings = mergeSettingsWithDefault({
      agent: { height: 1 },
      balance: { priorityBehavior: "test" },
    })
    behaviorLoop({
      agent,
      behaviors: [
        () => {
          return expected
        },
        () => {
          const t = new Vector3(1, 1, 1) as TypedVector3
          t.kind = "not-test"
          return t
        },
      ],
      delta,
      settings,
      flock: [],
    })

    expect(agent.applyForce).toHaveBeenCalledTimes(1)
    expect(agent.applyForce).toHaveBeenCalledWith(expected)

    agent.applyForce = oldApplyForce
  })

  it("should respect priorities", () => {
    const expected = new Vector3(1, 1, 1) as TypedVector3
    expected.kind = "test"
    const oldMultiplyScalar = expected.multiplyScalar
    expected.multiplyScalar = jest.fn()

    //@ts-ignore
    const settings = mergeSettingsWithDefault({
      agent: { height: 1 },
      balance: { priorities: [["test", 2]] },
    })
    behaviorLoop({
      agent,
      behaviors: [
        () => {
          return expected
        },
      ],
      delta,
      settings,
      flock: [],
    })

    expect(expected.multiplyScalar).toHaveBeenCalledTimes(1)
    expect(expected.multiplyScalar).toHaveBeenCalledWith(2)

    expected.multiplyScalar = oldMultiplyScalar
  })

  it("should call applyForce and update", () => {
    const oldUpdate = agent.update
    agent.update = jest.fn()
    const oldApply = agent.applyForce
    agent.applyForce = jest.fn()

    const expected = new Vector3(1, 1, 1) as TypedVector3
    expected.kind = "test"

    //@ts-ignore
    const settings = mergeSettingsWithDefault({ agent: { height: 1 } })
    behaviorLoop({
      agent,
      behaviors: [
        () => {
          return expected
        },
      ],
      delta,
      settings,
      flock: [],
    })

    expect(agent.applyForce).toHaveBeenCalledTimes(1)
    expect(agent.applyForce).toHaveBeenCalledWith(expected)
    expect(agent.update).toHaveBeenCalledTimes(1)

    agent.applyForce = oldApply
    agent.update = oldUpdate
  })
})
