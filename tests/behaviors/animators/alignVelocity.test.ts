import { Object3D, Vector3 } from "three"
import { MovingAgent } from "../../../src/agents"
import { alignVelocity } from "../../../src/behaviors/animators/alignVelocity"

describe("alignVelocity", () => {
  it("should set a correct quaternion", () => {
    const o = new Object3D()
    const a = new MovingAgent({
      position: new Vector3(),
      velocity: new Vector3(1, 0, 0),
    })

    alignVelocity(a, o)

    const expected = 1 / Math.sqrt(2)

    expect(o.quaternion.x).toBe(0)
    expect(o.quaternion.y).toBe(0)
    expect(o.quaternion.z).toBe(-expected)
    expect(o.quaternion.w).toBe(expected)
  })
})
