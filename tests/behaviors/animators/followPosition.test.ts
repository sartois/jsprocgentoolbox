import { Object3D, Vector3 } from "three"
import { MovingAgent } from "../../../src/agents"
import { followPosition } from "../../../src/behaviors/animators/followPosition"

describe("followPosition", () => {
  it('should set a correct position with a "z" to 0 if 2D is true', () => {
    const o = new Object3D()
    const a = new MovingAgent({ position: new Vector3(1, 1, 1) })

    followPosition(a, o, true)

    expect(o.position.x).toBe(1)
    expect(o.position.y).toBe(1)
    expect(o.position.z).toBe(0)
  })

  it("should set a correct position if 2D is false", () => {
    const o = new Object3D()
    const a = new MovingAgent({ position: new Vector3(1, 1, 1) })

    followPosition(a, o)

    expect(o.position.x).toBe(1)
    expect(o.position.y).toBe(1)
    expect(o.position.z).toBe(1)
  })
})
