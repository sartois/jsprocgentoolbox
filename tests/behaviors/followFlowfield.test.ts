import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { BehaviorKind, followFlowfield } from "../../src/behaviors"
import { state } from "../../src/store"

jest.mock("../../src/store", () => {
  const originalModule = jest.requireActual("../../src/store")
  return {
    ...originalModule,
    state: {
      flowfield: {
        lookup: jest.fn(() => new Vector3(1, 0, 0)),
      },
    },
  }
})

describe("followFlowfield", () => {
  let agent: MovingAgent

  beforeEach(() => {
    agent = new MovingAgent(
      { position: new Vector3(0, 1, 0), velocity: new Vector3(1, 0, 0) },
      1,
      1,
    )
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = followFlowfield(agent)
    const result2 = followFlowfield(agent)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })

  it("should get flowfield from state", () => {
    followFlowfield(agent)
    expect(state.flowfield?.lookup).toHaveBeenCalledTimes(1)
  })

  it("should returns coherent computation", () => {
    const result = followFlowfield(agent)
    expect(result).toBeInstanceOf(Vector3)
    expect(result).toMatchObject({
      x: 0,
      y: 0,
      z: 0,
      kind: BehaviorKind.Flowfield,
    })
  })

  it("should warn if flowfield is undefined", () => {
    const oldWarn = console.warn
    console.warn = jest.fn()
    state.flowfield = undefined

    const result = followFlowfield(agent)

    expect(console.warn).toHaveBeenCalledWith("Flowfield is undefined")
    console.warn = oldWarn

    expect(result).toBe(undefined)
  })
})
