import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { BehaviorKind, evade } from "../../src/behaviors"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { subscribeKey } from "valtio/utils"

const evadeFrom = new MovingAgent({ position: new Vector3() })
let funcRef: any

jest.mock("valtio/utils", () => {
  return {
    subscribeKey: jest.fn((_, __, cb) => {
      funcRef = cb
      cb(evadeFrom)
    }),
  }
})

describe("evade", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent({ position: new Vector3(0, 1, 0) }, 1, 1)
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      evade: {
        thresold: 10,
        c: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should call subscribeKey once", () => {
    evade(agent, settings)
    evade(agent, settings)

    expect(subscribeKey).toHaveBeenCalledTimes(1)
    //@ts-ignore
    expect(subscribeKey.mock.calls[0][2]).toBe(funcRef)
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = evade(agent, settings)
    const result2 = evade(agent, settings)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })

  it("should returns coherent force", () => {
    const result = evade(agent, settings)
    expect(result).toBeInstanceOf(Vector3)
    expect(result).toMatchObject({
      x: -0,
      y: 1,
      z: -0,
      kind: BehaviorKind.Evade,
    })
  })

  it("should return undefined if evadeFrom is too far", () => {
    settings.evade.thresold = 0.5
    const result = evade(agent, settings)
    expect(result).toBe(undefined)
  })

  it("should return undefined if evadeFrom is falsy", () => {
    funcRef(null)
    const result = evade(agent, settings)
    expect(result).toBe(undefined)
  })
})
