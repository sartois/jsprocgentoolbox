import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { align } from "../../src/behaviors"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { getNeighbors } from "../../src/boundary/getNeighbors"

jest.mock("../../src/boundary/getNeighbors", () => {
  return {
    getNeighbors: jest
      .fn(() => []) //test 3
      //@ts-ignore
      .mockImplementationOnce(() => [
        new MovingAgent({
          position: new Vector3(),
          velocity: new Vector3(1, 0, 0),
        }),
      ]) //test 1
      //@ts-ignore
      .mockImplementationOnce(() => [
        new MovingAgent({
          position: new Vector3(),
          velocity: new Vector3(1, 0, 0),
        }),
      ]) //test 2
      //@ts-ignore
      .mockImplementationOnce(() => [
        new MovingAgent({
          position: new Vector3(),
          velocity: new Vector3(1, 0, 0),
        }),
      ]), //test 2
  }
})

describe("align", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent(
      { position: new Vector3(), velocity: new Vector3(1, 0, 0) },
      1,
      1,
    )
    //@ts-ignore
    settings = mergeSettingsWithDefault({ agent: { height: 1 } })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should returns coherent computation", () => {
    const result = align(agent, settings, [])
    expect(result).toBeInstanceOf(Vector3)
    expect(getNeighbors).toHaveBeenCalledTimes(1)
    expect(result).toMatchObject({
      x: 1,
      y: 0,
      z: 0,
      kind: "align",
    })
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = align(agent, settings, [])
    const result2 = align(agent, settings, [])

    expect(result1).toBeInstanceOf(Vector3)
    expect(getNeighbors).toHaveBeenCalledTimes(2)
    expect(result1 !== result2).toBe(true)
  })

  it("should returns undefined if there's no neighbors", () => {
    const result = align(agent, settings, [])

    expect(getNeighbors).toHaveBeenCalledTimes(1)
    expect(result).toBe(undefined)
  })
})
