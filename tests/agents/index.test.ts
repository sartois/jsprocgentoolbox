import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"

type MovingAgentKeys = keyof MovingAgent

describe("MovingAgent", () => {
  it("should have a valid constructor", () => {
    const v = new Vector3(),
      mv = new MovingAgent({
        acceleration: v,
        position: v,
        velocity: v,
      })

    ;["acceleration", "position", "velocity"].forEach((prop) => {
      expect(mv[prop as MovingAgentKeys]).toBe(v)
    })

    expect(mv.maxSpeed).toBe(5)
  })
  it("should add to acceleration when calling applyForce, and not updating anything else", () => {
    const v = new Vector3(1, 1, 1),
      mv = new MovingAgent({
        position: v,
      })

    mv.applyForce(new Vector3(1, 0, 0))
    expect(mv.acceleration).toMatchObject({ x: 1, y: 0, z: 0 })
    expect(mv.position).toMatchObject({ x: 1, y: 1, z: 1 })
    expect(mv.velocity).toMatchObject({ x: 0, y: 0, z: 0 })
  })
  it('when calling "update", should add acceleration to velocity, add velocity to position, and reset acceleration. MaxSpeed must also limit velocity.', () => {
    const mv = new MovingAgent({
      position: new Vector3(1, 1, 1),
    })
    mv.applyForce(new Vector3(6, 0, 0))
    mv.update()

    expect(mv.acceleration).toMatchObject({ x: 0, y: 0, z: 0 })
    expect(mv.velocity).toMatchObject({ x: 5, y: 0, z: 0 })
    expect(mv.position).toMatchObject({ x: 6, y: 1, z: 1 })
  })
})
