import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { SphereBoundaries, sphereBoundaries } from "../../src/boundary"
import { BehaviorKind } from "../../src/behaviors"

describe("sphereBoundaries", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent({ position: new Vector3(2, 2, 2) }, 1, 1)
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      boundaries: {
        shape: {
          radius: 2,
          kind: "sphere",
        },
        threshold: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should warn if threshold is undefined", () => {
    const oldWarn = console.warn
    console.warn = jest.fn()

    settings.boundaries.threshold = undefined

    const result = sphereBoundaries(
      agent,
      settings.boundaries.shape as SphereBoundaries,
      settings.boundaries.threshold,
    )

    expect(console.warn).toHaveBeenCalledWith(
      "Boundaries threshold should be defined, no force will be applied",
    )
    console.warn = oldWarn

    expect(result).toBe(undefined)
  })

  it("should return expected computation", () => {
    const result = sphereBoundaries(
      agent,
      settings.boundaries.shape as SphereBoundaries,
      settings.boundaries.threshold,
    )
    expect(result).toBeInstanceOf(Vector3)
    expect(result).toMatchObject({
      x: -1.4226497308103743,
      y: -1.4226497308103743,
      z: -1.4226497308103743,
      kind: BehaviorKind.Boundaries,
    })
  })

  it("should returns different V3 instances each time its called", () => {
    const result1 = sphereBoundaries(
      agent,
      settings.boundaries.shape as SphereBoundaries,
      settings.boundaries.threshold,
    )
    const result2 = sphereBoundaries(
      agent,
      settings.boundaries.shape as SphereBoundaries,
      settings.boundaries.threshold,
    )

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })
})
