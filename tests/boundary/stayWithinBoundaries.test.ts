import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { stayWithinBoundaries } from "../../src/boundary"
import { boxBoundaries } from "../../src/boundary/boxBoundaries"
import { sphereBoundaries } from "../../src/boundary/sphereBoundaries"
import { rectangleBoundaries } from "../../src/boundary/rectangleBoundaries"

jest.mock("../../src/boundary/boxBoundaries", () => {
  return {
    boxBoundaries: jest.fn(() => new Vector3()),
  }
})

jest.mock("../../src/boundary/sphereBoundaries", () => {
  return {
    sphereBoundaries: jest.fn(() => new Vector3()),
  }
})

jest.mock("../../src/boundary/rectangleBoundaries", () => {
  return {
    rectangleBoundaries: jest.fn(() => new Vector3()),
  }
})

describe("stayWithinBoundaries", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent(
      { position: new Vector3(0, 1, 0), velocity: new Vector3(1, 0, 0) },
      1,
      1,
    )
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      boundaries: {
        shape: {
          radius: 1,
          kind: "sphere",
        },
        threshold: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("should warn if boundary settings are missing", () => {
    const oldWarn = console.warn
    console.warn = jest.fn()
    //@ts-ignore
    settings.boundaries = undefined

    const result = stayWithinBoundaries(agent, settings)

    expect(console.warn).toHaveBeenCalledWith(
      "Please configure flock boundaries or remove stayWithinBoundaries behavior",
    )
    console.warn = oldWarn

    expect(result).toBe(undefined)
  })

  it("should call sphereBoundaries if shape.kind is sphere", () => {
    stayWithinBoundaries(agent, settings)

    expect(sphereBoundaries).toHaveBeenCalledTimes(1)
    expect(sphereBoundaries).toHaveBeenCalledWith(
      agent,
      settings.boundaries.shape,
      settings.boundaries.threshold,
    )
  })

  it("should call boxBoundaries if shape.kind is box", () => {
    settings.boundaries.shape.kind = "box"
    stayWithinBoundaries(agent, settings)

    expect(boxBoundaries).toHaveBeenCalledTimes(1)
    expect(boxBoundaries).toHaveBeenCalledWith(
      agent,
      settings.boundaries.shape,
      settings.boundaries.threshold,
    )
  })

  it("should call rectangleBoundaries if shape.kind is rectangle", () => {
    settings.boundaries.shape.kind = "rectangle"
    stayWithinBoundaries(agent, settings)

    expect(rectangleBoundaries).toHaveBeenCalledTimes(1)
    expect(rectangleBoundaries).toHaveBeenCalledWith(
      agent,
      settings.boundaries.shape,
    )
  })
})
