import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { RectangleBoundaries, rectangleBoundaries } from "../../src/boundary"
import { state } from "../../src/store"

jest.mock("../../src/store", () => {
  const originalModule = jest.requireActual("../../src/store")
  return {
    ...originalModule,
    state: {
      viewport: {
        width: 1920,
        height: 1080,
      },
    },
  }
})

describe("rectangleBoundaries", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent({ position: new Vector3(2, 2, 2) }, 1, 1)
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      boundaries: {
        shape: {
          behavior: "torus",
          kind: "rectangle",
        },
        threshold: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  //viewport in rectangleBoundaries is not undefined
  it.skip.failing("should warn if state.viewport is undefined", () => {
    const oldWarn = console.warn
    console.warn = jest.fn()

    state.viewport = undefined

    const result = rectangleBoundaries(
      agent,
      settings.boundaries.shape as RectangleBoundaries,
    )

    expect(console.warn).toHaveBeenCalledWith(
      "Please share viewport in state to use RectangleBoundaries",
    )
    expect(result).toBe(undefined)

    console.warn = oldWarn
    state.viewport = {
      width: 1920,
      height: 1080,
    }
  })

  //state is not mocked in rectangleBoundaries
  it.skip.failing(
    "torus should update agent position and return undefined",
    () => {
      state.viewport!.width = 2
      const result = rectangleBoundaries(
        agent,
        settings.boundaries.shape as RectangleBoundaries,
      )
      expect(result).toBeUndefined()
      expect(agent.position.x).toBe(-1)
    },
  )
})
