import { Vector3 } from "three"
import { MovingAgent } from "../../src/agents"
import { FlockSettings, mergeSettingsWithDefault } from "../../src/store"
import { BoxBoundaries, boxBoundaries } from "../../src/boundary"
import { BehaviorKind } from "../../src/behaviors"

describe("boxBoundaries", () => {
  let agent: MovingAgent, settings: FlockSettings

  beforeEach(() => {
    agent = new MovingAgent({ position: new Vector3() }, 1, 1)
    settings = mergeSettingsWithDefault({
      //@ts-ignore
      agent: { height: 1 },
      boundaries: {
        shape: {
          behavior: "torus",
          halfSize: 100,
          kind: "box",
        },
        threshold: 1,
      },
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("torus should update agent position and return undefined", () => {
    agent.position.setX(101)
    const result = boxBoundaries(
      agent,
      settings.boundaries.shape as BoxBoundaries,
      0,
    )
    expect(result).toBeUndefined()
    expect(agent.position.x).toBe(-100)

    agent.position.setX(-102)

    const result1 = boxBoundaries(
      agent,
      settings.boundaries.shape as BoxBoundaries,
      1,
    )
    expect(result1).toBeUndefined()
    expect(agent.position.x).toBe(101)

    agent.position.setZ(101)
    const result2 = boxBoundaries(
      agent,
      settings.boundaries.shape as BoxBoundaries,
      0,
    )
    expect(result2).toBeUndefined()
    expect(agent.position.z).toBe(-100)
  })

  it("bounce should create a unclamped go-to center vector", () => {
    agent.position.set(101, 0, 0)
    const shape = settings.boundaries.shape as BoxBoundaries
    shape.behavior = "bounce"
    const result1 = boxBoundaries(agent, shape, 0)
    expect(result1).toBeInstanceOf(Vector3)
    expect(result1!.kind).toBe(BehaviorKind.Boundaries)
    expect(result1).toMatchObject({
      x: -1,
      y: 0,
      z: 0,
    })
  })

  it("bounce should returns different V3 instances each time its called", () => {
    agent.position.set(101, 0, 0)
    const shape = settings.boundaries.shape as BoxBoundaries
    shape.behavior = "bounce"

    const result1 = boxBoundaries(agent, shape, 0)
    const result2 = boxBoundaries(agent, shape, 0)

    expect(result1).toBeInstanceOf(Vector3)
    expect(result2).toBeInstanceOf(Vector3)
    expect(result1 !== result2).toBe(true)
  })
})
