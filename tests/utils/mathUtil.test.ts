import { random } from "../../src/utils/mathUtil"

describe("mathUtil", () => {
  describe("random", () => {
    it("should have valid bounds", () => {
      expect(random(0, 1, () => 0)).toBe(0)
      expect(random(0, 1, () => 1)).toBe(1)
    })
  })
})
