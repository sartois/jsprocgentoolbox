import { Vector3 } from "three"
import {
  getGrid,
  createGrid,
  registerInGrid,
  getNeighborsFromGrid,
  resetGrid,
} from "../../src/boundary"
import { MovingAgent } from "../../src/agents"
import { state } from "../../src/store"

jest.mock("../../src/store", () => {
  const originalModule = jest.requireActual("../../src/store")
  return {
    ...originalModule,
    state: {
      viewport: {
        width: 20,
        height: 20,
      },
    },
  }
})

describe("gridPartionner", () => {
  const agents = [
    new MovingAgent({ position: new Vector3(-1, -1, 0) }),
    new MovingAgent({ position: new Vector3(15, -1, 0) }),
    new MovingAgent({ position: new Vector3(31, -1, 0) }),
    new MovingAgent({ position: new Vector3(-1, 15, 0) }),
    new MovingAgent({ position: new Vector3(10, 15, 0) }),
    new MovingAgent({ position: new Vector3(15, 15, 0) }),
    new MovingAgent({ position: new Vector3(20, 15, 0) }),
    new MovingAgent({ position: new Vector3(31, 15, 0) }),
    new MovingAgent({ position: new Vector3(-1, 31, 0) }),
    new MovingAgent({ position: new Vector3(15, 31, 0) }),
    new MovingAgent({ position: new Vector3(31, 31, 0) }),
  ]

  afterEach(() => {
    resetGrid()
  })

  it("createGrid should warn if not enough cells", () => {
    const oldWarn = console.warn
    console.warn = jest.fn()

    createGrid(
      {
        kind: "rectangle",
        behavior: "torus",
      },
      { gridCellSize: 10 },
    )

    expect(console.warn).toHaveBeenCalledWith(
      "Not enough rows or columns. Modify gridCellSize to have at least 3 columns and 3 rows.",
    )
    console.warn = oldWarn
  })

  it("createGrid should behave as expected", () => {
    state.viewport = {
      width: 29,
      height: 29,
    }

    createGrid(
      {
        kind: "rectangle",
        behavior: "torus",
      },
      { gridCellSize: 10 },
    )

    //@ts-ignore
    const rectangleGrid = getGrid({ kind: "rectangle" })

    expect(rectangleGrid).toMatchObject({
      width: 30,
      height: 30,
      colsCount: 3,
      rowsCount: 3,
      cellSize: 10,
      content: [],
      behavior: "torus",
    })
  })

  it("registerInGrid should place agents in the right place", () => {
    state.viewport = {
      width: 30,
      height: 30,
    }

    createGrid(
      {
        kind: "rectangle",
        behavior: "torus",
      },
      { gridCellSize: 10 },
    )

    agents.map((agent) => {
      registerInGrid(agent, {
        boundaries: {
          //@ts-ignore
          shape: {
            kind: "rectangle",
          },
        },
      })
    })

    //@ts-ignore
    const rectangleGrid = getGrid({ kind: "rectangle" })

    for (let index = 0; index < 9; index++) {
      const cell = rectangleGrid.content[index]

      if (index === 0) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[0])
      }
      if (index === 1) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[1])
      }
      if (index === 2) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[2])
      }
      if (index === 3) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[3])
      }
      if (index === 4) {
        expect(cell.length).toBe(2)
        expect(cell).toContain(agents[4])
        expect(cell).toContain(agents[5])
      }
      if (index === 5) {
        expect(cell.length).toBe(2)
        expect(cell).toContain(agents[6])
        expect(cell).toContain(agents[7])
      }
      if (index === 6) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[8])
      }
      if (index === 7) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[9])
      }
      if (index === 8) {
        expect(cell.length).toBe(1)
        expect(cell).toContain(agents[10])
      }
    }
  })

  it("getNeighborsFromGrid should returns correct agents if behavior is set to torus", () => {
    state.viewport = {
      width: 30,
      height: 30,
    }

    createGrid(
      {
        kind: "rectangle",
        behavior: "torus",
      },
      { gridCellSize: 10 },
    )

    agents.map((agent) => {
      registerInGrid(agent, {
        boundaries: {
          //@ts-ignore
          shape: {
            kind: "rectangle",
          },
        },
      })
    })

    //grid has 9 cells, so whatever where we are, cells contains all neighbors
    agents.map((agent) => {
      expect(getNeighborsFromGrid(agent, 1).length).toBe(agents.length)
    })
  })

  it("getNeighborsFromGrid should returns correct agents if behavior is set to bounce", () => {})
})
