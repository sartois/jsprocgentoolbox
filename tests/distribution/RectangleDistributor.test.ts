import { Position, RectangleDistributor } from "../../src/distribution"

describe("RectangleDistributor", () => {
  let rectangleDistributor: RectangleDistributor

  beforeEach(() => {
    rectangleDistributor = new RectangleDistributor({ width: 1, height: 1 })
  })

  it("getNormal results should have z set to 0 and x/y between width/height", () => {
    testRectangleDistributorResults(rectangleDistributor.getNormal(10))
  })
  it("getUniform results should have z set to 0 and x/y between width/height", () => {
    testRectangleDistributorResults(rectangleDistributor.getUniform(10))
  })
  it("getEdgy results should have z set to 0 and x/y between width/height", () => {
    testRectangleDistributorResults(rectangleDistributor.getEdgy(10))
  })
})

function testRectangleDistributorResults(results: Position[]) {
  expect(results.length).toBe(10)
  const sumZ = results.reduce(
    (accumulator, position) => accumulator + position.z,
    0,
  )
  expect(sumZ).toBe(0)
  results.forEach((position) => {
    Object.entries(position).forEach(([key, value]) => {
      if (key === "x" || key === "y") expect(value).toBeGreaterThan(-1)
      expect(value).toBeLessThanOrEqual(1)
    })
  })
}
