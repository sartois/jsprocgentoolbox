import { CircleDistributor } from "../../src/distribution/"

describe("CircleDistributor", () => {
  let circleDistributor: CircleDistributor
  it("getNormal", () => {
    circleDistributor = new CircleDistributor(0.5)
    const results = circleDistributor.getNormal(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-0.5)
        expect(value).toBeLessThanOrEqual(0.5)
      })
    })
  })
  it("getUniform", () => {
    circleDistributor = new CircleDistributor(0.5)
    const results = circleDistributor.getUniform(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-0.5)
        expect(value).toBeLessThanOrEqual(0.5)
      })
    })
  })
  it("getEdgy", () => {
    circleDistributor = new CircleDistributor(0.5)
    const results = circleDistributor.getEdgy(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-0.5)
        expect(value).toBeLessThanOrEqual(0.5)
      })
    })
  })
})
