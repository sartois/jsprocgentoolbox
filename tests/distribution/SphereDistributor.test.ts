import { SphereDistributor } from "../../src/distribution"

describe("SphereDistributor", () => {
  let sphereDistributor: SphereDistributor
  it("getNormal", () => {
    sphereDistributor = new SphereDistributor(0.5)
    const results = sphereDistributor.getNormal(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-0.5)
        expect(value).toBeLessThanOrEqual(0.5)
      })
    })
  })
  it("getUniform", () => {
    sphereDistributor = new SphereDistributor(0.5)
    const results = sphereDistributor.getUniform(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-0.5)
        expect(value).toBeLessThanOrEqual(0.5)
      })
    })
  })
  it("getEdgy", () => {
    sphereDistributor = new SphereDistributor(0.5)
    const results = sphereDistributor.getEdgy(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-0.5)
        expect(value).toBeLessThanOrEqual(0.5)
      })
    })
  })
})
