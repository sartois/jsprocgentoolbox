import { BoxDistributor } from "../../src/distribution/"

describe("CubeDistributor", () => {
  let cubeDistributor: BoxDistributor
  it("getNormal", () => {
    cubeDistributor = new BoxDistributor(1)
    const results = cubeDistributor.getNormal(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-1)
        expect(value).toBeLessThanOrEqual(1)
      })
    })
  })
  it("getUniform", () => {
    cubeDistributor = new BoxDistributor(1)
    const results = cubeDistributor.getUniform(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-1)
        expect(value).toBeLessThanOrEqual(1)
      })
    })
  })
  it("getEdgy", () => {
    cubeDistributor = new BoxDistributor(1)
    const results = cubeDistributor.getEdgy(10)
    expect(results.length).toBe(10)
    results.forEach((position) => {
      Object.entries(position).forEach(([, value]) => {
        expect(value).toBeGreaterThan(-1)
        expect(value).toBeLessThanOrEqual(1)
      })
    })
  })
})
