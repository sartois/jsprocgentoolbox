import { Canvas } from "@react-three/fiber"
import {
  GizmoHelper,
  GizmoViewport,
  CameraControls,
  StatsGl,
} from "@react-three/drei"
import { CSSProperties, ReactNode } from "react"

export type CameraProps = {
  fov: number
  near: number
  far: number
  position: [number, number, number]
}

export interface ThreeDCanvasProps {
  wrapperStyle: CSSProperties
  children: ReactNode
  debug?: boolean
  addLight: boolean
  camera?: CameraProps
  controls?: boolean
}

export const ThreeDCanvas = ({
  wrapperStyle,
  children,
  debug = false,
  addLight = true,
  camera = { fov: 75, near: 0.1, far: 1000, position: [0, 10, 50] },
  controls = true,
}: ThreeDCanvasProps) => {
  return (
    <>
      <Canvas camera={camera} style={wrapperStyle}>
        {controls && <CameraControls />}
        <StatsGl />
        {debug && (
          <>
            <GizmoHelper margin={[50, 50]}>
              <GizmoViewport />
            </GizmoHelper>
            <axesHelper args={[25]} />
            <gridHelper args={[100]} />
          </>
        )}
        {addLight && (
          <>
            <hemisphereLight args={["white", "darkslategrey", 2]} />
            <directionalLight castShadow={true} intensity={5} color={"white"} />
          </>
        )}
        {children}
      </Canvas>
    </>
  )
}
