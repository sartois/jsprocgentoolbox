import { Canvas, useThree } from "@react-three/fiber"
import {
  StatsGl,
  GizmoHelper,
  GizmoViewport,
  CameraControls,
  OrthographicCamera,
} from "@react-three/drei"
import { CSSProperties, ReactNode } from "react"

export interface TwoDCanvasProps {
  wrapperStyle: CSSProperties
  children: ReactNode
  debug?: boolean
  preserveDrawingBuffer?: boolean
}

export const TwoDCanvas = ({
  wrapperStyle,
  children,
  debug = false,
  preserveDrawingBuffer = false,
}: TwoDCanvasProps) => (
  <Canvas
    style={wrapperStyle}
    gl={{ preserveDrawingBuffer: preserveDrawingBuffer }}
  >
    <CameraControls />
    {preserveDrawingBuffer && <DisableAutoClearColor />}
    {debug && (
      <>
        <StatsGl />
        <GizmoHelper margin={[50, 50]}>
          <GizmoViewport />
        </GizmoHelper>
        <axesHelper args={[25]} />
        <gridHelper args={[100]} />
      </>
    )}
    <TwoDCam />
    {children}
  </Canvas>
)

function DisableAutoClearColor() {
  const { gl } = useThree()

  gl.autoClearColor = false
  return null
}

//Create a world with 1 three unit = 1 canvas unit
//And origin in the top left corner like a normal 2D canvas
function TwoDCam() {
  const {
    size: { width, height },
  } = useThree()

  return (
    <OrthographicCamera
      makeDefault
      zoom={1}
      top={0}
      bottom={height}
      left={0}
      right={width}
      near={-10}
      far={10}
    />
  )
}
