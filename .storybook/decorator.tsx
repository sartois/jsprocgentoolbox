import React from "react"
import { TwoDCanvas } from "./World/2DCanvas"
import { ThreeDCanvas } from "./World/3DCanvas"

export const Decorator = (Story, options) => {
  const { parameters, args } = options
  const is2D = args.settings?.world?.is2D ?? false
  return (
    <>
      {parameters.addHtml && <parameters.addHtml />}
      {is2D ? (
        <TwoDCanvas
          debug={parameters.debug}
          wrapperStyle={{
            minHeight: "calc(100vh - 2rem)",
            height: "calc(100vh - 2rem)",
            width: "calc(100vw - 2rem)",
            backgroundColor: parameters.backgroundColor,
          }}
          preserveDrawingBuffer={parameters.preserveDrawingBuffer}
        >
          <Story />
        </TwoDCanvas>
      ) : (
        <ThreeDCanvas
          debug={parameters.debug}
          camera={parameters.camera}
          controls={parameters.controls}
          wrapperStyle={{
            minHeight: "calc(100vh - 2rem)",
            height: "calc(100vh - 2rem)",
            width: "calc(100vw - 2rem)",
            backgroundColor: parameters.backgroundColor,
          }}
          addLight={parameters.addLight}
        >
          <Story />
        </ThreeDCanvas>
      )}
    </>
  )
}
