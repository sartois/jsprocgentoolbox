import type { StorybookConfig } from "@storybook/react-vite"
import { mergeConfig } from "vite"
import glsl from "vite-plugin-glsl"

const config: StorybookConfig = {
  stories: [
    "../stories/**/*.mdx",
    "../stories/**/*.stories.@(js|jsx|mjs|ts|tsx)",
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
  ],
  framework: {
    name: "@storybook/react-vite",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  staticDirs: ["../stories/public"],
  async viteFinal(config) {
    return mergeConfig(config, {
      plugins: [glsl()],
    })
  },
}
export default config
