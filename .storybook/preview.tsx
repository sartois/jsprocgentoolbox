import type { Preview } from "@storybook/react"
import { Decorator } from "./decorator"

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
  decorators: [Decorator],
}

export default preview
