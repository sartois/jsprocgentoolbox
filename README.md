Implements Steering Behaviors For Autonomous Characters by **Craig Reynolds** (https://www.red3d.com/cwr/steer/gdc99/) in Javascript, using Three.js and React Three Fiber.

```jsx
<Flock distribution={distributor.getUniform(count)}>
  <InstancedAgent behaviors={[align, cohesion, separate]}>
    <cylinderGeometry args={[0, 3, height, 3]} />
    <meshStandardMaterial color={dark} roughness={0.7} />
  </InstancedAgent>
</Flock>
```
