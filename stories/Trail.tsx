import React, { useMemo } from "react"
import { Meta, StoryObj } from "@storybook/react"
import { Color, Vector3 } from "three"
import { useThree, extend, useFrame } from "@react-three/fiber"
import { SimplexFlowField2D } from "../src/FlowField/SimplexFlowField2D"
import { PreComputedFlowField } from "../src/FlowField/helpers/PreComputedFlowField"
import { InstancedAgent } from "../src/Agents/components/InstancedAgent"
import { Flock } from "../src/Agents/components/Flock"
import { FlowFieldHelper } from "../src/FlowField/helpers/FlowFieldHelper"
import { stay2D, separate } from "../src/Agents/behaviors"
import { registerInGrid, stayWithinBoundaries } from "../src/boundary"
import { RectangleDistributor } from "../src/distribution"
import { followFlowfield } from "../src/Agents/behaviors/followFlowfield"
import { FlockSettings, state } from "../src/store"
import { Agent } from "../src/Agents/components/Agent"
import { Trail } from "../src/utils/Trail"

extend({ FlowFieldHelper })

const colors = {
  lightYellow: "#FFFADD",
  yellow: "#FFCC70",
  lightBlue: "#8ECDDD",
  blue: "#22668D",
}

const meta: Meta<typeof FlowFieldWithTrail> = {
  title: "Flowfield 2D with trail",
  component: FlowFieldWithTrail,
}

export default meta

type Story = StoryObj<typeof FlowFieldWithTrail>

const settings: Partial<FlockSettings> = {
  agent: {
    maxSpeed: 5,
    maxForce: 0.05,
    height: 25,
    count: 50,
  },
  boundaries: {
    shape: {
      kind: "rectangle",
      behavior: "torus",
    },
    threshold: 0,
  },
  neighbors: {
    useGrid: true,
    gridCellSize: 25,
  },
  world: {
    is2D: true,
  },
}

export const Basic: Story = {
  args: {
    settings,
    showFlowfield: false,
  },
  parameters: {
    debug: true,
    backgroundColor: colors.lightYellow,
  },
}

interface SimplexFlowField2DDemoProps {
  settings: Partial<FlockSettings>
  showFlowfield: boolean
}

function FlowFieldWithTrail({ settings }: SimplexFlowField2DDemoProps) {
  const {
    viewport: { width, height },
  } = useThree()

  const simplexFF = useMemo(() => new SimplexFlowField2D(), [])

  const FF = useMemo(
    () => new PreComputedFlowField(width, height, 50, simplexFF),
    [width, height, simplexFF],
  )

  state.flowfield = FF

  const rectangleDistributor = new RectangleDistributor(
    { width, height },
    false,
  )

  useFrame(() => {
    //console.log("test");
  })

  return (
    <Flock
      distribution={rectangleDistributor.getUniform(settings.agent!.count)}
      settings={settings}
      randomVelocity={false}
      render={(agent) => (
        <Trail
          width={0.1} // Width of the line
          color={agent.id % 2 === 0 ? colors.blue : colors.yellow} // Color of the line
          length={3} // Length of the line
          decay={1} // How fast the line fades away
          local={false} // Wether to use the target's world or local positions
          stride={0} // Min distance between previous and current point
          interval={1} // Number of frames to wait before next calculation
          target={undefined} // Optional target. This object will produce the trail.
          attenuation={(width) => width} // A function to define the width in each point along it.
        >
          <Agent
            agent={agent}
            behaviors={[followFlowfield, stayWithinBoundaries, stay2D]}
          >
            <mesh>
              <sphereGeometry args={[2]} />
              <meshBasicMaterial
                color={agent.id % 2 === 0 ? colors.blue : colors.yellow}
              />
            </mesh>
          </Agent>
        </Trail>
      )}
    />
  )
}
