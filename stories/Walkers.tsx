import React from "react"
import type { Meta, StoryObj } from "@storybook/react"
import { FlockSettings } from "../src/store"
import { useThree } from "@react-three/fiber"
import { RectangleDistributor } from "../src/distribution"
import { MovingAgent } from "../src/agents"
import { Vector3 } from "three"
import { Flock, Agent } from "../src/components"
import { walk, stay2D } from "../src/behaviors"
import { getRandomVelocity } from "../src/utils/getRandomVelocity"
import { stayWithinBoundaries } from "../src/boundary"

const colors = {
  ochre: "#F6BD60",
  offwhite: "#F7EDE2",
  palerose: "#F5CAC3",
  deepgreen: "#84A59D",
  vibrantpink: "#F28482",
}

const meta: Meta<typeof Walkers> = {
  title: "Walkers",
  component: Walkers,
}

export default meta

type Story = StoryObj<typeof Walkers>

const customSettings: Partial<FlockSettings> = {
  agent: {
    maxSpeed: 2,
    maxForce: 0.05,
    height: 25,
    count: 10,
  },
  boundaries: {
    shape: {
      kind: "rectangle",
      behavior: "torus",
    },
  },
  walk: {
    turnChance: 0.01,
    useSteppedAngle: true,
  },
  world: {
    is2D: true,
  },
}

export const TwoD: Story = {
  args: {
    settings: customSettings,
  },
  parameters: {
    debug: true,
    backgroundColor: colors.palerose,
    preserveDrawingBuffer: true,
  },
}

function Walkers(props) {
  const settings = props.settings as Partial<FlockSettings>

  const { maxSpeed, maxForce } = settings.agent!

  const { viewport } = useThree()
  const rectangleDistributor = new RectangleDistributor(viewport, false)

  const distribution = rectangleDistributor.getEdgy(settings.agent!.count)

  const flock = distribution.map(
    (p) =>
      new MovingAgent(
        {
          position: new Vector3(p.x, p.y, 0),
          velocity: getRandomVelocity(
            settings.walk!.useSteppedAngle,
            settings.world!.is2D,
          ),
        },
        maxSpeed,
        maxForce,
      ),
  )

  return (
    <Flock
      agents={flock}
      settings={settings}
      render={(agent) => (
        <Agent agent={agent} behaviors={[stayWithinBoundaries, walk, stay2D]}>
          <mesh>
            <sphereGeometry args={[1]} />
            <meshStandardMaterial color={colors.vibrantpink} />
          </mesh>
        </Agent>
      )}
    />
  )
}
