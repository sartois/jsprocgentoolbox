import { IAgent } from "../../src/agents"
import { random } from "../../src/utils/mathUtil"
import { SimpleFSM } from "../../src/machine/SimpleFSM"
import { state } from "../../src/store"

function dispatchAfter(delay: number, nextTransition: string, machine) {
  setTimeout(() => {
    machine.dispatch(nextTransition)
  }, delay)
}

export function getTarget(flock: IAgent[]): IAgent {
  const target = flock[Math.round(random(0, flock.length - 1))]
  return target
}

function fallback(agent, machine) {
  //set the walk velocity. Maybe chose the closest screen edge ?
  agent.velocity.randomDirection()
  //start a timer for rand time then dispatch rest
  dispatchAfter(random(3000, 6000), "lie down", machine)
}

function attack(_, machine) {
  state.pursueTarget = getTarget(state.flock as IAgent[])
  dispatchAfter(random(10000, 15000), "back off", machine)
}

function rest(agent, machine) {
  agent.velocity.set(0, 0, 0)
  dispatchAfter(random(3000, 5000), "wake up", machine)
}

let machine: SimpleFSM

export function createHunterFSM(agent: IAgent) {
  if (!(machine instanceof SimpleFSM)) {
    machine = new SimpleFSM()
    const [onAttack, onFallback, onRest] = [attack, fallback, rest].map((f) =>
      f.bind(null, agent, machine),
    )
    machine.setTransitions({
      transitions: [
        { name: "wake up", to: "attack", before: onAttack },
        { name: "back off", to: "fallback", before: onFallback },
        { name: "lie down", to: "rest", before: onRest },
      ],
      initial: "wake up",
    })
  }

  return machine
}
