import React from "react"
import type { Meta, StoryObj } from "@storybook/react"
import { useThree } from "@react-three/fiber"
import { Agent } from "../src/Agents/components/Agent"
import { wander, stay2D, getWanderDefaults } from "../src/Agents/behaviors"
import { Flock } from "../src/Agents/components/Flock"
import { SphereDistributor, RectangleDistributor } from "../src/distribution"
import { FlockSettings } from "../src/store"
import { stayWithinBoundaries } from "../src/boundary"

const colors = {
  blue: "#282673",
  yellow: "#F2B705",
  naplesYellow: "#F29F05",
  deepYellow: "#D97904",
  white: "#F2F2F2",
}

const meta: Meta<typeof AgentDemo> = {
  title: "Wandering Agent",
  component: AgentDemo,
}

export default meta

type Story = StoryObj<typeof AgentDemo>

function getSettings(is2D: boolean): Partial<FlockSettings> {
  return {
    agent: {
      maxSpeed: 1,
      maxForce: 0.01,
      height: 25,
      count: 10,
    },
    wander: { ...getWanderDefaults(25) },
    world: {
      is2D,
    },
  }
}

export const Basic: Story = {
  args: {
    settings: getSettings(false),
  },
  parameters: {
    backgroundColor: colors.white,
    debug: true,
    camera: { fov: 60, near: 0.1, far: 500, position: [0, 75, 250] },
  },
}

export const TwoD: Story = {
  args: {
    settings: getSettings(true),
  },
  parameters: {
    debug: true,
    backgroundColor: colors.white,
  },
}

function AgentDemo(props) {
  const { settings } = props
  const behaviors = [wander, stayWithinBoundaries]
  let distributor

  const {
    viewport: { width, height },
  } = useThree()

  if (settings.world.is2D) {
    settings.boundaries = {
      shape: {
        kind: "rectangle",
        behavior: "torus",
      },
    }

    distributor = new RectangleDistributor({ width, height }, false)
    behaviors.push(stay2D)
  } else {
    settings.boundaries = {
      shape: {
        kind: "sphere",
        radius: 200,
      },
    }
    distributor = new SphereDistributor(settings.boundaries.shape.radius)
  }

  return (
    <>
      {!settings.world.is2D && (
        <mesh>
          <sphereGeometry args={[settings.boundaries.shape.radius]} />
          <meshStandardMaterial wireframe={true} />
        </mesh>
      )}

      <Flock
        distribution={distributor.getUniform(settings.agent.count)}
        settings={settings}
        render={(agent) => (
          <Agent agent={agent} behaviors={behaviors}>
            <mesh>
              <coneGeometry args={[5, settings.agent.height]} />
              <meshStandardMaterial color={colors.blue} />
            </mesh>
          </Agent>
        )}
      />
    </>
  )
}
