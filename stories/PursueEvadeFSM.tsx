import React, { useEffect, useRef } from "react"
import { Color, InstancedMesh, Vector3 } from "three"
import { Meta, StoryObj } from "@storybook/react"
import { useFrame, useThree } from "@react-three/fiber"
import { subscribeKey } from "valtio/utils"
import { Flock } from "../src/Agents/components/Flock"
import { InstancedAgent } from "../src/Agents/components/InstancedAgent"
import { AgentWithFSM } from "../src/Agents/components/AgentWithFSM"
import { evade, pursue, walk, stay2D, Behavior } from "../src/Agents/behaviors"
import { MovingAgent } from "../src/Agents/MovingAgent"
import { createHunterFSM } from "./helper/hunterMachine"
import { FlockSettings, state } from "../src/store"
import { RectangleDistributor } from "../src/distribution"
import { Agent } from "../src/Agents/Agent"
import { stayWithinBoundaries } from "../src/boundary"

const colors = {
  Navy: "#364F6B",
  Teal: "#3FC1C9",
  White: "#F5F5F5",
  Pink: "#FC5185",
}

const Teal = new Color(colors.Teal)
const Navy = new Color(colors.Navy)
const Pink = new Color(colors.Pink)

const meta: Meta<typeof PursueEvadeDemo> = {
  title: "Pursue-Evade with FSM",
  component: PursueEvadeDemo,
}

export default meta

type Story = StoryObj<typeof PursueEvadeDemo>

const customSettings: Partial<FlockSettings> = {
  agent: {
    count: 10,
    maxSpeed: 1,
    maxForce: 0.05,
    height: 25,
  },
  boundaries: {
    shape: {
      kind: "rectangle",
      behavior: "torus",
    },
  },
  evade: {
    thresold: 50,
    c: 2 / 3,
  },
  pursue: {
    c: 2 / 3,
    threshold: 75 * 3,
  },
  world: {
    is2D: true,
  },
}

export const Basic: Story = {
  args: {
    settings: customSettings,
  },
  parameters: {
    is2D: true,
    debug: true,
    backgroundColor: colors.White,
  },
}

function PursueEvadeDemo(props) {
  const settings: FlockSettings = props.settings

  const { viewport } = useThree()

  const ref = useRef<InstancedMesh>(null)
  const pursueTarget = useRef<Agent>(null!)

  useEffect(() => {
    const unsubscribe = subscribeKey(state, "pursueTarget", (t) => {
      pursueTarget.current = t as unknown as Agent
    })
    return () => {
      unsubscribe()
    }
  }, [])

  const sphereDistributor = new RectangleDistributor(viewport, false)

  const hunter = new MovingAgent(
    { position: new Vector3(200, 200, 0) },
    10,
    0.1,
  )

  state.evadeFrom = hunter

  const machine = createHunterFSM(hunter)

  const behaviors: [string, Behavior[]][] = [
    ["attack", [pursue, stay2D]],
    ["fallback", [walk, stay2D]],
    ["rest", []],
  ]

  useFrame(() => {
    if (ref.current) {
      state.flock.map((agent, i) => {
        ref.current!.setColorAt(
          agent.id,
          agent.id === pursueTarget.current?.id &&
            machine.getCurrentState() === "attack"
            ? Teal
            : Navy,
        )
      })
      ref.current.instanceColor.needsUpdate = true
    }
  })

  return (
    <Flock
      distribution={sphereDistributor.getUniform(settings.agent.count)}
      settings={settings}
      randomVelocity={true}
    >
      <InstancedAgent
        behaviors={[stayWithinBoundaries, walk, evade, stay2D]}
        disableGameLoop={false}
        ref={ref}
      >
        <coneGeometry args={[5, 25]}></coneGeometry>
        <meshBasicMaterial />
      </InstancedAgent>
      <AgentWithFSM agent={hunter} machine={machine} behaviors={behaviors}>
        <mesh>
          <boxGeometry args={[10, 30]} />
          <meshBasicMaterial color={colors.Pink} />
        </mesh>
      </AgentWithFSM>
    </Flock>
  )
}
