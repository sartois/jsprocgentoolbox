import React, { useEffect, useRef, useState } from "react"
import { Meta, StoryObj } from "@storybook/react"
import { Bloom, EffectComposer } from "@react-three/postprocessing"
import { useFrame } from "@react-three/fiber"
import { PerformanceMonitor } from "@react-three/drei"
import { PointLight, SpotLight, Vector3 } from "three"
import { subscribeKey } from "valtio/utils"
import { FlockSettings, state } from "../src/store"
import { BoxDistributor } from "../src/distribution"
import { Flock, InstancedAgent } from "../src/components"
import {
  BehaviorKind,
  TypedVector3,
  align,
  cohesion,
  separate,
} from "../src/behaviors"
import { seek } from "../src/behaviors/root/seek"
import { IAgent } from "../src/agents"
import { BoxBoundaries } from "../src/boundary"

const palette = {
  dark: "#202426",
  deepKaki: "#6C733D",
  lightKaki: "#9DA65D",
  grey: "#8C8C88",
}

const meta: Meta<typeof BoidsDemo> = {
  title: "Typical boids implementation",
  component: BoidsDemo,
}

export default meta

type Story = StoryObj<typeof BoidsDemo>

const settings: Partial<FlockSettings> = {
  agent: {
    maxSpeed: 3,
    maxForce: 0.2,
    height: 10,
    count: 100,
  },
  boundaries: {
    shape: {
      kind: "box",
      behavior: "torus",
      halfSize: 250,
    },
  },
  separate: {
    threshold: 250,
  },
  align: {
    threshold: 500,
  },
  cohesion: {
    threshold: 500,
  },
  balance: {
    priorities: [
      [BehaviorKind.Cohesion, 1.5],
      [BehaviorKind.Align, 1],
      [BehaviorKind.Separate, 1],
      ["followTheLight", 3],
    ],
  },
}

export const Basic: Story = {
  args: {
    settings,
  },
  parameters: {
    debug: false,
    camera: { fov: 60, near: 0.1, far: 10000, position: [0, 125, 250] },
    addLight: false,
    backgroundColor: "#03091f",
  },
}

const lightPosition = new Vector3()

function BoidsDemo() {
  const flock = useRef<IAgent[]>(null!)
  const light = useRef<PointLight>(null!)
  const redLight = useRef<SpotLight>(null!)
  const boxDistributor = new BoxDistributor(
    (settings.boundaries!.shape as BoxBoundaries).halfSize,
  )

  useEffect(() => {
    const unsubscribe = subscribeKey(state, "flock", (f) => {
      flock.current = f
    })
    return () => {
      unsubscribe()
    }
  }, [])

  useFrame((state) => {
    if (light.current) {
      light.current.position.set(
        (-state.mouse.x * state.viewport.width) / 5,
        (-state.mouse.y * state.viewport.height) / 5,
        0,
      )
      lightPosition.copy(light.current.position)
    }
  })

  const [_settings, setSettings] = useState(settings)

  const onIncline = (o) => {
    _settings.agent!.count *= 2
    setSettings({ ..._settings })
  }
  const onDecline = (o) => {
    _settings.agent!.count /= 2
    setSettings({ ..._settings })
  }

  return (
    <>
      <PerformanceMonitor
        bounds={() => [60, 180]}
        onIncline={onIncline}
        onDecline={onDecline}
        flipflops={1}
      />
      <spotLight
        ref={redLight}
        intensity={15}
        position={[250, 250, -0]}
        color="red"
        distance={750}
        penumbra={0.3}
        angle={Math.PI / 8}
      />
      <ambientLight intensity={1} />

      <pointLight ref={light} distance={75} intensity={8} color="lemonchiffon">
        <mesh>
          <dodecahedronGeometry args={[8, 2]} />
        </mesh>
      </pointLight>

      <Flock
        distribution={boxDistributor.getNormal(_settings.agent!.count)}
        settings={_settings}
        sameVelocity={new Vector3(0.01, 0.01, 0.01)}
      >
        <InstancedAgent behaviors={[align, cohesion, separate, followTheLight]}>
          <cylinderGeometry args={[0, 3, _settings.agent?.height, 3]} />
          <meshStandardMaterial color={palette.dark} roughness={0.7} />
        </InstancedAgent>
      </Flock>

      <EffectComposer>
        <Bloom luminanceThreshold={0.2} luminanceSmoothing={0} />
      </EffectComposer>
    </>
  )
}

function followTheLight(agent: IAgent): TypedVector3 | void {
  const target = new Vector3() as TypedVector3
  target.kind = "followTheLight"
  return target.fromArray(seek(agent, lightPosition))
}
