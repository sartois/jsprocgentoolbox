import React, { useEffect, useMemo, useRef } from "react"
import {
  BufferGeometry,
  Color,
  Float32BufferAttribute,
  MathUtils,
  Mesh,
  PointsMaterial,
  SRGBColorSpace,
  TextureLoader,
  Vector3,
} from "three"
import { subscribeKey } from "valtio/utils"
import { useFrame, useLoader, extend } from "@react-three/fiber"
import { Meta, StoryObj } from "@storybook/react"
import { FilmPass, WaterPass, UnrealBloomPass, LUTPass } from "three-stdlib"
import { BoxDistributor } from "../src/distribution"
import { FlockSettings, state } from "../src/store"
import { BoxBoundaries, stayWithinBoundaries } from "../src/boundary"
import { Flock } from "../src/components"
import {
  BehaviorKind,
  behaviorLoop,
  followFlowfield,
  pursue,
} from "../src/behaviors"
import { SimplexFlowField3D } from "../src/flowfield"
import { MovingAgent } from "../src/agents"
import { Bloom, EffectComposer } from "@react-three/postprocessing"

extend({ WaterPass, UnrealBloomPass, FilmPass, LUTPass })

const meta: Meta<typeof AgentAsPointsDemo> = {
  title: "Agents as Points, follow Simplex flowfield, attracted to origin",
  component: AgentAsPointsDemo,
}

export default meta

type Story = StoryObj<typeof AgentAsPointsDemo>

export const Basic: Story = {
  args: {},
  parameters: {
    debug: false,
    camera: { fov: 40, near: 100, far: 10000, position: [0, 285, 550] },
    addLight: false,
    backgroundColor: "#000000",
  },
}

function AgentAsPointsDemo() {
  const sparkMap = useLoader(TextureLoader, "spark1.png")
  sparkMap.colorSpace = SRGBColorSpace

  const customSettings: Partial<FlockSettings> = {
    agent: {
      count: 5000,
      maxSpeed: 1,
      maxForce: 0.1,
      height: 1,
    },
    boundaries: {
      shape: {
        kind: "box",
        behavior: "torus",
        halfSize: 500,
      },
    },
    balance: { priorities: [[BehaviorKind.Flowfield, 0.5]] },
  }

  const simplexFF3D = useMemo(() => new SimplexFlowField3D(), [])
  state.flowfield = simplexFF3D

  const distribution = new BoxDistributor(
    (customSettings.boundaries!.shape as BoxBoundaries).halfSize,
  ).getUniform(customSettings.agent!.count)

  const behaviors = [followFlowfield, stayWithinBoundaries, pursue]

  const geometry = useMemo(() => new BufferGeometry(), [])
  const material = new PointsMaterial({
    size: 15,
    vertexColors: true,
    sizeAttenuation: true,
    map: sparkMap,
    alphaTest: 0.5,
    transparent: true,
    toneMapped: false,
  })

  const positions = useRef<Float32BufferAttribute>()
  const colors = useRef<Float32BufferAttribute>()
  const star = useRef<Mesh>(null!)
  const color = useMemo(() => new Color(), [])
  const { flock, settings } = state

  alternatePursueTarget(45000)

  useEffect(() => {
    const unsubscribe = subscribeKey(state, "flock", (f) => {
      positions.current = new Float32BufferAttribute(
        f.reduce((acc, agent) => {
          agent.position.toArray(acc, agent.id * 3)
          return acc
        }, []),
        3,
      )
      geometry.setAttribute("position", positions.current)
      geometry.computeBoundingSphere()

      colors.current = new Float32BufferAttribute(
        f.reduce((acc, agent) => {
          const d = MathUtils.mapLinear(agent.position.length(), 0, 860, 0, 1)
          color.setHSL(0.75, d, 1 - d, SRGBColorSpace)
          acc.push(color.r, color.g, color.b)
          return acc
        }, [] as number[]),
        3,
      )

      geometry.setAttribute("color", colors.current)
    })
    return () => {
      unsubscribe()
    }
  }, [color, geometry])

  useFrame(({ clock }, delta) => {
    if (
      flock &&
      Array.isArray(flock) &&
      positions.current &&
      colors.current &&
      settings
    ) {
      flock.map((agent) => {
        behaviorLoop({ agent, behaviors, delta, settings, flock })
        positions.current &&
          positions.current.set(agent.position.toArray(), agent.id * 3)

        const d = MathUtils.mapLinear(agent.position.length(), 0, 860, 0, 1)
        color.setHSL(0.75, d, 1 - d, SRGBColorSpace)
        colors.current && colors.current.set(color.toArray(), agent.id * 3)
      })
      positions.current.needsUpdate = true
      colors.current.needsUpdate = true
    }
    if (star.current) {
      const factor = 0.1 * Math.sin(clock.getElapsedTime() * 2) + 2
      star.current.scale.set(factor, factor, factor)
    }
  })

  return (
    <>
      <mesh ref={star}>
        <dodecahedronGeometry args={[10, 0]} />
      </mesh>

      <Flock
        distribution={distribution}
        randomVelocity={false}
        settings={customSettings}
      >
        <points geometry={geometry} material={material} />
      </Flock>

      <EffectComposer>
        {true && <Bloom luminanceThreshold={0.1} luminanceSmoothing={1} />}
      </EffectComposer>
    </>
  )
}

let targetToggleFlag = true
const centerAgent = new MovingAgent({ position: new Vector3() })

function alternatePursueTarget(delay) {
  setInterval(() => {
    if (targetToggleFlag) {
      targetToggleFlag = false
      state.pursueTarget = undefined
      state.settings!.balance = {}
    } else {
      targetToggleFlag = true
      state.pursueTarget = centerAgent
      state.settings!.balance = {
        priorities: [
          [BehaviorKind.Flowfield, 0.5],
          [BehaviorKind.Pursue, 2],
        ],
      }
    }
  }, delay)
}
