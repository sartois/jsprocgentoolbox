import React, { useEffect, useRef } from "react"
import {
  Color,
  CubeTextureLoader,
  InstancedMesh,
  SpotLight,
  Vector3,
} from "three"
import { Meta, StoryObj } from "@storybook/react"
import { useFrame, useThree } from "@react-three/fiber"
import { subscribeKey } from "valtio/utils"
import { Flock, Agent as CAgent } from "../src/components"
import { InstancedAgent } from "../src/components/InstancedAgent"
import {
  BehaviorKind,
  align,
  cohesion,
  evade,
  pursue,
  separate,
} from "../src/behaviors"
import { MovingAgent } from "../src/agents"
import { FlockSettings, state } from "../src/store"
import { BoxDistributor } from "../src/distribution"
import { getTarget } from "./helper/hunterMachine"
import { BoxBoundaries, stayWithinBoundaries } from "../src/boundary"

const colors = {
  Navy: "#364F6B",
  Teal: "#3FC1C9",
  White: "#F5F5F5",
  Pink: "#FC5185",
}

const Teal = new Color(colors.Teal)
const Pink = new Color(colors.Pink)

const meta: Meta<typeof PursueEvadeDemo> = {
  title: "Basic Pursue-Evade",
  component: PursueEvadeDemo,
}

export default meta

type Story = StoryObj<typeof PursueEvadeDemo>

const customSettings: Partial<FlockSettings> = {
  agent: {
    count: 300,
    maxSpeed: 1,
    maxForce: 1,
    height: 10,
  },
  boundaries: {
    shape: {
      kind: "box",
      behavior: "torus",
      halfSize: 500,
    },
  },
  evade: {
    thresold: 500,
    c: 0.005,
  },
  pursue: {
    c: 0.005,
    threshold: 10 * 50,
  },
  separate: {
    threshold: 250,
  },
  align: {
    threshold: 500,
  },
  cohesion: {
    threshold: 500,
  },
  wander: {
    radius: 20,
    distance: 5,
    variation: 0.52,
  },
  balance: {
    priorityBehavior: undefined, //BehaviorKind.Evade,
    priorities: [[BehaviorKind.Evade, 4]],
  },
  world: {
    is2D: false,
  },
}

export const Basic: Story = {
  args: {
    settings: customSettings,
    hunter: {
      maxSpeed: 3,
      maxForce: 3,
    },
  },
  parameters: {
    debug: false,
    backgroundColor: "#03091f",
    camera: { fov: 45, near: 0.1, far: 10000, position: [-600, 600, 600] },
    addLight: false,
    controls: true,
  },
}

function PursueEvadeDemo(props) {
  const settings: Partial<FlockSettings> = props.settings
  const { maxSpeed, maxForce } = props.hunter

  const { scene } = useThree()

  const ref = useRef<InstancedMesh>(null)
  const pursueTarget = useRef<MovingAgent>(null!)
  const redLight = useRef<SpotLight>(null!)

  const loader = new CubeTextureLoader()
  const texture = loader.load([
    "/cube/ce.png",
    "/cube/ww.png",
    "/cube/nn.png",
    "/cube/ss.png",
    "/cube/wc.png",
    "/cube/ee.png",
  ])

  scene.background = texture
  //camera.lookAt(new Vector3(500, 500, -500))
  //camera.updateProjectionMatrix()

  useEffect(() => {
    const unsubscribe = subscribeKey(state, "pursueTarget", (t) => {
      pursueTarget.current = t as unknown as MovingAgent
    })
    return () => {
      unsubscribe()
    }
  }, [])

  useEffect(() => {
    const unsubscribe = subscribeKey(state, "flock", (f) => {
      state.pursueTarget = getTarget(f)
    })
    return () => {
      unsubscribe()
    }
  }, [])

  const boxDistributor = new BoxDistributor(
    (settings.boundaries?.shape as BoxBoundaries).halfSize,
  )

  const hunter = new MovingAgent(
    { position: new Vector3() },
    maxSpeed,
    maxForce,
  )

  state.evadeFrom = hunter

  useFrame(() => {
    if (ref.current) {
      state.flock.map((agent, i) => {
        ref.current!.setColorAt(
          agent.id,
          agent.id === pursueTarget.current?.id ? Teal : Pink,
        )
      })
      ref.current!.instanceColor!.needsUpdate = true
    }
  })

  //useHelper(redLight, SpotLightHelper, 'cyan')

  return (
    <>
      <spotLight
        ref={redLight}
        intensity={10}
        position={[0, 1000, 0]}
        color="red"
        distance={2000}
        penumbra={0.1}
        angle={Math.PI / 8}
      />
      <ambientLight intensity={0.2} />

      <Flock
        distribution={boxDistributor.getUniform(settings.agent!.count)}
        settings={settings}
        randomVelocity={false}
      >
        <InstancedAgent
          behaviors={[stayWithinBoundaries, align, cohesion, separate, evade]}
          disableGameLoop={false}
          ref={ref}
        >
          <cylinderGeometry args={[0, 4, settings.agent?.height, 3]} />
          <meshStandardMaterial roughness={0.8} />
        </InstancedAgent>
        <CAgent agent={hunter} behaviors={[pursue]}>
          <mesh>
            <cylinderGeometry args={[0, 16, settings.agent!.height * 4, 3]} />
            <meshStandardMaterial color={colors.White} />
          </mesh>
        </CAgent>
      </Flock>
    </>
  )
}
