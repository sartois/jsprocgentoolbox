import React, { useRef } from "react"
import { Color, InstancedMesh, Vector3 } from "three"
import { Meta, StoryObj } from "@storybook/react"
import { useFrame, useThree } from "@react-three/fiber"
import { Flock } from "../src/Agents/components/Flock"
import { InstancedAgent } from "../src/Agents/components/InstancedAgent"
import {
  evade,
  pursue,
  stay2D,
  BehaviorKind,
  wander,
} from "../src/Agents/behaviors"
import { MovingAgent } from "../src/Agents/MovingAgent"
import { FlockSettings, state } from "../src/store"
import { RectangleDistributor } from "../src/distribution"
import { Agent } from "../src/Agents/components/Agent"
import { stayWithinBoundaries } from "../src/boundary"

const colors = {
  Navy: "#364F6B",
  Teal: "#3FC1C9",
  White: "#F5F5F5",
  Pink: "#FC5185",
}

const Teal = new Color(colors.Teal)
const Navy = new Color(colors.Navy)
const Pink = new Color(colors.Pink)

const meta: Meta<typeof PursueEvadeClosestTarget> = {
  title: "Pursue-Evade with dynamic target (closest)",
  component: PursueEvadeClosestTarget,
}

export default meta

type Story = StoryObj<typeof PursueEvadeClosestTarget>

const customSettings: Partial<FlockSettings> = {
  agent: {
    count: 100,
    maxSpeed: 1,
    maxForce: 0.05,
    height: 25,
  },
  boundaries: {
    shape: {
      kind: "rectangle",
      behavior: "bounce",
    },
  },
  evade: {
    thresold: 500,
    c: 0.5,
  },
  pursue: {
    c: 0.005,
    threshold: 75 * 3,
  },
  balance: {
    priorityBehavior: undefined, //BehaviorKind.Evade,
    priorities: [],
  },
  world: {
    is2D: true,
  },
}

export const Basic: Story = {
  args: {
    settings: customSettings,
    hunter: {
      maxSpeed: 2,
      maxForce: 0.1,
    },
  },
  parameters: {
    debug: true,
    backgroundColor: colors.White,
  },
}

function PursueEvadeClosestTarget(props) {
  const settings: Partial<FlockSettings> = props.settings
  const { maxSpeed, maxForce } = props.hunter

  const { viewport } = useThree()

  const ref = useRef<InstancedMesh>(null)
  const pursueTarget = useRef<MovingAgent>(null!)

  const sphereDistributor = new RectangleDistributor(viewport, false)

  const hunter = new MovingAgent(
    { position: new Vector3(viewport.width / 2, viewport.height / 2, 0) },
    maxSpeed,
    maxForce,
  )

  state.evadeFrom = hunter

  setInterval(() => {
    //pick the closest agent and set as target
    const result = [...state.flock]
      .sort(
        (a, b) =>
          a.position.distanceToSquared(hunter.position) -
          b.position.distanceToSquared(hunter.position),
      )
      .shift()
    if (result !== state.pursueTarget) {
      state.pursueTarget = result
      pursueTarget.current = result!
    }
  }, 10000)

  useFrame(() => {
    if (ref.current) {
      const ins = ref.current //typescript hell
      state.flock.map((agent, i) => {
        ins.setColorAt(
          agent.id,
          agent.id === pursueTarget.current?.id ? Teal : Navy,
        )
      })
      if (ins.instanceColor) {
        ins.instanceColor.needsUpdate = true
      }
    }
  })

  return (
    <Flock
      distribution={sphereDistributor.getUniform(settings.agent!.count)}
      settings={settings}
      randomVelocity={true}
    >
      <InstancedAgent
        behaviors={[stayWithinBoundaries, wander, evade, stay2D /*, separate*/]}
        disableGameLoop={false}
        ref={ref}
      >
        <coneGeometry args={[5, 25]}></coneGeometry>
        <meshBasicMaterial />
      </InstancedAgent>
      <Agent agent={hunter} behaviors={[stayWithinBoundaries, pursue, stay2D]}>
        <mesh>
          <boxGeometry args={[10, 30]} />
          <meshBasicMaterial color={colors.Pink} />
        </mesh>
      </Agent>
    </Flock>
  )
}
