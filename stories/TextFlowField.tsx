import React, { useEffect, useMemo, useRef } from "react"
import { Meta, StoryObj } from "@storybook/react"
import {
  CanvasTexture,
  Color,
  DoubleSide,
  LinearMipMapLinearFilter,
  MeshBasicMaterial,
  NearestFilter,
  RepeatWrapping,
  Vector3,
} from "three"
import { useThree, extend } from "@react-three/fiber"
import { FlowfieldFromText } from "../src/FlowField/helpers/FlowfieldFromText"
import { MovingAgent } from "../src/Agents/MovingAgent"
import { InstancedAgent } from "../src/Agents/components/InstancedAgent"
import { Flock } from "../src/Agents/components/Flock"
import { FlowFieldHelper } from "../src/FlowField/helpers/FlowFieldHelper"
import { RectangleDistributor } from "../src/utils/distribution/RectangleDistributor"
import {
  wander,
  followFlowfield,
  stayWithinBoundaries,
  stay2D,
} from "../src/Agents/behaviors"

extend({ FlowFieldHelper })

const meta: Meta<typeof TextFlowfield2DDemo> = {
  title: "Text Flowfield 2D demo",
  component: TextFlowfield2DDemo,
}

export default meta

type Story = StoryObj<typeof TextFlowfield2DDemo>

export const Basic: Story = {
  args: {
    flowFieldCellSize: 50,
    agentCount: 1000,
    agentParam: {
      maxSpeed: 5,
      maxForce: 0.05,
    },
    showFlowfield: false,
  },
  parameters: {
    is2D: true,
    debug: true,
    backgroundColor: config.backgroundColor,
  },
}

const dummy = new Vector3()

function TextFlowfield2DDemo({}) {
  const {
    viewport: { width, height },
    gl,
  } = useThree()

  console.log({ width, height })

  const material = useRef<MeshBasicMaterial>()

  const ffText = useMemo(() => {
    const t = new FlowfieldFromText({ width: 1024, height: 1024 })
    t.fillCanvas({
      text: "ART",
      font: '550px "Paytone One"',
      color: "DarkSlateBlue",
    })
    t.createFlowField(8)
    return t
  }, [])

  const texture = useMemo(() => {
    return new CanvasTexture(ffText.canvas)
  }, [ffText])

  useEffect(() => {
    texture.anisotropy = gl.capabilities.getMaxAnisotropy()
    texture.minFilter = LinearMipMapLinearFilter
    texture.magFilter = NearestFilter
    texture.wrapS = RepeatWrapping
    texture.repeat.x = -1
    if (material.current) {
      material.current.map = texture
    }
  }, [texture, material])

  const agents = useMemo(() => {
    const rectangleDistributor = new RectangleDistributor(
      { width, height },
      false,
    )
    return rectangleDistributor.getNormal(1).map(
      (p) =>
        new MovingAgent(
          {
            position: dummy.clone().set(p.x, p.y, 0),
            velocity: dummy.set(1, 0, 0).clone(),
          },
          1,
        ),
      [],
    )
  }, [width, height])

  const settings = {
    flowfield: ffText,
    boundaries: {
      shape: {
        kind: "rectangle",
        width,
        height,
        behavior: "torus",
        gridCellSize: 25,
      },
      threshold: 0,
    },
    flock: agents,
    wander: {
      radius: config.baseMeshRadius,
      distance: config.baseMeshHeight / 2,
      variation: Math.PI / 8,
    },
    balance: {
      priorityBehavior: "flowfield",
    },
  }

  return (
    <>
      {false && <flowFieldHelper args={[ffText, , new Color(0x022f8e)]} />}
      <mesh visible={false} position={[512, 512, 0]} rotation-z={Math.PI}>
        <planeBufferGeometry args={[1024, 1024]} />
        <meshBasicMaterial
          ref={material}
          side={DoubleSide}
          color={0xff0000}
          transparent={true}
        />
      </mesh>
      <Flock agents={agents} settings={settings}>
        <InstancedAgent
          behaviors={[wander, followFlowfield, stayWithinBoundaries, stay2D]}
          disableGameLoop={false}
        >
          <coneGeometry args={[5, 25]} />
          <meshBasicMaterial color={config.baseMeshColor} />
        </InstancedAgent>
      </Flock>
    </>
  )
}
