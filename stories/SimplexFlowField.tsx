import React, { useMemo } from "react"
import { Meta, StoryObj } from "@storybook/react"
import { Color, Vector3 } from "three"
import { useThree, extend } from "@react-three/fiber"
import { SimplexFlowField2D } from "../src/FlowField/SimplexFlowField2D"
import { PreComputedFlowField } from "../src/FlowField/helpers/PreComputedFlowField"
import { InstancedAgent } from "../src/Agents/components/InstancedAgent"
import { Flock } from "../src/Agents/components/Flock"
import { FlowFieldHelper } from "../src/FlowField/helpers/FlowFieldHelper"
import { stay2D, separate } from "../src/Agents/behaviors"
import { registerInGrid, stayWithinBoundaries } from "../src/boundary"
import { RectangleDistributor } from "../src/distribution"
import { followFlowfield } from "../src/Agents/behaviors/followFlowfield"
import { FlockSettings, state } from "../src/store"

extend({ FlowFieldHelper })

const colors = {
  dark: "#202426",
  deepKaki: "#6C733D",
  lightKaki: "#9DA65D",
  grey: "#8C8C88",
}

const meta: Meta<typeof SimplexFlowField2DDemo> = {
  title: "Simplex Flowfield 2D demo",
  component: SimplexFlowField2DDemo,
}

export default meta

type Story = StoryObj<typeof SimplexFlowField2DDemo>

const settings: Partial<FlockSettings> = {
  agent: {
    maxSpeed: 5,
    maxForce: 0.05,
    height: 25,
    count: 200,
  },
  boundaries: {
    shape: {
      kind: "rectangle",
      behavior: "torus",
    },
    threshold: 0,
  },
  neighbors: {
    useGrid: true,
    gridCellSize: 25,
  },
  world: {
    is2D: true,
  },
}

let flowField

export const Basic: Story = {
  args: {
    settings,
    showFlowfield: false,
  },
  parameters: {
    debug: true,
    backgroundColor: colors.lightKaki,
    addHtml: () => (
      <div
        style={{ position: "absolute", top: "10px", right: "10px", zIndex: 2 }}
      >
        <button onClick={() => download()}>Export flowfield</button>
      </div>
    ),
  },
}

function download() {
  var element = document.createElement("a")
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," +
      encodeURIComponent(JSON.stringify(flowField)),
  )
  element.setAttribute("download", `flowfield-${new Date().toISOString()}`)
  element.style.display = "none"
  document.body.appendChild(element)
  element.click()
  document.body.removeChild(element)
}

interface SimplexFlowField2DDemoProps {
  settings: Partial<FlockSettings>
  showFlowfield: boolean
}

function SimplexFlowField2DDemo({
  settings,
  showFlowfield = false,
}: SimplexFlowField2DDemoProps) {
  const {
    viewport: { width, height },
  } = useThree()

  const simplexFF = useMemo(() => new SimplexFlowField2D(), [])

  const FF = useMemo(
    () => new PreComputedFlowField(width, height, 50, simplexFF),
    [width, height, simplexFF],
  )

  state.flowfield = FF
  flowField = FF

  const rectangleDistributor = new RectangleDistributor(
    { width, height },
    false,
  )

  return (
    <>
      {showFlowfield && (
        <flowFieldHelper args={[FF, , new Color(colors.dark)]} />
      )}
      <Flock
        distribution={rectangleDistributor.getUniform(settings.agent!.count)}
        settings={settings}
        randomVelocity={true}
      >
        <InstancedAgent
          behaviors={[
            registerInGrid,
            followFlowfield,
            stayWithinBoundaries,
            separate,
            stay2D,
          ]}
          disableGameLoop={false}
        >
          <coneGeometry
            args={[settings.agent!.height / 5, settings.agent?.height]}
          />
          <meshBasicMaterial color={colors.deepKaki} />
        </InstancedAgent>
      </Flock>
    </>
  )
}
