import React from "react"
import { Meta, StoryObj } from "@storybook/react"
import { Agent } from "../src/components"
import {
  BoxDistributor,
  CircleDistributor,
  Distributor,
  RectangleDistributor,
  SphereDistributor,
} from "../src/distribution"
import { MovingAgent } from "../src/agents"
import { Vector3 } from "three"

const colors = {
  blue: "#282673",
  yellow: "#F2B705",
  naplesYellow: "#F29F05",
  deepYellow: "#D97904",
  white: "#F2F2F2",
}

const meta: Meta<typeof DistributionViz> = {
  title: "Distribution",
  component: DistributionViz,
}

export default meta

type Story = StoryObj<typeof DistributionViz>

export const DistributionVizStory: Story = {
  args: {
    type: "Uniform",
    shape: "Rectangle",
    agentCount: 1000,
  },
  argTypes: {
    type: {
      options: ["Uniform", "Normal", "Edgy"],
      control: {
        type: "radio",
      },
    },
    shape: {
      options: ["Rectangle", "Cube", "Sphere", "Circle"],
      control: {
        type: "select",
      },
    },
  },
  parameters: {
    debug: true,
    backgroundColor: colors.white,
  },
}

interface DistributionVizProps {
  type: "Uniform" | "Normal" | "Edgy"
  shape: "Circle" | "Cube" | "Rectangle" | "Sphere"
  agentCount: number
}

function DistributionViz({ type, shape, agentCount }: DistributionVizProps) {
  let distributor: Distributor
  let positions: { x: number; y: number; z: number }[]

  if (shape === "Rectangle") {
    distributor = new RectangleDistributor({ width: 100, height: 100 })
  } else if (shape === "Cube") {
    distributor = new BoxDistributor(50)
  } else if (shape === "Circle") {
    distributor = new CircleDistributor(50)
  } else {
    distributor = new SphereDistributor(50)
  }

  if (type === "Uniform") {
    positions = distributor.getUniform(agentCount)
  } else if (type === "Normal") {
    positions = distributor.getNormal(agentCount)
  } else {
    positions = distributor.getEdgy(agentCount)
  }

  const flock = positions.map((p) => {
    if (shape === "Circle" || shape === "Rectangle") {
      return new MovingAgent({ position: new Vector3(p.x, 0, p.y) })
    } else {
      return new MovingAgent({ position: new Vector3(p.x, p.y, p.z) })
    }
  })

  return (
    <>
      {flock.map((agent, i) => (
        <Agent
          agent={agent}
          disableGameLoop={true}
          behaviors={[]}
          key={`agent${i}`}
        >
          <mesh>
            <boxGeometry args={[1, 1, 1]} />
            <meshStandardMaterial color={colors.deepYellow} />
          </mesh>
        </Agent>
      ))}
    </>
  )
}
