import React, { useMemo, useRef } from "react"
import { Meta, StoryObj } from "@storybook/react"
import { Points, ShaderMaterial, Vector3 } from "three"
import { SphereDistributor } from "../src/distribution"
import { useFrame } from "@react-three/fiber"

const meta: Meta<typeof TestComputingShader> = {
  title: "Test computing position with shader",
  component: TestComputingShader,
}

export default meta

type Story = StoryObj<typeof TestComputingShader>

const colors = {
  Navy: "#364F6B",
  Teal: "#3FC1C9",
  White: "#F5F5F5",
  Pink: "#FC5185",
}

export const Basic: Story = {
  args: {
    count: 400000,
    radius: 200,
  },
  parameters: {
    debug: true,
    backgroundColor: colors.White,
    camera: { fov: 60, near: 0.1, far: 20000, position: [0, 125, 250] },
  },
}

function TestComputingShader(props) {
  const { count, radius } = props
  const vertex = new Vector3()
  const points = useRef<Points>(null!)

  const distribution = new SphereDistributor(radius).getUniform(count)

  const particlesPosition = useMemo(() => {
    const positions = new Float32Array(count * 3)
    distribution.forEach((p, i) =>
      vertex.set(p.x, p.y, p.z).toArray(positions, i * 3),
    )
    return positions
  }, [count])

  const uniforms = useMemo(
    () => ({
      uTime: {
        value: 0.0,
      },
    }),
    [],
  )

  useFrame((state) => {
    if (points.current) {
      const { clock } = state
      ;(points.current.material as ShaderMaterial).uniforms.uTime.value =
        clock.elapsedTime
    }
  })

  return (
    <points ref={points}>
      <bufferGeometry>
        <bufferAttribute
          attach="attributes-position"
          count={particlesPosition.length / 3}
          array={particlesPosition}
          itemSize={3}
        />
      </bufferGeometry>
      <shaderMaterial
        depthWrite={false}
        fragmentShader={fragmentShader}
        vertexShader={vertexShader}
        uniforms={uniforms}
      />
    </points>
  )
}

const vertexShader = `
uniform float uTime;
uniform float uRadius;

// Source: https://github.com/dmnsgn/glsl-rotate/blob/main/rotation-3d-y.glsl.js
mat3 rotation3dY(float angle) {
  float s = sin(angle);
  float c = cos(angle);
  return mat3(
    c, 0.0, -s,
    0.0, 1.0, 0.0,
    s, 0.0, c
  );
}


void main() {
  float distanceFactor = pow(uRadius - distance(position, vec3(0.0)), 1.5);
  vec3 particlePosition = position * rotation3dY(uTime * 0.3 * distanceFactor);

  vec4 modelPosition = modelMatrix * vec4(particlePosition, 1.0);
  vec4 viewPosition = viewMatrix * modelPosition;
  vec4 projectedPosition = projectionMatrix * viewPosition;

  gl_Position = projectedPosition;
  gl_PointSize = 3.0;
}

`

const fragmentShader = `
void main() {
  gl_FragColor = vec4(0.34, 0.53, 0.96, 1.0);
}
`
