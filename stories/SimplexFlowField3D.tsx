import React, { useMemo } from "react"
import { Meta, StoryObj } from "@storybook/react"
import { Color, Vector3 } from "three"
import { useThree, extend } from "@react-three/fiber"
import { SimplexFlowField3D } from "../src/FlowField"
//import { PreComputedFlowField } from "../src/FlowField/helpers/PreComputedFlowField";
import { InstancedAgent } from "../src/Agents/components/InstancedAgent"
import { Flock } from "../src/Agents/components/Flock"
import { FlowFieldHelper } from "../src/FlowField/helpers/FlowFieldHelper"
import { stay2D, separate } from "../src/Agents/behaviors"
import { registerInGrid, stayWithinBoundaries } from "../src/boundary"
import { followFlowfield } from "../src/Agents/behaviors/followFlowfield"
import { FlockSettings, state } from "../src/store"
import { BoxDistributor } from "../src/distribution"

extend({ FlowFieldHelper })

const colors = {
  teal: "#3fb8af",
  paleGreen: "#7fc7af",
  paleYellow: "#dad8a7",
  paleRose: "#ff9e9d",
  vibrantFushia: "#ff3d7f",
}

const meta: Meta<typeof SimplexFlowField3DDemo> = {
  title: "Simplex Flowfield 3D demo",
  component: SimplexFlowField3DDemo,
}

export default meta

type Story = StoryObj<typeof SimplexFlowField3DDemo>

const settings: Partial<FlockSettings> = {
  agent: {
    maxSpeed: 2,
    maxForce: 0.02,
    height: 25,
    count: 1000,
  },
  boundaries: {
    shape: {
      kind: "box",
      behavior: "torus",
      halfSize: 1000,
    },
    threshold: 0,
  },
  neighbors: {
    useGrid: false,
    gridCellSize: 25,
  },
}

//let flowField;

export const Basic: Story = {
  args: {
    settings,
    showFlowfield: false,
  },
  parameters: {
    debug: true,
    backgroundColor: colors.paleYellow,
    camera: { fov: 60, near: 0.1, far: 10000, position: [0, 125, 250] },
    /*addHtml: () => (
      <div
        style={{ position: "absolute", top: "10px", right: "10px", zIndex: 2 }}
      >
        <button onClick={() => download()}>Export flowfield</button>
      </div>
    ),*/
  },
}

/*function download() {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," +
      encodeURIComponent(JSON.stringify(flowField))
  );
  element.setAttribute("download", `flowfield-${new Date().toISOString()}`);
  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}*/

interface SimplexFlowField3DDemoProps {
  settings: Partial<FlockSettings>
  showFlowfield: boolean
}

function SimplexFlowField3DDemo({
  settings,
  showFlowfield = false,
}: SimplexFlowField3DDemoProps) {
  const simplexFF3D = useMemo(() => new SimplexFlowField3D(), [])

  state.flowfield = simplexFF3D
  //flowField = simplexFF3D;

  const boxDistributor = new BoxDistributor(1000)

  return (
    <>
      <Flock
        distribution={boxDistributor.getUniform(settings.agent!.count)}
        settings={settings}
        randomVelocity={true}
      >
        <InstancedAgent
          behaviors={[
            //registerInGrid,
            followFlowfield,
            stayWithinBoundaries,
            separate,
            //stay2D,
          ]}
          disableGameLoop={false}
        >
          <coneGeometry
            args={[settings.agent!.height / 5, settings.agent?.height]}
          />
          <meshStandardMaterial color={colors.vibrantFushia} />
        </InstancedAgent>
      </Flock>
    </>
  )
}
