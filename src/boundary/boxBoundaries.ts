import { Vector3 } from "three"
import { IAgent } from "../agents"
import { BoxBoundaries } from "."
import { seek } from "../behaviors/root/seek"
import { BehaviorKind, TypedVector3 } from "../behaviors"

const bounceWorldCenter = new Vector3()

const bounceFutureFactor = 3

export function boxBoundaries(
  agent: IAgent,
  shape: BoxBoundaries,
  baseHeight: number,
) {
  if (shape.behavior === "torus") {
    return boxTorus(agent, shape.halfSize, baseHeight)
  } else {
    return boxBounce(agent, shape.halfSize)
  }
}

function boxTorus(agent: IAgent, halfSize: number, baseHeight: number) {
  const position = agent.position.toArray()
  const min = -halfSize - baseHeight
  const max = halfSize + baseHeight
  let methodToCall: "setX" | "setY" | "setZ" | null = null
  let arg = null
  position.forEach((p, i) => {
    if (p > max || p < min) {
      methodToCall = i === 0 ? "setX" : i === 1 ? "setY" : "setZ"
      arg = p > max ? min : max
    }
  })

  if (typeof methodToCall === "string") {
    //@ts-ignore
    agent.position[methodToCall](arg)
  }
}

function boxBounce(agent: IAgent, halfSize: number) {
  const bouncer = new Vector3() as TypedVector3
  bouncer.kind = BehaviorKind.Boundaries
  const position = bouncer
    .copy(agent.velocity)
    .multiplyScalar(bounceFutureFactor)
    .add(agent.position)
    .toArray()
  let needToSeek = false

  position.forEach((p) => {
    if (p > halfSize || p < -halfSize) {
      needToSeek = true
    }
  })
  if (needToSeek) {
    return bouncer.fromArray(seek(agent, bounceWorldCenter, false))
  }
}
