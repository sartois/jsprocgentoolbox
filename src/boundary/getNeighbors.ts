import { IAgent } from "../agents"
import { getNeighborsFromGrid } from "./gridPartitionner"

export interface getNeighborsSettings {
  agent: IAgent
  flock: IAgent[] | undefined
  threshold?: number
  depth?: number
  useGrid: boolean
}

export function getNeighbors({
  agent,
  flock,
  threshold,
  depth,
  useGrid,
}: getNeighborsSettings): IAgent[] {
  let neighbors: IAgent[] = []

  if (useGrid) {
    neighbors = getNeighborsFromGrid(agent, depth).filter((a) => a !== agent)
  } else {
    const squaredThreshold = threshold! * threshold!
    //@todo return a tuple [agent, distanceToSquared] to avoid computing twice in separate ?
    neighbors = flock!.filter(
      (a) =>
        a !== agent &&
        agent.position.distanceToSquared(a.position) <= squaredThreshold,
    )
  }

  return neighbors
}
