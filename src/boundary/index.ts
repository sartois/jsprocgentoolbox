export interface RectangleBoundaries {
  kind: "rectangle"
  behavior: "bounce" | "torus" | "centerAttraction"
  centerAttractionFutureFactor?: 3
}

export interface SphereBoundaries {
  kind: "sphere"
  radius: number
}

export interface BoxBoundaries {
  kind: "box"
  halfSize: number
  behavior: "bounce" | "torus"
}

export type BoundariesShape =
  | SphereBoundaries
  | RectangleBoundaries
  | BoxBoundaries

export * from "./boxBoundaries"
export * from "./getNeighbors"
export * from "./gridPartitionner"
export * from "./rectangleBoundaries"
export * from "./sphereBoundaries"
export * from "./stayWithinBoundaries"
