import { MathUtils, Vector3 } from "three"
import { IAgent } from "../agents"
import { BehaviorKind, TypedVector3 } from "../behaviors"
import { SphereBoundaries } from "."

export function sphereBoundaries(
  agent: IAgent,
  shape: SphereBoundaries,
  threshold: number | undefined,
) {
  if (threshold === undefined) {
    return console.warn(
      "Boundaries threshold should be defined, no force will be applied",
    )
  }
  const desired = new Vector3() as TypedVector3
  desired.kind = BehaviorKind.Boundaries
  const { radius } = shape
  const center = { x: 0, y: 0, z: 0 } as Vector3
  const distToCenter = agent.position.distanceTo(center)

  let multiplier = 0

  desired.subVectors(center, agent.position)

  const overtaking = distToCenter - (radius - threshold)

  if (overtaking > 0) {
    multiplier = MathUtils.mapLinear(
      overtaking,
      0,
      threshold,
      0,
      agent.maxSpeed,
    )
  }

  return desired
    .normalize()
    .multiplyScalar(multiplier) /*.clampLength(0, agent.maxForce)*/
}
