import { Vector3 } from "three"
import { IAgent } from "../agents"
import { state } from "../store"
import { RectangleBoundaries } from "."
import { seek } from "../behaviors/root/seek"
import { BehaviorKind, TypedVector3 } from "../behaviors"

const bounce2DWorldCenter = new Vector3()

type Rectangle = {
  width: number
  height: number
}

export function rectangleBoundaries(agent: IAgent, shape: RectangleBoundaries) {
  if (state.viewport === undefined) {
    return console.warn(
      "Please share viewport in state to use RectangleBoundaries",
    )
  }

  if (shape.behavior === "centerAttraction") {
    return centerAttraction(
      agent,
      state.viewport,
      shape.centerAttractionFutureFactor ?? 1,
    ) as TypedVector3
  } else if (shape.behavior === "torus") {
    return torus(agent, state.viewport)
  } else {
    return bounce(agent, state.viewport)
  }
}

function bounce(agent: IAgent, { width, height }: Rectangle) {
  if (agent.position.x <= 0 || agent.position.x >= width) {
    agent.velocity.setX(agent.velocity.x * -1)
  }
  if (agent.position.y <= 0 || agent.position.y >= height) {
    agent.velocity.setY(agent.velocity.y * -1)
  }
}

function centerAttraction(
  agent: IAgent,
  { width, height }: Rectangle,
  centerAttractionFutureFactor: number,
) {
  bounce2DWorldCenter.set(width / 2, height / 2, 0)

  const bouncer = new Vector3() as TypedVector3
  bouncer.kind = BehaviorKind.Boundaries
  bouncer
    .copy(agent.velocity)
    .multiplyScalar(centerAttractionFutureFactor)
    .add(agent.position)

  if (
    bouncer.x < 0 ||
    bouncer.x > width ||
    bouncer.y < 0 ||
    bouncer.y > height
  ) {
    return bouncer.fromArray(seek(agent, bounce2DWorldCenter, false))
  }
}

function torus(agent: IAgent, { width, height }: Rectangle) {
  if (agent.position.x < 0) {
    agent.position.setX(width)
  }
  if (agent.position.x > width) {
    agent.position.setX(0)
  }
  if (agent.position.y < 0) {
    agent.position.setY(height)
  }
  if (agent.position.y > height) {
    agent.position.setY(0)
  }
}
