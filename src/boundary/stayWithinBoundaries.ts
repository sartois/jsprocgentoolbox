import { IAgent } from "../agents"
import { TypedVector3 } from "../behaviors"
import { FlockSettings } from "../store"
import {
  BoundariesShape,
  boxBoundaries,
  rectangleBoundaries,
  sphereBoundaries,
} from "."

export interface BoundariesSettings {
  shape: BoundariesShape
  threshold?: number
}

export function getBoundariesDefaults(baseHeight: number): BoundariesSettings {
  return {
    shape: {
      radius: baseHeight * 10,
      kind: "sphere",
    },
    threshold: baseHeight * 2,
  }
}

export function stayWithinBoundaries(
  agent: IAgent,
  settings: FlockSettings,
): TypedVector3 | void {
  if (settings.boundaries === undefined) {
    return console.warn(
      "Please configure flock boundaries or remove stayWithinBoundaries behavior",
    )
  }

  const { shape, threshold } = settings.boundaries

  if (shape.kind === "sphere") {
    return sphereBoundaries(agent, shape, threshold)
  } else if (shape.kind === "box") {
    return boxBoundaries(agent, shape, settings.agent.height)
  } else if (shape.kind === "rectangle") {
    return rectangleBoundaries(agent, shape)
  }
}
