import { IAgent } from "../agents"
import { FlockSettings, state } from "../store"
import { BoundariesShape, RectangleBoundaries } from "../boundary"
import { Behavior } from "../behaviors"

export type GridSize = { gridCellSize: number }

export { createGrid, registerInGrid, resetGrid, getNeighborsFromGrid, getGrid }

function createGrid(shape: BoundariesShape, size: GridSize) {
  if (shape.kind === "rectangle") {
    createRectangleGrid(shape as RectangleBoundaries, size)
  } else {
    throw Error("Not yet implemented")
  }
}

const registerInGrid: Behavior = (agent: IAgent, settings: FlockSettings) => {
  if (settings.boundaries?.shape.kind === "rectangle") {
    registerInRectangleGrid(agent, settings)
  } else {
    throw Error("Not yet implemented")
  }
}

interface RectangleGridData {
  width: number
  height: number
  colsCount: number
  rowsCount: number
  cellSize: number
  behavior: RectangleBoundaries["behavior"]
  content: IAgent[][]
}

const rectangleGrid: RectangleGridData = {
  width: 0,
  height: 0,
  colsCount: 0,
  rowsCount: 0,
  cellSize: 0,
  content: [],
  behavior: "torus",
}

function getGrid(shape: BoundariesShape) {
  if (shape.kind === "rectangle") {
    return rectangleGrid
  } else {
    throw Error("Not yet implemented")
  }
}

function createRectangleGrid(shape: RectangleBoundaries, size: GridSize) {
  let remainder

  const { width: _width, height: _height } = state.viewport!

  if (!_width || !_height) {
    return console.warn("Please provide correct viewport in state")
  }

  const gridCellSize = size.gridCellSize

  const w = Math.ceil(_width),
    h = Math.ceil(_height)

  rectangleGrid.width =
    (remainder = w % gridCellSize) === 0 ? w : w + (gridCellSize - remainder)
  rectangleGrid.height =
    (remainder = h % gridCellSize) === 0 ? h : h + (gridCellSize - remainder)

  rectangleGrid.colsCount =
    gridCellSize > 0 ? rectangleGrid.width / gridCellSize : 0
  rectangleGrid.rowsCount =
    gridCellSize > 0 ? rectangleGrid.height / gridCellSize : 0

  if (rectangleGrid.colsCount < 3 || rectangleGrid.rowsCount < 3) {
    console.warn(
      "Not enough rows or columns. Modify gridCellSize to have at least 3 columns and 3 rows.",
    )
  }

  rectangleGrid.cellSize = gridCellSize
  rectangleGrid.behavior = shape.behavior
}

function getIndex([row, column]: [number, number]): number {
  const index = row * rectangleGrid.colsCount + column
  return index
}

function getColAndRow(agent: IAgent): [number, number] {
  const cellSize = rectangleGrid.cellSize
  const { width, height } = rectangleGrid

  if (!cellSize || cellSize <= 0) throw Error("Please provide correct cellSize")

  let { x, y } = agent.position

  if (x < 0) x = 0
  if (x >= width) x = width - 1
  if (y < 0) y = 0
  if (y >= height) y = height - 1

  const row = Math.floor(y / cellSize)
  const column = Math.floor(x / cellSize)

  return [row, column]
}

const registerInRectangleGrid: Behavior = (agent: IAgent) => {
  const tuple = getColAndRow(agent)
  const index = getIndex(tuple)

  if (!Array.isArray(rectangleGrid.content[index])) {
    rectangleGrid.content[index] = []
  }

  rectangleGrid.content[index].push(agent)
}

function resetGrid() {
  for (let index = 0; index < rectangleGrid.content.length; index++) {
    rectangleGrid.content[index] = []
  }
}

function getNeighboringCells([row, column]: [number, number]) {
  const tuples: [number, number][] = [
    [row - 1, column - 1],
    [row - 1, column],
    [row - 1, column + 1],
    [row, column - 1],
    [row, column],
    [row, column + 1],
    [row + 1, column - 1],
    [row + 1, column],
    [row + 1, column + 1],
  ]

  if (rectangleGrid.behavior === "torus") {
    if (row === 0) {
      tuples[0][0] = rectangleGrid.rowsCount - 1
      tuples[1][0] = rectangleGrid.rowsCount - 1
      tuples[2][0] = rectangleGrid.rowsCount - 1
    }

    if (row === rectangleGrid.rowsCount - 1) {
      tuples[6][0] = 0
      tuples[7][0] = 0
      tuples[8][0] = 0
    }

    if (column === 0) {
      tuples[0][1] = rectangleGrid.colsCount - 1
      tuples[3][1] = rectangleGrid.colsCount - 1
      tuples[6][1] = rectangleGrid.colsCount - 1
    }

    if (column === rectangleGrid.colsCount - 1) {
      tuples[2][1] = 0
      tuples[5][1] = 0
      tuples[8][1] = 0
    }
  } else if (rectangleGrid.behavior === "bounce") {
    //delete create sparse array, which is exactly what we want
    if (row === 0) {
      delete tuples[0]
      delete tuples[1]
      delete tuples[2]
    }
    if (row === rectangleGrid.rowsCount) {
      delete tuples[6]
      delete tuples[7]
      delete tuples[8]
    }
    if (column === 0) {
      delete tuples[0]
      delete tuples[3]
      delete tuples[6]
    }
    if (column === rectangleGrid.colsCount) {
      delete tuples[2]
      delete tuples[5]
      delete tuples[8]
    }
  }

  return tuples
}

function getNeighborsFromGrid(agent: IAgent, depth = 0): IAgent[] {
  const [row, column] = getColAndRow(agent)
  let neighboringCells: [number, number][] = []

  if (depth > 1) {
    throw Error(
      "Not yet implemented. getNeighborsFromGrid only works for depth 0 or 1",
    )
  } else if (depth === 1) {
    neighboringCells = getNeighboringCells([row, column])
  } else {
    neighboringCells = [[row, column]]
  }

  return neighboringCells
    .reduce((acc, tuple) => {
      if (tuple) {
        acc.push(getIndex(tuple))
      }
      return acc
    }, [] as number[])
    .reduce((acc, index) => {
      const list = rectangleGrid.content[index]
      if (Array.isArray(list)) {
        acc = acc.concat(list)
      }
      return acc
    }, [] as IAgent[])
}
