import { Vector3 } from "three"
import { FlowField } from "../FlowField"

export class PreComputedFlowField implements FlowField {
  width: number
  height: number
  cols: number
  rows: number
  fields: Vector3[] = []

  constructor(
    _width: number,
    _height: number,
    readonly cellSize: number,
    readonly flowField: FlowField,
  ) {
    let remainder

    const w = Math.ceil(_width),
      h = Math.ceil(_height)

    this.width =
      (remainder = w % cellSize) === 0 ? w : w + (cellSize - remainder)
    this.height =
      (remainder = h % cellSize) === 0 ? h : h + (cellSize - remainder)

    this.cols = cellSize > 0 ? this.width / cellSize : 0
    this.rows = cellSize > 0 ? this.height / cellSize : 0

    this.#fill()
  }

  #fill() {
    const { rows, cols, cellSize } = this

    const halfCell = Math.round(cellSize / 2)

    const here = new Vector3()

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        here.set(j * cellSize + halfCell, i * cellSize + halfCell, 0)
        this.fields[i * cols + j] = this.flowField.lookup(here)
      }
    }
  }

  lookup(position: { x: number; y: number }): Vector3 {
    if (this.cellSize <= 0) return new Vector3()
    if (position.x < 0 || position.x >= this.width) return new Vector3()
    if (position.y < 0 || position.y >= this.height) return new Vector3()

    const row = Math.floor(position.y / this.cellSize)
    const column = Math.floor(position.x / this.cellSize)
    const index = row * this.cols + column

    return this.fields[index] instanceof Vector3
      ? this.fields[index].clone()
      : new Vector3()
  }
}
