import { MathUtils, Vector3 } from "three"

type Viewport = {
  width: number
  height: number
}

type FillCanvasArgs = {
  text: string
  font: string
  color: string
}

export class FlowfieldFromText {
  width: number
  height: number
  ctx: CanvasRenderingContext2D
  imgData: ImageData
  canvas: HTMLCanvasElement
  fields: Vector3[]
  cellSize: number
  cols: number

  constructor({ width, height }: Viewport) {
    this.width = width
    this.height = height

    this.#createCanvas()
  }

  #createCanvas() {
    const canvas = document.createElement("canvas")
    const ctx = canvas.getContext("2d") as CanvasRenderingContext2D

    // Set display size (css pixels).
    canvas.style.width = `${this.width}px`
    canvas.style.height = `${this.height}px`

    // Set actual size in memory (scaled to account for extra pixel density).
    const scale = window.devicePixelRatio // Change to 1 on retina screens to see blurry canvas.
    canvas.width = Math.floor(this.width * scale)
    canvas.height = Math.floor(this.height * scale)

    // Normalize coordinate system to use CSS pixels.
    ctx.scale(scale, scale)

    this.ctx = ctx
    this.canvas = canvas
  }

  fillCanvas({ text, font, color }: FillCanvasArgs) {
    const { ctx, width, height } = this
    ctx.fillStyle = color
    //loading font, see https://usefulangle.com/post/74/javascript-dynamic-font-loading
    ctx.font = font
    ctx.textAlign = "center"
    ctx.textBaseline = "middle"

    const x = width / 2
    const y = height / 2

    const textString = text
    ctx.fillText(textString, x, y)
  }

  createFlowField(cellSize = 10) {
    this.cellSize = cellSize

    if (
      cellSize === 0 ||
      this.width % cellSize !== 0 ||
      this.height % cellSize !== 0
    ) {
      throw Error("CellSize must be not null perfect divisor for width/height")
    }

    const cols = this.width / cellSize
    const rows = this.height / cellSize

    this.cols = cols

    let reds = []
    const averageReds = []

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        const myImageData = this.ctx.getImageData(
          j * cellSize,
          i * cellSize,
          cellSize,
          cellSize,
        )
        const data = myImageData.data
        reds = []
        for (let i = 0; i < data.length; i += 4) {
          reds.push(data[i])
        }
        const sum = reds.reduce((a, c) => a + c, 0)
        averageReds[i * cols + j] = sum / reds.length
      }
    }

    this.fields = averageReds.map((red) => {
      if (red !== 0) {
        const angle = MathUtils.mapLinear(red, 0, 255, 0, Math.PI * 2)
        return new Vector3(Math.cos(angle), Math.sin(angle), 0)
      } else {
        return new Vector3()
      }
    })
  }

  lookup(position: { x: number; y: number }): Vector3 {
    if (this.cellSize <= 0) return new Vector3()
    if (position.x < 0 || position.x >= this.width) return new Vector3()
    if (position.y < 0 || position.y >= this.height) return new Vector3()

    const row = Math.floor(position.y / this.cellSize)
    const column = Math.floor(position.x / this.cellSize)
    const index = row * this.cols + column

    return this.fields[index] instanceof Vector3
      ? this.fields[index].clone()
      : new Vector3()
  }

  #getRedIndiceForCoord(x: number, y: number) {
    const red = y * (this.width * 4) + x * 4
    return red
  }
}
