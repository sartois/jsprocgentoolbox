import {
  ArrowHelper,
  BufferGeometry,
  Color,
  Float32BufferAttribute,
  LineBasicMaterial,
  LineSegments,
  Object3D,
  Vector3,
} from "three"
import { extend, Object3DNode } from "@react-three/fiber"
import { PreComputedFlowField } from "./PreComputedFlowField"

export class FlowFieldHelper extends Object3D {
  constructor(
    readonly flowField: PreComputedFlowField,
    gridColor = new Color(0xff00ff),
    arrowColor = new Color(0xff00ff),
    showGrid = false,
  ) {
    super()

    //@ts-ignore
    this.type = "FlowFieldHelper"
    //eslint-disable-next-line no-constant-condition
    if (showGrid && false) {
      this.add(new FlowFieldGrid(flowField, gridColor))
    }
    this.add(this.addArrows(arrowColor))
  }

  addArrows(arrowColor: Color = new Color(0xff0000)) {
    const halfCell = this.flowField.cellSize / 2
    const widthEnd = this.flowField.width
    const heightEnd = this.flowField.height
    const arrows = new Object3D()

    for (let i = halfCell; i < heightEnd; i += this.flowField.cellSize) {
      for (let j = halfCell; j < widthEnd; j += this.flowField.cellSize) {
        const dir = this.flowField.lookup({ x: j, y: i })
        if (dir != null && dir.length() > 0) {
          arrows.add(
            new ArrowHelper(
              new Vector3(dir.x, dir.y, 0),
              new Vector3(j, i, 0),
              halfCell,
              arrowColor,
              0.5 * halfCell,
              0.4 * halfCell,
            ),
          )
        }
      }
    }

    return arrows
  }
}

//DONT USE NOW
//@todo fix it, cols are not displayed
//FPS very low
export class FlowFieldGrid extends LineSegments {
  /**
   * @param {FlowField} flowField
   * @param {Color} color
   */
  constructor(
    flowField: { height: number; cellSize: number; width: number },
    color: Color,
  ) {
    const vertices = []
    const colors: number[] = []
    let k = 0

    for (let i = 0; i <= flowField.height; i += flowField.cellSize) {
      vertices.push(0, i, 0, flowField.width, i, 0)
      color.toArray(colors, k)
      k += 3
      color.toArray(colors, k)
      k += 3
    }

    for (let j = 0; j <= flowField.width; j += flowField.cellSize) {
      vertices.push(j, 0, 0, j, 0, flowField.height)
      color.toArray(colors, k)
      k += 3
      color.toArray(colors, k)
      k += 3
    }

    const geometry = new BufferGeometry()
    geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3))
    geometry.setAttribute("color", new Float32BufferAttribute(colors, 3))

    const material = new LineBasicMaterial({
      vertexColors: true,
      toneMapped: false,
    })

    super(geometry, material)
    //@ts-ignore
    this.type = "FlowFieldGrid"
  }
}

extend({ FlowFieldHelper, FlowFieldGrid })

declare module "@react-three/fiber" {
  interface ThreeElements {
    flowFieldHelper: Object3DNode<FlowFieldHelper, typeof FlowFieldHelper>
  }
}
