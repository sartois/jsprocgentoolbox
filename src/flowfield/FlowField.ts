import { Vector3 } from "three"

export interface FlowField {
  lookup(
    position: { x: number; y: number; z?: number },
    time?: number | undefined,
  ): Vector3
}
