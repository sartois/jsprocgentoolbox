import { MathUtils, Vector3 } from "three"
import {
  NoiseFunction4D,
  NoiseFunction3D,
  createNoise3D,
  createNoise4D,
} from "simplex-noise"
import { FlowField } from "./FlowField"

export class SimplexFlowField3D implements FlowField {
  noise3D: NoiseFunction3D
  noise4D: NoiseFunction4D

  //720°
  private static readonly upperLimit: number = 2 * Math.PI * 2

  //trial and error
  private static readonly scaleDownFactor: number = 0.0005

  private static shift = 40000

  constructor() {
    this.noise3D = createNoise3D()
    this.noise4D = createNoise4D()
  }

  lookup(
    position: { x: number; y: number; z: number },
    time?: number | undefined,
  ): Vector3 {
    let theta
    let phi

    if (time === undefined) {
      theta =
        this.noise3D(
          position.x * SimplexFlowField3D.scaleDownFactor,
          position.y * SimplexFlowField3D.scaleDownFactor,
          position.z * SimplexFlowField3D.scaleDownFactor,
        ) + 1

      phi =
        this.noise3D(
          (position.x + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
          (position.y + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
          (position.z + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
        ) + 1
    } else {
      theta =
        this.noise4D(
          position.x * SimplexFlowField3D.scaleDownFactor,
          position.y * SimplexFlowField3D.scaleDownFactor,
          position.z * SimplexFlowField3D.scaleDownFactor,
          time * SimplexFlowField3D.scaleDownFactor,
        ) + 1

      phi =
        this.noise4D(
          (position.x + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
          (position.y + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
          (position.z + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
          (time + SimplexFlowField3D.shift) *
            SimplexFlowField3D.scaleDownFactor,
        ) + 1
    }

    //Map from 0 to 720°, see https://www.bit-101.com/blog/2021/07/mapping-perlin-noise-to-angles/
    const mappedTheta = MathUtils.mapLinear(
      theta,
      0,
      2,
      0,
      SimplexFlowField3D.upperLimit,
    )
    const mappedPhi = MathUtils.mapLinear(
      phi,
      0,
      2,
      0,
      SimplexFlowField3D.upperLimit,
    )

    return new Vector3(
      Math.cos(mappedTheta) * Math.sin(mappedPhi),
      Math.sin(mappedTheta) * Math.sin(mappedPhi),
      Math.cos(mappedPhi),
    )
  }
}
