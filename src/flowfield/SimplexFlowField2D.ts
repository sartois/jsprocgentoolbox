import { MathUtils, Vector3 } from "three"
import {
  NoiseFunction2D,
  NoiseFunction3D,
  createNoise2D,
  createNoise3D,
} from "simplex-noise"
import { FlowField } from "./FlowField"

export class SimplexFlowField2D implements FlowField {
  noise3D: NoiseFunction3D
  noise2D: NoiseFunction2D

  //720°
  private static readonly upperLimit: number = 2 * Math.PI * 2

  //trial and error
  private static readonly scaleDownFactor: number = 0.0005

  constructor() {
    this.noise3D = createNoise3D()
    this.noise2D = createNoise2D()
  }

  lookup(
    position: { x: number; y: number; z?: number },
    time?: number | undefined,
  ): Vector3 {
    let noise

    if (time === undefined) {
      noise =
        this.noise2D(
          position.x * SimplexFlowField2D.scaleDownFactor,
          position.y * SimplexFlowField2D.scaleDownFactor,
        ) + 1
    } else {
      noise =
        this.noise3D(
          position.x * SimplexFlowField2D.scaleDownFactor,
          position.y * SimplexFlowField2D.scaleDownFactor,
          time * SimplexFlowField2D.scaleDownFactor,
        ) + 1
    }

    //Map from 0 to 720°, see https://www.bit-101.com/blog/2021/07/mapping-perlin-noise-to-angles/
    const mapped = MathUtils.mapLinear(
      noise,
      0,
      2,
      0,
      SimplexFlowField2D.upperLimit,
    )

    return new Vector3(Math.cos(mapped), Math.sin(mapped), 0)
  }
}
