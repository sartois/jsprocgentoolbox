import { proxy } from "valtio"
import { devtools } from "valtio/utils"
import merge from "lodash.merge"
import {
  type WanderSettings,
  type WalkSettings,
  type PursueSettings,
  type EvadeSettings,
  type SeparateSettings,
  type AlignSettings,
  type CohesionSettings,
  BehaviorKind,
} from "../behaviors"
import { FlowField } from "../flowfield/FlowField"
import { IAgent } from "../agents"
import {
  getAlignDefaults,
  getCohesionDefaults,
  getEvadeDefault,
  getPursueDefault,
  getSeparateDefaults,
  getWanderDefaults,
} from "../behaviors"
import { type BoundariesSettings, getBoundariesDefaults } from "../boundary"

export interface FlockSettings {
  world: {
    is2D: boolean
  }
  agent: {
    maxSpeed: number
    maxForce: number
    height: number
    count: number
  }
  wander: WanderSettings
  boundaries: BoundariesSettings
  separate: SeparateSettings
  align: AlignSettings
  cohesion: CohesionSettings
  neighbors: {
    useGrid: boolean
    gridCellSize: number
  }
  walk: WalkSettings
  pursue: PursueSettings
  evade: EvadeSettings
  balance: {
    priorityBehavior?: string | undefined
    priorities?: [BehaviorKind | string, number][]
  }
}

interface FlockState {
  pursueTarget: IAgent | undefined
  evadeFrom: IAgent | undefined
  flock: IAgent[]
  viewport: { width: number; height: number } | undefined
  debug: boolean
  settings: FlockSettings | undefined
  is2D: boolean
  flowfield: FlowField | undefined
}

const curFlockState: FlockState = {
  pursueTarget: undefined,
  evadeFrom: undefined,
  flock: [],
  viewport: undefined,
  debug: true,
  settings: undefined,
  is2D: false,
  flowfield: undefined,
}

export const state = proxy(curFlockState)

const nodeEnv = process.env.NODE_ENV

if (nodeEnv !== "test") {
  /*const unsub = */ devtools(state, { name: "state name", enabled: true })
}

export function mergeSettingsWithDefault(
  settings: Partial<FlockSettings>,
): FlockSettings {
  if (!settings.agent?.height) {
    console.warn(
      "settings.agent.height is not set, arbitrarily set base height to 25",
    )
  }
  const baseHeight = settings.agent?.height ?? 25

  const flockSettingsDefault: FlockSettings = {
    world: {
      is2D: false,
    },
    agent: {
      maxSpeed: 5,
      maxForce: 0.05,
      height: baseHeight,
      count: 5,
    },
    boundaries: {
      ...getBoundariesDefaults(baseHeight),
    },
    wander: {
      ...getWanderDefaults(baseHeight),
    },
    separate: {
      ...getSeparateDefaults(baseHeight),
    },
    align: {
      ...getAlignDefaults(baseHeight),
    },
    cohesion: {
      ...getCohesionDefaults(baseHeight),
    },
    neighbors: {
      useGrid: false,
      gridCellSize: 0,
    },
    walk: {
      turnChance: 0,
      useSteppedAngle: false,
    },
    pursue: {
      ...getPursueDefault(baseHeight),
    },
    evade: {
      ...getEvadeDefault(baseHeight),
    },
    balance: {
      priorityBehavior: undefined,
      priorities: [],
    },
  }

  return merge(flockSettingsDefault, settings)
}
