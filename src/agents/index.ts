import { Vector3 } from "three"

export interface IAgent {
  id: number

  acceleration: Vector3
  velocity: Vector3
  position: Vector3

  maxSpeed: number
  maxForce: number

  update(delta?: number): void
  applyForce(force: Vector3): void
}

export interface AgentArgs {
  acceleration?: Vector3
  velocity?: Vector3
  position: Vector3
}

export interface AgentParam {
  maxForce: number
  maxSpeed: number
}

export class MovingAgent implements IAgent {
  acceleration: Vector3
  velocity: Vector3
  position: Vector3
  id: number

  static counter = 0

  constructor(
    agent: AgentArgs,
    readonly maxSpeed = 5,
    readonly maxForce = 0.05,
    id: number | undefined = undefined,
  ) {
    this.acceleration = agent.acceleration || new Vector3()
    this.velocity = agent.velocity || new Vector3()
    this.position = agent.position
    this.id = id ?? ++MovingAgent.counter
  }

  update(delta = 1): void {
    this.acceleration.multiplyScalar(delta)
    this.velocity.add(this.acceleration)
    this.velocity.clampLength(0, this.maxSpeed)
    this.position.add(this.velocity)
    this.acceleration.multiplyScalar(0)
  }

  applyForce(force: Vector3): void {
    this.acceleration.add(force)
  }
}
