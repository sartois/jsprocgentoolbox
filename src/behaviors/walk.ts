import { Vector3 } from "three"
import { random } from "../utils/mathUtil"
import { FlockSettings, state } from "../store"
import { IAgent } from "../agents"
import { BehaviorKind, TypedVector3 } from "."

export type WalkSettings = {
  turnChance: number
  useSteppedAngle: boolean
}

const cos45 = 0.707106781186548

export function walk(
  agent: IAgent,
  settings: FlockSettings,
  _: IAgent[] | undefined,
  rng = Math.random,
): TypedVector3 | void {
  const { turnChance, useSteppedAngle } = settings.walk

  //Dir change chance
  if (turnChance > rng()) {
    if (state.is2D) {
      const v = {
        x: 0,
        y: 0,
        z: 0,
      }

      if (useSteppedAngle) {
        if (rng() > 0.5) {
          v.x = cos45
          v.y = cos45
        } else {
          v.x = -cos45
          v.y = -cos45
        }
      } else {
        const a = random(-Math.PI, Math.PI)
        v.x = Math.cos(a)
        v.y = Math.sin(a)
      }

      agent.velocity.add(v as Vector3).normalize()
    } else {
      agent.velocity.randomDirection()
    }
  }

  const f = new Vector3() as TypedVector3
  f.kind = BehaviorKind.Walk
  return f
    .copy(agent.velocity)
    .normalize()
    .multiplyScalar(agent.maxSpeed)
    .clampLength(0, agent.maxForce)
}
