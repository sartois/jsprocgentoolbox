import { Vector3 } from "three"
import { IAgent } from "../agents"
import { type FlockSettings } from "../store"
import { getNeighbors } from "../boundary"
import { BehaviorKind, TypedVector3 } from "."

export type AlignSettings = {
  threshold?: number
  depth?: 0 | 1 | 2
}

export function getAlignDefaults(baseHeight: number): AlignSettings {
  return {
    threshold: baseHeight * 2,
    depth: 1,
  }
}

//Compute neighbor average velocity
export function align(
  agent: IAgent,
  settings: FlockSettings,
  flock?: IAgent[],
): TypedVector3 | void {
  const { useGrid } = settings.neighbors
  const { threshold, depth } = settings.align

  const neighbors = getNeighbors({ agent, flock, threshold, depth, useGrid })
  if (neighbors.length === 0) return

  const sum = new Vector3() as TypedVector3

  neighbors.forEach((a) => sum.add(a.velocity))
  sum
    .divideScalar(neighbors.length)
    .normalize()
    .multiplyScalar(agent.maxSpeed)
    .clampLength(0, agent.maxForce)
  sum.kind = BehaviorKind.Align
  return sum
}
