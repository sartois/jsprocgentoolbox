import { Vector3 } from "three"
import { IAgent } from "../agents"
import { FlockSettings } from "../store"
import { getNeighbors } from "../boundary"
import { BehaviorKind, TypedVector3 } from "."

export type SeparateSettings = {
  threshold?: number
  depth?: 0 | 1 | 2
}

export function getSeparateDefaults(baseHeight: number): SeparateSettings {
  return {
    threshold: baseHeight,
    depth: 0,
  }
}

const diff = new Vector3()

//Checks for nearby agents and steers away
//source https://github.com/nature-of-code/noc-examples-processing/blob/master/chp06_agents/NOC_6_09_Flocking/Boid.pde
export function separate(
  agent: IAgent,
  settings: FlockSettings,
  flock?: IAgent[],
): TypedVector3 | void {
  const { threshold, depth } = settings.separate
  const { useGrid } = settings.neighbors

  const neighbors = getNeighbors({ agent, flock, threshold, depth, useGrid })
  if (neighbors.length === 0) return

  const steer = new Vector3() as TypedVector3
  let distinctCounter = 0

  neighbors.forEach((a) => {
    const d = agent.position.distanceToSquared(a.position)
    if (d !== 0) {
      distinctCounter++
      diff.subVectors(agent.position, a.position)
      diff.normalize().divideScalar(d)
      steer.add(diff)
    }
  })

  if (distinctCounter > 0) {
    steer.divideScalar(distinctCounter)
  }

  steer
    .normalize()
    .multiplyScalar(agent.maxSpeed)
    .sub(agent.velocity)
    .clampLength(0, agent.maxForce)

  steer.kind = BehaviorKind.Separate

  return steer
}
