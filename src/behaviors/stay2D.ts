import { IAgent } from "../agents"

export function stay2D(agent: IAgent): void {
  agent.position.setZ(0)
  agent.velocity.setZ(0)
}
