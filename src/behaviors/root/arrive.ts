import { Vector3, Vector3Tuple } from "three"
import { IAgent } from "../../agents"
import { MathUtils } from "three"

const desired = new Vector3(),
  steer = new Vector3()

export function arrive(
  agent: IAgent,
  target: Vector3,
  threshold: number,
): Vector3Tuple {
  desired.subVectors(target, agent.position)

  const distance = desired.length()
  desired.normalize()
  desired.multiplyScalar(
    MathUtils.mapLinear(distance, 0, threshold, 0, agent.maxSpeed),
  )

  steer.subVectors(desired, agent.velocity)
  steer.clampLength(0, agent.maxForce)

  return steer.toArray()
}
