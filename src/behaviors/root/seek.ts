import { Vector3, Vector3Tuple } from "three"
import { IAgent } from "../../agents"

const desired = new Vector3(),
  steer = new Vector3()

//Compute a steering force towards a target
//Reynolds: STEER = TARGET - VELOCITY
//See http://www.red3d.com/cwr/papers/1999/gdc99steer.pdf
export function seek(
  agent: IAgent,
  target: Vector3,
  clampLength?: boolean,
): Vector3Tuple {
  if (clampLength === undefined) clampLength = true
  desired
    .subVectors(target, agent.position)
    .normalize()
    .multiplyScalar(agent.maxSpeed)
  steer.subVectors(desired, agent.velocity)
  if (clampLength) {
    steer.clampLength(0, agent.maxForce)
  }
  return steer.toArray()
}
