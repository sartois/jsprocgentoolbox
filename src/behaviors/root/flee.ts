import { Vector3, Vector3Tuple } from "three"
import { IAgent } from "../../agents"
import { seek } from "./seek"

const force = new Vector3()

export function flee(agent: IAgent, target: Vector3): Vector3Tuple {
  return force.fromArray(seek(agent, target)).multiplyScalar(-1).toArray()
}
