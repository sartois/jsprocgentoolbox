import { Vector3 } from "three"
import { IAgent } from "../../agents"
import { Behavior, BehaviorKind, TypedVector3 } from ".."
import { type FlockSettings } from "../../store"

interface BehaviorLoopParam {
  agent: IAgent
  behaviors: Behavior[]
  flock: IAgent[]
  delta: number
  settings: FlockSettings
}

export function behaviorLoop({
  agent,
  behaviors,
  flock,
  delta,
  settings,
}: BehaviorLoopParam) {
  if (
    settings.neighbors.useGrid &&
    !behaviors.some((f) => f.name === "registerInGrid")
  ) {
    logOnce(
      "settings.neighbors.useGrid is true, but registerInGrid is not used in behaviors",
    )
  }

  let forces = behaviors
    .map((behavior) => behavior(agent, settings, flock))
    .filter((v) => v instanceof Vector3) as TypedVector3[]

  //avoid blinking
  if (behaviors.some((f) => f.name === "stay2D")) {
    forces.map((f) => f.setZ(0))
  }

  const { priorityBehavior, priorities } = settings.balance

  if (priorityBehavior) {
    const primaryForce = forces.find((f) => f.kind === priorityBehavior)
    if (primaryForce) {
      forces = [primaryForce]
    }
  }

  if (priorities && priorities.length > 0) {
    const _prio = Object.fromEntries(priorities)
    forces.map((f) =>
      _prio[f.kind as keyof typeof BehaviorKind]
        ? f.multiplyScalar(_prio[f.kind as keyof typeof BehaviorKind])
        : f,
    )
  }

  forces.map((v) => agent.applyForce(v))
  agent.update(delta)
}

const logOnce = (function () {
  let done = false
  return function (message: string) {
    if (!done) {
      done = true
      console.warn(message)
    }
  }
})()
