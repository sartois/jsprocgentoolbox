import { Object3D } from "three"
import { IAgent } from "../../agents"

export function followPosition(agent: IAgent, mesh: Object3D, is2D = false) {
  mesh.position.set(
    agent.position.x,
    agent.position.y,
    is2D ? 0 : agent.position.z,
  )
}
