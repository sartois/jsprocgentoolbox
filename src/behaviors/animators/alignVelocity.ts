import { Object3D, Vector3 } from "three"
import { IAgent } from "../../agents"

const up = new Vector3(0, 1, 0)
const vel = new Vector3()

export function alignVelocity(agent: IAgent, mesh: Object3D) {
  mesh.quaternion.setFromUnitVectors(up, vel.copy(agent.velocity).normalize())
}
