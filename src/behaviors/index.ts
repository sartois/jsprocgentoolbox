import { Vector3 } from "three"
import { FlockSettings } from "../store"
import { IAgent } from "../agents"

export { alignVelocity } from "./animators/alignVelocity"
export { followPosition } from "./animators/followPosition"
export { behaviorLoop } from "./helper/behaviorLoop"
export { align, type AlignSettings, getAlignDefaults } from "./align"
export {
  cohesion,
  type CohesionSettings,
  getCohesionDefaults,
} from "./cohesion"
export { evade, type EvadeSettings, getEvadeDefault } from "./evade"
export { followFlowfield } from "./followFlowfield"
export { pursue, type PursueSettings, getPursueDefault } from "./pursue"
export {
  separate,
  type SeparateSettings,
  getSeparateDefaults,
} from "./separate"
export { stay2D } from "./stay2D"
export { walk, type WalkSettings } from "./walk"
export { wander, type WanderSettings, getWanderDefaults } from "./wander"

export { seek } from "./root/seek"
export { flee } from "./root/flee"
export { arrive } from "./root/arrive"

export interface TypedVector3 extends Vector3 {
  kind: string
}

export interface Behavior {
  (
    agent: IAgent,
    settings: FlockSettings,
    flock?: IAgent[],
  ): TypedVector3 | void
}

export enum BehaviorKind {
  Align = "align",
  Boundaries = "boundaries",
  Cohesion = "cohesion",
  Evade = "evade",
  Flowfield = "flowfield",
  Pursue = "pursue",
  Separate = "separate",
  Wander = "wander",
  Walk = "walk",
}
