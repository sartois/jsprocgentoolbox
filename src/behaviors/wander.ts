import { Vector3 } from "three"
import { IAgent } from "../agents"
import { random } from "../utils/mathUtil"
import { seek } from "./root/seek"
import { FlockSettings } from "../store"
import { Behavior, BehaviorKind, TypedVector3 } from "."

export type WanderSettings = {
  /**
   * The "wander sphere" radius
   */
  radius: number
  /**
   * The "wander sphere" center position, following agent velocity
   */
  distance: number
  /**
   * Maximun variation in radian
   */
  variation: number
}

export function getWanderDefaults(baseHeight: number): WanderSettings {
  return {
    radius: baseHeight * 2,
    distance: baseHeight * 0.5,
    variation: 0.52, //Math.PI/6
  }
}

type remembers = {
  lastTheta: number
  lastPhi: number
}

const memory: remembers[] = []

export const wander: Behavior = (agent: IAgent, settings: FlockSettings) => {
  if (!memory[agent.id]) {
    memory[agent.id] = {
      lastTheta: 0,
      lastPhi: 0,
    }
  }

  let { lastTheta, lastPhi } = memory[agent.id]

  const { radius, distance, variation } = settings.wander
  const halfVariation = variation / 2
  const spherePos = new Vector3() as TypedVector3
  spherePos.kind = BehaviorKind.Wander

  //get pos for wander sphere
  spherePos
    .copy(agent.velocity)
    .normalize()
    .multiplyScalar(distance)
    .add(agent.position)

  //displacement
  lastTheta = lastTheta + random(-variation, variation)
  lastPhi = lastPhi + random(-halfVariation, halfVariation)

  memory[agent.id] = {
    lastTheta,
    lastPhi,
  }

  const offset = {
    x: radius * Math.cos(lastTheta) * Math.sin(lastPhi),
    y: radius * Math.sin(lastTheta) * Math.sin(lastPhi),
    z: radius * Math.cos(lastPhi),
  }

  spherePos.add(offset as Vector3)

  return spherePos.fromArray(seek(agent, spherePos))
}
