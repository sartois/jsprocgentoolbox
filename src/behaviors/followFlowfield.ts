import { IAgent } from "../agents"
import { state } from "../store"
import { BehaviorKind, TypedVector3 } from "."

//From Reynolds https://www.red3d.com/cwr/steer/gdc99/ :
//> The implementation of flow field following is very simple. The future position of a character is estimated and the flow field is sampled at that location. This flow direction (vector F in Figure 11) is the “desired velocity” and the steering direction (vector S) is simply the difference between the current velocity (vector V) and the desired velocity.
//To do that we should have a flowfield that extends behind boundaries, which is not the case
//Sample the flow field at current location
export function followFlowfield(agent: IAgent): TypedVector3 | void {
  if (state.flowfield === undefined) {
    console.warn("Flowfield is undefined")
    return
  }

  const { x, y, z } = agent.position

  const noise = state.flowfield.lookup({ x, y, z }) as TypedVector3
  noise.kind = BehaviorKind.Flowfield
  noise.setLength(agent.maxSpeed)
  noise.sub(agent.velocity).clampLength(0, agent.maxForce)

  return noise
}
