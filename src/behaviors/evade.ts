import { Vector3 } from "three"
import { IAgent } from "../agents"
import { FlockSettings, state } from "../store"
import { flee } from "./root/flee"
import { BehaviorKind, TypedVector3 } from "."
import { subscribeKey } from "valtio/utils"

export interface EvadeSettings {
  /**
   * Distance from which agents start to evade
   */
  thresold: number
  /**
   * A simple estimator of moderate quality is T=Dc where D is the distance between pursuer and quarry, and c is a turning parameter.
   */
  c: number
}

export function getEvadeDefault(baseHeight: number): EvadeSettings {
  return {
    thresold: baseHeight * 2,
    c: 0.05,
  }
}

let evadeFrom: IAgent | undefined

export function evade(
  agent: IAgent,
  settings: FlockSettings,
): TypedVector3 | void {
  subscribeOnce(() => {
    subscribeKey(state, "evadeFrom", (a) => {
      evadeFrom = a
    })
  })

  const { thresold, c } = settings.evade

  if (evadeFrom) {
    const d = agent.position.distanceToSquared(evadeFrom.position)
    if (d < thresold * thresold) {
      const v = new Vector3() as TypedVector3
      v.kind = BehaviorKind.Evade
      //see https://www.red3d.com/cwr/steer/gdc99/
      const T = Math.sqrt(d) * c
      v.copy(evadeFrom.velocity).multiplyScalar(T).add(evadeFrom.position)
      v.fromArray(flee(agent, v))
      return v
    }
  }
}

const subscribeOnce = (function () {
  let done = false
  return function (subscribe: () => void) {
    if (!done) {
      done = true
      subscribe()
    }
  }
})()
