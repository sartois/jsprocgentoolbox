import { subscribeKey } from "valtio/utils"
import { IAgent } from "../agents"
import { FlockSettings, state } from "../store"
import { arrive } from "./root/arrive"
import { BehaviorKind, TypedVector3 } from "."

export interface PursueSettings {
  /**
   * A simple estimator of moderate quality is T=Dc where D is the distance between pursuer and quarry, and c is a turning parameter.
   */
  c?: number
  /**
   * The threshold from which agent will descelerate to arrive on target
   */
  threshold: number
}

export function getPursueDefault(baseHeight: number): PursueSettings {
  return {
    c: 0.005,
    threshold: baseHeight * 3,
  }
}

let target: IAgent | undefined

export function pursue(
  agent: IAgent,
  settings: FlockSettings,
): TypedVector3 | void {
  subscribeOnce(() => {
    subscribeKey(state, "pursueTarget", (t) => {
      target = t
    })
  })

  const { c, threshold } = settings.pursue

  if (!target) {
    return
  }

  const T = agent.position.distanceTo(target.position) * (c ?? 1)
  const future = target.velocity
    .clone()
    .multiplyScalar(T)
    .add(target.position) as TypedVector3
  future.kind = BehaviorKind.Pursue
  return future.fromArray(arrive(agent, future, threshold))
}

const subscribeOnce = (function () {
  let done = false
  return function (subscribe: () => void) {
    if (!done) {
      done = true
      subscribe()
    }
  }
})()
