import { Vector3 } from "three"
import { IAgent } from "../agents"
import { seek } from "./root/seek"
import { FlockSettings } from "../store"
import { getNeighbors } from "../boundary"
import { BehaviorKind, TypedVector3 } from "."

export interface CohesionSettings {
  threshold?: number
  depth?: 0 | 1 | 2
}

export function getCohesionDefaults(baseHeight: number): CohesionSettings {
  return {
    threshold: baseHeight * 2,
    depth: 1,
  }
}

//steering vector towards average position of all nearby agent
export function cohesion(
  agent: IAgent,
  settings: FlockSettings,
  flock?: IAgent[],
): TypedVector3 | void {
  const { useGrid } = settings.neighbors
  const { threshold, depth } = settings.cohesion

  const neighbors = getNeighbors({ agent, flock, threshold, depth, useGrid })
  if (neighbors.length === 0) return

  const sum = new Vector3() as TypedVector3
  sum.kind = BehaviorKind.Cohesion

  neighbors.forEach((a) => sum.add(a.position))
  sum.divideScalar(neighbors.length)

  return sum.fromArray(seek(agent, sum))
}
