export interface Transition {
  name: string
  from?: string
  to: string
  before?: () => void
}

export interface MachineParams {
  initial: string
  transitions: Transition[]
}

export class SimpleFSM extends EventTarget {
  #currentState: string | undefined
  transitions: Transition[] | undefined
  initial: string | undefined
  hasStarted = false

  setTransitions({ transitions, initial }: MachineParams) {
    this.transitions = transitions
    this.initial = initial
  }

  #findTransition(name: string) {
    const transition = this.transitions!.find((t) => t.name === name)
    if (!transition) {
      throw Error(`Can't find transition ${name}`)
    }
    return transition
  }

  start() {
    if (!this.transitions)
      throw Error("Please call setTransitions before starting")
    if (!this.hasStarted) {
      const firstTransition = this.#findTransition(this.initial as string)
      this.hasStarted = true
      this.dispatch(firstTransition.name)
    }
  }

  dispatch(transitionName: string) {
    const transition = this.#findTransition(transitionName)
    if (transition.before) {
      transition.before()
    }
    this.#currentState = transition.to
    this.dispatchEvent(
      new CustomEvent("MachineStateChanged", { detail: transition.to }),
    )
  }

  getCurrentState() {
    return this.#currentState
  }
}
