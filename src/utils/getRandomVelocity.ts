import { random } from "mathjs"
import { Vector3 } from "three"

const angle = [
  new Vector3(1, 1, 0).normalize(),
  new Vector3(1, -1, 0).normalize(),
  new Vector3(-1, 1, 0).normalize(),
  new Vector3(-1, -1, 0).normalize(),
  new Vector3(1, 0, 0),
  new Vector3(-1, 0, 0),
  new Vector3(0, 1, 0),
  new Vector3(0, -1, 0),
]

export function getRandomVelocity(useSteppedAngle: boolean, is2D: boolean) {
  if (useSteppedAngle) {
    if (is2D) {
      return angle[Math.floor(random(1, 9)) - 1].clone()
    } else {
      throw Error("Not yet implemented")
    }
  } else {
    const vel = new Vector3().randomDirection()
    return is2D ? vel.setZ(0) : vel
  }
}
