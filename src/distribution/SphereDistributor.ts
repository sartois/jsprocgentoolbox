import { Vector3, MathUtils } from "three"
import { Position, NormalDistributionConfig } from "."
import { randomNormal } from "d3-random"

const TwoPI = Math.PI * 2

const dummy = new Vector3()

export class SphereDistributor /* extends Distributor*/ {
  radius: number

  constructor(radius: number) {
    //super()
    this.radius = radius
  }

  getEdgy(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { mu: 1, sigma: 0.1 }
    }

    const { mu, sigma } = config

    return this.getNormal(agentNb, { mu, sigma })
  }

  getUniform(agentNb: number) {
    return this.loop(agentNb, () => {
      const u = Math.random(),
        v = Math.random()
      const theta = u * TwoPI
      const phi = Math.acos(2.0 * v - 1.0)
      const r = Math.cbrt(Math.random())
      const sinTheta = Math.sin(theta)
      const cosTheta = Math.cos(theta)
      const sinPhi = Math.sin(phi)
      const cosPhi = Math.cos(phi)
      const x = r * sinPhi * cosTheta
      const y = r * sinPhi * sinTheta
      const z = r * cosPhi
      return {
        x: MathUtils.mapLinear(x, -1, 1, -this.radius, this.radius),
        y: MathUtils.mapLinear(y, -1, 1, -this.radius, this.radius),
        z: MathUtils.mapLinear(z, -1, 1, -this.radius, this.radius),
      }
    })
  }

  getNormal(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { mu: 0, sigma: 0.2 }
    }

    const { mu, sigma } = config

    const rngGauss = randomNormal(mu, sigma)
    return this.loop(agentNb, () => {
      let r = 0
      while (r <= 0 || r >= 1) {
        r = rngGauss()
      }
      r = MathUtils.mapLinear(r, 0, 1, 0, this.radius)
      dummy.randomDirection().multiplyScalar(r)
      return {
        x: dummy.x,
        y: dummy.y,
        z: dummy.z,
      }
    })
  }

  protected loop(
    agentNb: number,
    callback: (args: any) => Position,
  ): Position[] {
    return new Array(agentNb).fill(null).map(callback)
  }
}
