import { MathUtils } from "three"
import { NormalDistributionConfig, Position } from "."
import { randomUniform, randomNormal } from "d3-random"

export class BoxDistributor /*extends Distributor*/ {
  size: number
  halfSize: number

  constructor(halfSize: number) {
    //super()
    this.size = halfSize * 2
    this.halfSize = halfSize
  }

  getUniform(agentNb: number) {
    const rng = randomUniform(this.size)
    const keys: Array<keyof Position> = ["x", "y", "z"]
    //@ts-ignore
    return this.loop(agentNb, () =>
      Object.fromEntries(keys.map((n) => [n, rng() - this.halfSize])),
    )
  }

  getEdgy(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { sigma: 0.13 }
    }

    const { sigma } = config

    const rngGaussNorth = randomNormal(0, sigma),
      rngGaussSouth = randomNormal(1, sigma)

    return this.loop(agentNb, () => {
      const rngX = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth
      const rngY = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth
      const rngZ = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth

      let x = 0,
        y = 0,
        z = 0

      while (!(x > 0 && x < 1 && y > 0 && y < 1 && z > 0 && z < 1)) {
        if (!(x > 0 && x < 1)) x = rngX()
        if (!(y > 0 && y < 1)) y = rngY()
        if (!(z > 0 && z < 1)) z = rngZ()
      }

      return {
        x: MathUtils.mapLinear(x, 0, 1, 0, this.size) - this.halfSize,
        y: MathUtils.mapLinear(y, 0, 1, 0, this.size) - this.halfSize,
        z: MathUtils.mapLinear(z, 0, 1, 0, this.size) - this.halfSize,
      }
    })
  }

  getNormal(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { mu: 0.5, sigma: 0.1 }
    }

    const { mu, sigma } = config

    const rngGauss = randomNormal(mu, sigma)

    return this.loop(agentNb, () => ({
      x: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.size) - this.halfSize,
      y: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.size) - this.halfSize,
      z: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.size) - this.halfSize,
    }))
  }

  protected loop(
    agentNb: number,
    callback: (args: any) => Position,
  ): Position[] {
    return new Array(agentNb).fill(null).map(callback)
  }
}
