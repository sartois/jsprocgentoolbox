import { NormalDistributionConfig, Position } from "."
import { randomNormal, randomUniform } from "d3-random"
import { MathUtils } from "three"

export type Rectangle = {
  width: number
  height: number
}

export class RectangleDistributor /*extends Distributor*/ {
  width: number
  height: number
  halfWidth: number
  halfHeight: number

  constructor({ width, height }: Rectangle, originCentered = true) {
    //super()
    this.width = width
    this.height = height
    this.halfWidth = originCentered ? this.width / 2 : 0
    this.halfHeight = originCentered ? this.height / 2 : 0
  }

  getUniform(agentNb: number) {
    const rngX = randomUniform(this.width),
      rngY = randomUniform(this.height)

    return this.loop(agentNb, () => ({
      x: rngX() - this.halfWidth,
      y: rngY() - this.halfHeight,
      z: 0,
    }))
  }

  getNormal(agentNb: number, config?: NormalDistributionConfig): Position[] {
    if (config === undefined) {
      config = { mu: 0.5, sigma: 0.1 }
    }

    const { mu, sigma } = config

    const rngGauss = randomNormal(mu, sigma)

    return this.loop(agentNb, () => ({
      x: MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.width) - this.halfWidth,
      y:
        MathUtils.mapLinear(rngGauss(), 0, 1, 0, this.height) - this.halfHeight,
      z: 0,
    }))
  }

  getEdgy(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { sigma: 0.13 }
    }

    const { sigma } = config

    const rngGaussNorth = randomNormal(0, sigma),
      rngGaussSouth = randomNormal(1, sigma)

    return this.loop(agentNb, () => {
      const rngX = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth
      const rngY = Math.random() > 0.5 ? rngGaussNorth : rngGaussSouth

      let x = 0,
        y = 0

      while (!(x > 0 && x < 1 && y > 0 && y < 1)) {
        if (!(x > 0 && x < 1)) x = rngX()
        if (!(y > 0 && y < 1)) y = rngY()
      }

      return {
        x: MathUtils.mapLinear(x, 0, 1, 0, this.width) - this.halfWidth,
        y: MathUtils.mapLinear(y, 0, 1, 0, this.height) - this.halfHeight,
        z: 0,
      }
    })
  }

  protected loop(
    agentNb: number,
    callback: (args: any) => Position,
  ): Position[] {
    return new Array(agentNb).fill(null).map(callback)
  }
}
