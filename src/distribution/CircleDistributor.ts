import { NormalDistributionConfig, Position } from "."
import { randomNormal } from "d3-random"

const TwoPI = Math.PI * 2

export class CircleDistributor /*extends Distributor*/ {
  radius: number

  constructor(radius: number) {
    //super()
    this.radius = radius
  }

  getUniform(agentNb: number) {
    return this.loop(agentNb, () => {
      const r = this.radius * Math.sqrt(Math.random())
      const theta = Math.random() * TwoPI

      return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 }
    })
  }

  getEdgy(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { mu: 1, sigma: 0.2 }
    }

    const { mu, sigma } = config

    const rngGauss = randomNormal(mu, sigma)
    return this.loop(agentNb, () => {
      let h = 0
      while (h <= 0 || h >= 1) {
        h = rngGauss()
      }

      const r = this.radius * Math.sqrt(h)
      const theta = Math.random() * TwoPI

      return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 }
    })
  }

  getNormal(agentNb: number, config?: NormalDistributionConfig) {
    if (config === undefined) {
      config = { mu: 0.1, sigma: 0.3 }
    }

    const { mu, sigma } = config

    const rngGauss = randomNormal(mu, sigma)
    return this.loop(agentNb, () => {
      let h = 0
      while (h <= 0 || h >= 1) {
        h = rngGauss()
      }

      const r = this.radius * Math.pow(h, 2)
      const theta = Math.random() * TwoPI

      return { x: r * Math.cos(theta), y: r * Math.sin(theta), z: 0 }
    })
  }

  protected loop(
    agentNb: number,
    callback: (args: any) => Position,
  ): Position[] {
    return new Array(agentNb).fill(null).map(callback)
  }
}
