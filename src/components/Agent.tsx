import { PropsWithChildren, useRef } from "react"
import { Group } from "three"
import { useFrame } from "@react-three/fiber"
import { IAgent } from "../agents"
import {
  alignVelocity,
  followPosition,
  behaviorLoop,
  type Behavior,
} from "../behaviors"
import { state } from "../store"

export type AgentProps = {
  agent: IAgent
  behaviors: Behavior[]
  disableGameLoop?: boolean
}

export function Agent(props: PropsWithChildren<AgentProps>) {
  const { children, behaviors, disableGameLoop } = props
  const agent = props.agent
  const ref = useRef<Group>(null!)
  const { flock, is2D, settings } = state

  useFrame((_, delta) => {
    if (ref?.current && !disableGameLoop && settings) {
      behaviorLoop({ agent, behaviors, delta, flock, settings })
      alignVelocity(agent, ref.current)
      followPosition(agent, ref.current, is2D)
    }
  })

  return (
    <group position={agent.position} ref={ref}>
      {children}
    </group>
  )
}
