import { useFrame } from "@react-three/fiber"
import { PropsWithChildren, useMemo, useRef } from "react"
import { state } from "../store"
import { Group, Vector3 } from "three"
import { MovingAgent, IAgent } from "../agents"
import {
  alignVelocity,
  followPosition,
  behaviorLoop,
  type Behavior,
} from "../behaviors"
import { SimpleFSM } from "../machine/SimpleFSM"

interface AgentWithFSMProps {
  agent?: IAgent
  machine: SimpleFSM
  behaviors: [string, Behavior[] | []][]
}

export function AgentWithFSM(props: PropsWithChildren<AgentWithFSMProps>) {
  const ref = useRef<Group>(null!)
  const agent: IAgent =
    props.agent ?? new MovingAgent({ position: new Vector3() })

  const machine = props.machine
  const behaviors = props.behaviors

  const { flock, settings, is2D } = state

  const _behaviors = Object.fromEntries(behaviors)

  useMemo(() => machine.start(), [machine])

  useFrame((_, delta) => {
    if (ref?.current && settings) {
      behaviorLoop({
        agent,
        behaviors: _behaviors[machine.getCurrentState() as string],
        delta,
        settings,
        flock,
      })
      alignVelocity(agent, ref.current)
      followPosition(agent, ref.current, is2D)
    }
  })

  return (
    <group position={agent.position} ref={ref}>
      {props.children}
    </group>
  )
}
