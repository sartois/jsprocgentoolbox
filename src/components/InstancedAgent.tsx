import { PropsWithChildren, useRef, forwardRef } from "react"
import { InstancedMesh, Object3D, Vector3 } from "three"
import { mergeRefs } from "react-merge-refs"
import { useFrame } from "@react-three/fiber"
import { state } from "../store"
import { behaviorLoop, type Behavior } from "../behaviors"

export type InstancedAgentProps = {
  behaviors: Behavior[]
  disableGameLoop?: boolean
}

const up = new Vector3(0, 1, 0)
const temp = new Object3D()

export const InstancedAgent = forwardRef<InstancedMesh, InstancedAgentProps>(
  (props: PropsWithChildren<InstancedAgentProps>, ref) => {
    const { children, behaviors, disableGameLoop } = props
    const { flock, settings } = state

    if (!flock || flock.length === 0) {
      throw Error("Please populate flock")
    }

    const instanceCount = flock.length
    const localRef = useRef<InstancedMesh>()

    useFrame((_, delta) => {
      if (localRef?.current) {
        if (!disableGameLoop && settings) {
          flock.map((agent) => {
            behaviorLoop({ agent, behaviors, flock, delta, settings })
          })
        }

        flock.map((agent, i) => {
          temp.quaternion.setFromUnitVectors(
            up,
            agent.velocity.clone().normalize(),
          )
          temp.position.set(
            agent.position.x,
            agent.position.y,
            state.is2D ? 0 : agent.position.z,
          )
          temp.updateMatrix()
          localRef.current?.setMatrixAt(i, temp.matrix)
        })
        localRef.current.instanceMatrix.needsUpdate = true
      }
    })

    return (
      <instancedMesh
        ref={mergeRefs([localRef, ref])}
        args={[undefined, undefined, instanceCount]}
      >
        {children}
      </instancedMesh>
    )
  },
)
