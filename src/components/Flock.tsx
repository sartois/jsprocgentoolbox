import { PropsWithChildren, Fragment } from "react"
import { Vector3 } from "three"
import { useFrame, useThree } from "@react-three/fiber"
import { IAgent, MovingAgent } from "../agents"
import { FlockSettings, state, mergeSettingsWithDefault } from "../store"
import type { Position } from "../distribution"
import { createGrid, resetGrid } from "../boundary"
import { ref } from "valtio"

export type FlockProps = {
  distribution?: Position[]
  settings?: Partial<FlockSettings>
  render?: (agent: IAgent) => JSX.Element
  agents?: IAgent[]
  randomVelocity?: boolean
  sameVelocity?: Vector3
  setFlock?: (flock: IAgent[]) => void
}

export function Flock(props: PropsWithChildren<FlockProps>) {
  const {
    children,
    settings,
    render,
    distribution,
    agents,
    randomVelocity,
    sameVelocity,
    setFlock,
  } = props

  //settings handling

  const mergedSettings = mergeSettingsWithDefault(settings ?? {})

  //flock creation

  const { maxSpeed, maxForce } = mergedSettings.agent
  let flock: IAgent[] = []

  if (agents === undefined && distribution !== undefined) {
    flock = distribution.map(
      (pos, i) =>
        new MovingAgent(
          { position: new Vector3(pos.x, pos.y, pos.z) },
          maxSpeed,
          maxForce,
          i,
        ),
    )
    if (randomVelocity) {
      flock.map((agent) => agent.velocity.randomDirection())
    }
    if (sameVelocity) {
      flock.map((agent) => agent.velocity.copy(sameVelocity))
    }
  } else if (agents) {
    flock = agents
  }

  //populate state

  const { viewport } = useThree()
  state.viewport = viewport
  state.flock = ref(flock)
  state.settings = mergedSettings
  state.is2D = mergedSettings.world.is2D

  if (setFlock) {
    setFlock(flock)
  }

  //grid creation

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  let realResetGrid = () => {}
  if (
    mergedSettings!.neighbors!.useGrid &&
    mergedSettings!.neighbors!.gridCellSize
  ) {
    createGrid(mergedSettings!.boundaries!.shape, mergedSettings!.neighbors!)
    realResetGrid = resetGrid
  }

  useFrame(realResetGrid)

  return (
    <>
      {render &&
        flock.map((agent) => (
          <Fragment key={agent.id}>{render(agent)}</Fragment>
        ))}
      {children}
    </>
  )
}
